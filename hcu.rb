require 'delegate'
require 'rpm'
require 'active_record'
require 'event'
require 'constants/event_ids'
require 'apputils/hcu_network_utils'
require 'Rpc/HcuRpc/Rpc'
require 'watchdog'
require 'log'
require 'hcu_upgrade'
require 'performance_data'
require 'mactrak_performance_data'
require 'hsmconfig'

#Database accessor (ORM)
class Hcuconfig < ActiveRecord::Base
  has_many :rpmconfigs
  has_one :hsmconfig
end

class Hcu < DelegateClass(Hcuconfig)
  attr_reader :rpm_delegator
  ONLINE_READY = 0  
  ONLINE_UPGRADING = 1
  def initialize(hcuconfig, rpm_delegator)
    @rpm_delegator = rpm_delegator
    @hcuconfig = hcuconfig
    #This initializes the delegation for the config info
    super(@hcuconfig)
    #Check to see if any devices require upgrade
    hcu_upgrade = HcuUpgrade.new(self, "/DOS1/Firmware/")
    upgrading = false
    #Check to see if any devices need upgraded if a firmware package exists		
    upgrading = hcu_upgrade.upgrade_rpms(@rpm_delegator.rpms) if hcu_upgrade.files_exist?
    #If we are upgrading devices, the online rpc call for HCU ready will be made after devices have finished upgrading
    Thread.new{send_hcu_online_rpc(ONLINE_READY)}
    
				
    @watchdog = Watchdog.new(uid, watchdog_interval)
  end

  def update_watchdog_interval(interval)
    begin
      @watchdog.set_update(interval)
      #Update the db. I'm not sure why the delegate doesn't work right here, but I have to specify the watchdog_interval
      # with the hcuconfig object. If I don't the value isn't saved in the database.  It is because by rails convention you 
      #should use the interface getter/setter rather than directly. z
      @hcuconfig.watchdog_interval = interval
      @hcuconfig.save
    rescue
      Log::GeneralLogger.error "update_watchdog_interval exception"
    end
  end

  # +send_hcu_online_rpc?+ sends the boot rpc event to the server with a status
  # === Parameters
  # * boot_status indicates the state of the hcu (put in the status of the RPC call)
  def send_hcu_online_rpc(boot_status)
    sleep 10
    #If the uid has not been assigned then the HCU has not been added to the server
    puts "Sending online rpc to server #{server_ip} from #{hcu_ip} with status #{boot_status}, uid is #{uid}"          
    if uid != 0
      #if HcuNetworkUtils.local_ip == hcu_ip
      #hcuBootRpc = HcuBootRpc.new
      #ret = hcuBootRpc.send_hcu_online(server_ip, hcu_ip, uid, boot_status)
      #if ret != 0
      #  Log::GeneralLogger.error "Error sending hcu online to server #{server_ip}"
      #else
          #Send online event if the HCU is ready
      #  Log::GeneralLogger.info "Done sending online message"
      #I can't reliably get the boot rpc message to send within the ruby context. It seems like if I completely
      #launch it in a separate process it works fine.
      system("ruby send_boot_rpc.rb #{server_ip} #{hcu_ip} #{uid} #{boot_status}")
      sleep 5
      send_hcu_online_event if boot_status == ONLINE_READY
      #end
      #else
        #The HCU sim address has changed since it was added to the server. This is a problem and the HCU ip
        #needs to be changed or it needs to be re-added to the server.
      #  Log::GeneralLogger.error "HCU sim address not equal to the same address as when it was added to the server!!!"
      #end
    end
  end

  # +send_hcu_online_event?+ sends the HCU online event
  def send_hcu_online_event
    Event.new(uid, EventIds::EVENT_HCU_POWERUP, EventIds::EVENT_PRIORITY_HIGH)
  end

  # +install_software?+ is called inproc of RPC call. It verifies upgrade files exist and spawns a new thread to do the upgrade. Returns true on success
  def install_software     
    hcu_upgrade = HcuUpgrade.new(self, "/DOS1/Firmware/")	
	  status = hcu_upgrade.files_exist?
		#If everything looks OK, spawn the upgrade
		if status
			Thread.new{hcu_upgrade.upgrade}
		end
		status
  end

  def print_cfg(extended=true)
    Log::GeneralLogger.info "HCU: #{self.sbc_model} (#{self.uid})"
    self.rpm_delegator.rpms.each do |slot, rpm|
      Log::GeneralLogger.info "Slot #{slot}: #{rpm.model} (#{rpm.uid})"
      if extended
        rpm.ports.each {|port| puts "\t#{port.label} (#{port.uid})"}
      end
    end
  end
  
  def hsm
    @hcuconfig.hsmconfig
  end
  
  def get_spectral_performance_data
    performance_data = PerformanceData.new
    self.rpm_delegator.rpms.each do |slot, rpm|
      rpm.ports.each do |port| 
        performance_data.create_port_data(slot, port.port_number, port.uid)        
      end
    end
    return performance_data.all_data
  end  

  def get_mactrak_performance_data
    mactrak_performance_data = MacTrakPerformanceData.new
    self.rpm_delegator.rpms.each do |slot, rpm|
      if rpm.model == 'RPM3000'
        rpm.ports.each do |port| 
          mactrak_performance_data.create_port_data(port.uid)        
        end
      end
    end
    return mactrak_performance_data
  end  
  
private
  #This method is meant to be used as a separate thred. It simulates the wait time of the actual HCU firmware.
  def install_software_wait
    #Wait for ~2 minutes to simulate the reboot time of the actual HCU
    sleep(30)
    Event.new(uid, EventIds::EVENT_HCU_CFG_MODIFIED, EventIds::EVENT_PRIORITY_HIGH)
  end

    def check_for_device_upgrades
    end
end



