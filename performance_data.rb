require 'performance_helper'

class PerformanceData
  attr_reader :default_data, :default_frequencies, :all_data
  def initialize
    @default_data = []
    @default_frequencies = []
    @all_data = []
    create_default_data    
  end 

  def create_port_data(rpm_slot, port, port_uid)
    data = {} 
    data[:rpm] = rpm_slot
    data[:port] = port
    data[:port_uid] = port_uid
    data[:major_rev] = 1
    data[:minor_rev] = 0
    data[:number_scans] = 2700    
    data[:start_time] = @times[0]
    data[:stop_time] = @times[1]
    data[:frequencies] = Array.new(@default_frequencies)
    data[:stats] = get_randomized_data    
    @all_data << data
    data
  end

  private  
  #For now we'll always use the default plan to return data so create the frequency array now
  def create_default_data
    #5.0-14.75 -50
    #15 - 18.25 -1.5 #3.2 wide channel @ 16.6MHz 
    #18.5 - 19.75 -50
    #20-23.25 -1.0 #3.2 wide channel @ 21.6MHz
    #23.50 - 25.75 -50
    #26 - 32.5 2.0 #6.4 wide channel @ 29.2MHz
    #32.75 - 33.75 -50
    #34.0 - 37.25 0.5 #3.2 wide channel @ 35.6MHz
    f = 5.0
    step_size = 0.25
    241.times do |i|
      this_point = {}
      if f <= 14.75
        this_point[:min_level] = -50.0
      elsif f <= 18.25
        this_point[:min_level] = -1.5
      elsif f <= 19.75
        this_point[:min_level] = -50.0
      elsif f <= 23.25
        this_point[:min_level] = -1.0
      elsif f <= 25.75
        this_point[:min_level] = -50.0
      elsif f <= 32.5
        this_point[:min_level] = -2.0
      elsif f <= 33.75
        this_point[:min_level] = -50.0
      elsif f <= 37.25
        this_point[:min_level] = -0.5
      else
        this_point[:min_level] = -50.0
      end
      this_point[:max_level] = this_point[:min_level] + 8.0
      this_point[:avg_level] = this_point[:min_level] + 4.0        
      @default_frequencies << f
      @default_data << this_point
      f += step_size
    end
    @times = PerformanceHelper.create_timestamps    
  end    
  def get_randomized_data
    data  = Array.new(@default_data)
    data.each do |d|
      d[:min_level] += (rand*4 - 2) #Move the levels around by +/- 2
      d[:max_level] += (rand*4 - 2) #Move the levels around by +/- 2
      d[:avg_level] += (rand*4 - 2) #Move the levels around by +/- 2       
      d[:threshold1_exceeded] = rand(100)        
      d[:threshold2_exceeded] = rand(100)
      d[:threshold3_exceeded] = rand(100)
      d[:threshold4_exceeded] = rand(100)
    end
    return data
  end
end

#pd = PerformanceData.new
#puts "Default data"
#p pd.default_data

#puts "Frequenceis"
#p pd.default_frequencies

# port_data = pd.create_port_data(1, 2, 103) 
# port_data = pd.create_port_data(1, 3, 104) 

# pd.all_data.each do |d|
  # puts "Port: #{d[:port_uid]}"
  # index = 0
  # port_data[:stats].each  do |s|
    # puts "#{pd.default_frequencies[index]}, #{s[:max_level]}"
    # index += 1
  # end
# end
