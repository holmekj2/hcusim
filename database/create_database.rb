#!/usr/bin/ruby
require 'rubygems'
require 'hcu'
require 'hsmconfig'
require 'rpm'
require 'port'
require 'active_record'

database = "database/hcusim.sqlite3"
ActiveRecord::Base.establish_connection(
  :adapter => "sqlite3",
  :database  => database,
  :pool => 5,
  :timeout => 5000
)

def create_hcu(number, hcu_ip)
  hcu = Hcuconfig.new
  hcu.sbc_model = 'MVME2700'
  hcu.chassis_model = 'HCU1500'
  hcu.flash_size = 10
  hcu.ram_size = 128
  hcu.sbc_fw_version = '3.00.12'
  hcu.fw_version = '3.00'
  hcu.hard_drive_capacity = 100000
  hcu.uid = 0
  hcu.watchdog_interval = 900
  hcu.serial_number = number
  #last_ip_octet = 30 + number
  #hcu.hcu_ip = "10.11.56.#{last_ip_octet}"
  hcu.hcu_ip = hcu_ip
  hcu.label = "Hcu#{number}"
  hcu.notes = ""
  hcu.location = ""
  hcu.save
  15.times do |i|
    create_rpm(hcu.id, i+1)
  end
  create_hsm(hcu.id)
end

def create_rpm(hcu_id, slot_number)
  rpm = Rpmconfig.new
  rpm.hcuconfig_id = hcu_id
  rpm.model = 'RPM3000'
  rpm.fw_version = "3.00.1"
  rpm.uid = 0
  rpm.serial_number = hcu_id + slot_number
  rpm.device_enabled = 0
  rpm.label = "Hcu#{hcu_id}Rpm#{slot_number}"
  rpm.notes = ""
  rpm.slot_number = slot_number
  rpm.minimum_frequency = 0.5
  rpm.maximum_frequency = 85.0
  rpm.assigned_device_id = 0
  rpm.save  
  8.times do |i|
    create_port(hcu_id, rpm.id, slot_number, i)
  end
end

def create_hsm(hcu_id)
  hsm = Hsmconfig.new
  hsm.hcuconfig_id = hcu_id
  hsm.model = 'HSM1000'
  hsm.fw_version = "1.00"
  hsm.uid = 0
  hsm.serial_number = rand(10000)
  hsm.device_enabled = 0
  hsm.broadcast_enabled = 0
  hsm.label = "HSM"
  hsm.notes = ""
  hsm.telemetry_frequency = 85.0
  hsm.telemetry_level = 0
  hsm.save  
end

def create_port(hcu_id, rpm_id, slot_number, port_number)
  port = Portconfig.new
  port.rpmconfig_id = rpm_id
  port.uid = 0
  port.label = "Hcu#{hcu_id}Rpm#{slot_number}Port#{port_number}"  
  port.notes = ""
  port.test_point_comp = 0
  port.attenuation = 0
  port.port_number = port_number
  port.save
end

if ARGV.length != 2
  puts "usage create_database.rb numberHcus baseIpAddress"
else
#hcu_ip = 10.11.56.31
  hcu_ip = ARGV[1]
  ARGV[0].to_i.times do |i|
    create_hcu(i+1, hcu_ip)
    hcu_ip =~ /(\d+\.\d+\.\d+\.)(\d+)/
    puts "Created HCU#{i} #{hcu_ip}"    
    hcu_ip = $1 + ($2.to_i + 1).to_s
  end
end

  #last_ip_octet = 30 + number
  #hcu.hcu_ip = "10.11.56.#{last_ip_octet}"

