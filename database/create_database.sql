CREATE TABLE "hcuconfigs" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "brief_description" varchar(255), "sbc_model" varchar(255), 
"chassis_model" varchar(255), "flash_size" integer, "ram_size" integer, "hard_drive_capacity" integer, "sbc_fw_version" varchar(255), 
"fw_version" varchar(255), "server_ip" varchar(255), "hcu_ip" varchar(255), "uid" integer, "watchdog_interval" integer, "serial_number" varchar(255), 
"device_enabled" boolean, "thresholds_enabled" boolean, "alarms_enabled" boolean, "snmp_enabled" boolean, "label" varchar(255), 
"location" varchar(255), "notes" text, "created_at" datetime, "updated_at" datetime);

CREATE TABLE "hsmconfigs" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "hcuconfig_id" integer, "model" varchar(255), "fw_version" varchar(255), 
"uid" integer, "serial_number" varchar(255), "device_enabled" integer, "broadcast_enabled" integer, "alarms_enabled" boolean, 
"label" varchar(255), "location" text, "notes" text, "telemetry_frequency" decimal(8,2), "telemetry_level" decimal(8,2), "created_at" datetime, "updated_at" datetime);


CREATE TABLE "portconfigs" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "rpmconfig_id" integer, "uid" integer, "device_enabled" boolean, 
"thresholds_enabled" boolean, "alarms_enabled" boolean, "label" varchar(255), "notes" text, "test_point_comp" integer, "attenuation" integer, 
"port_number" integer, "created_at" datetime, "updated_at" datetime);

CREATE TABLE "rpmconfigs" ("id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "hcuconfig_id" integer, "model" varchar(255), "fw_version" varchar(255), 
"uid" integer, "serial_number" varchar(255), "device_enabled" boolean, "thresholds_enabled" boolean, "alarms_enabled" boolean, "label" varchar(255), 
"notes" text, "slot_number" integer, "minimum_frequency" decimal(4,1), "maximum_frequency" decimal(4,1), "assigned_device_id" integer, 
"created_at" datetime, "updated_at" datetime);
