require 'delegate'
require 'hcu'
require 'port'
require 'forwardable'
require 'active_record'
require 'constants/event_ids'
require 'log'
require 'yaml'
require 'spectrum_analyzer'

#Database accessor (ORM)
class Rpmconfig < ActiveRecord::Base
  belongs_to :hcuconfig
  has_many :portconfigs
end

#Accepts all RPM or port messages from HCU
class RpmDelegator
  extend Forwardable
  #List all methods that need to be forwarded to an RPM. This prevents the RpmDelegator from wrapping all the calls to RPM's. 
  def_delegators :@current_rpm, :start_spectrum_analyzer

  attr_reader :rpms
  
  #Accepts an array of RPM instances and stores them in a hash with rpm Id's as the keys
  def initialize(rpm_list=[])
    #Build hash of all RPM's. This would be used as lookup whenever we get an RPM bound message.
    #@rpms = Hash.new
    @rpms = ActiveSupport::OrderedHash.new
    rpm_list.each do |rpm|
      @rpms[rpm.slot_number] = rpm
    end
    @current_rpm = nil
  end
  #Add an rpm to the hash
  def add_rpm(rpm)
    @rpms[rpm.slot_number] = rpm
  end
  
  #Get an instance to the RPM based on the RPM id integer. TODO I need to figure out whether this is 0 or 1 based.
  def get_rpm(slot_number)
    if @rpms[slot_number] == nil
      #puts "RPM#{slot_number} is not valid"
    end
    return @rpms[slot_number]
  end
  
  #Set the current delegate object. When an RPC call is received, that RPM is set here
  def set_rpm_delegate(slot_number)
    @current_rpm = @rpms[slot_number]
  end
  
  #Called if the RPM does not implement a method
  def method_missing(method)
    Log::GeneralLogger.error "Rpm #{@current_rpm.slot_number} received unknown message: #{method}"
  end
end

class Rpm < DelegateClass(Rpmconfig)
  attr_reader :ports, :spectrum_analyzers
  #Accept an active record (RpmConfig). We delegate all the parameters to the active record so we don't have to redefine all the parameters in this class.  
  def initialize(rpmconfig)
    @rpmconfig = rpmconfig
    #Initially create empty list of ports. These are added by the builder.
    @ports = []
    #This initializes the delegation for the config info
    super(@rpmconfig)
    #Setup SA stuff
    @spectrum_analyzers = Hash.new
    @sa_mutex = Mutex.new
  end
  #port_number is 0 - 7
  def add_port(port)
    @ports[port.port_number] = port 
  end

  def sa_connect(server_ip, server_port)
    ret = 0
    begin
      @sa_mutex.synchronize do
        if @spectrum_analyzers.length >= 2
          raise RangeError
        end    
        sa = SpectrumAnalyzer.new(server_ip, server_port)
        #Stick the newly created sa in the hash
        @spectrum_analyzers[server_port] = sa
      end        
    rescue RangeError
      ret = INTF_ERR_CONNECT_UNAVAIL
      Log::GeneralLogger.info "sa_connect no more sa's available"
    rescue
      ret = INTF_ERR_SOCKET_CONNECT    
      Log::GeneralLogger.info "sa_connect socket connect failure"      
    end
    return ret
  end
  #Index is the socket port
  def sa_disconnect(index)
    @spectrum_analyzers[index].disconnect
    #Removed the spectrum analyzer from the hash
    @spectrum_analyzers.delete(index)
  end
  # +install_software?+ simulates an rpm upgrade if a new version is available. Returns true if device is upgrading
  # === Parameters
  # * version new version available
  # * hcu_upgrade_handler reference to the handler to indicate when upgrade is complete
  def install_software(version, hcu_upgrade_handler)
    upgrading = false
    if @rpmconfig.fw_version < version
      Log::GeneralLogger.info "Upgrading RPM#{@rpmconfig.slot_number} firmware: #{version}"      
      @rpmconfig.fw_version = version
		  @rpmconfig.save      
      upgrading = true
      Thread.new{install_software_thread(hcu_upgrade_handler)}  
    end
    upgrading
  end
  #Read features from file
  def get_features
    features = File.open('features.yaml'){|yf| YAML::load(yf) }
    return features
  end
private
  # +install_software_thread?+ simulates an rpm upgrade 
  # === Parameters
  # * hcu_upgrade_handler reference to the handler to indicate when upgrade is complete
  def install_software_thread(hcu_upgrade_handler)  
    #Wait a little bit to simulate the upgrade process
    sleep(30)
    Event.new(uid, EventIds::EVENT_RPM1000_CFG_MOD, EventIds::EVENT_PRIORITY_HIGH)    
    hcu_upgrade_handler.send(:rpm_upgrade_complete, @rpmconfig.slot_number)
  end  
end

# class Rpm1000
# end

# class Rpm3000
  # def start_qam_analyzer
  # end
# end

# class RpmSpectrumData
# end

# class RpmQamData
# end

# rpms = []
# rpms << Rpm.new(1)
# rpms << Rpm.new(2)
# rd = RpmDelegator.new(rpms)
# rd.set_rpm_delegate(1)
# rd.start_spectrum_analyzer
# rd.set_rpm_delegate(2)
# rd.start_spectrum_analyzer2
