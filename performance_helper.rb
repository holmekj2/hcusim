module PerformanceHelper
  def self.create_timestamps
    t = Time.now.utc
    #Take the current time and round down to nearest 15 minute interval for stop time. Start time is 15 minutes prior to that
    stop_minute = 0
    start_minute = 0
    hour = 0
    if t.min > 45 
      stop_minute = 45
      start_minute = 30
    elsif t.min > 30 
      stop_minute = 30
      start_minute = 15
    elsif t.min > 15 
      stop_minute = 15
      start_minute = 0
    else 
      stop_minute = 0
      start_minute = 45
      hour = -1
    end    
    stop_time = Time.utc(t.year, t.month, t.day, t.hour, stop_minute, 0)
    start_time = Time.utc(t.year, t.month, t.day, t.hour + hour, start_minute, 0)    
    unix_stop_time = stop_time.to_i
    unix_start_time = start_time.to_i
    return [unix_start_time, unix_stop_time]
  end        
end
