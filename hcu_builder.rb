require 'rpm'
require 'hcu'
require 'log'

class HcuBuilder
  attr_reader :hcu
  #Read xml file and build composition structure of HCU    
  def initialize(device_configuration_id)
    #Configurations are stored in database. Use active records to access db.
    hcuconfig = Hcuconfig.find(device_configuration_id)
    #Find all RPM's on this HCU
    rpm_delegator = RpmDelegator.new()
    hcuconfig.rpmconfigs.each do |rpmconfig| 
      #Create a new RPM object for each config    
      rpm = Rpm.new(rpmconfig)
      #Add rpm instance to delegator
      rpm_delegator.add_rpm(rpm)      
      #Find all ports on this RPM
      rpmconfig.portconfigs.each do |portconfig| 
        #Create a new RPM object for each config    
        port = Port.new(portconfig)
        #Add rpm instance to delegator
        rpm.add_port(port)      
      end      
    end    
    @hcu = Hcu.new(hcuconfig, rpm_delegator)
  end
end
