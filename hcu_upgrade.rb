# This class simulates the sequence of HCU events during the package upgrade sequence
# :title HcuUpgrade
require 'xmlsimple'
require 'log'

class HcuUpgrade
  # +init+ is called upon a firmware upgrade request command  
  # It creates a new thread to run the install_software method outside the context of the RPC call  
  # === Parameters
  # * hcu reference to hcu object
  # * _firmware_files_path string of path where firmware files reside (must have / at the end)	
  def initialize(hcu, firmware_files_path)
    @firmware_files_path = firmware_files_path		
		@hcu = hcu
  end
  # +files_exist?+ verifies all files in the package description file are in the firmware_file_path
  def files_exist?
    status = true
    #Parse the description file and verify the existence of all firmware files. 
    begin
      filepath = @firmware_files_path + "PathtrakFirmwarePackage.xml"
      raise ArgumentError, filepath if !File.file?(filepath)

      #Parse the upgrade description file to a hash
      @upgrade_description = XmlSimple.xml_in(filepath)    
      files = Array.new
      if @hcu.sbc_model == 'MVME2700'
	files.push(@upgrade_description['HcuKernel2700Filename'])
      else
	files.push(@upgrade_description['HcuKernel5500Filename'])
      end
      if @hcu.chassis_model == 'HCU400'
	files.push(@upgrade_description['HcuApp400Filename'])       
      else
	files.push(@upgrade_description['HcuApp1500Filename'])
      end
      files.push(@upgrade_description['HsmFilename'])
      files.push(@upgrade_description['Rpm1000Filename'])         
      files.push(@upgrade_description['Rpm2000Filename'])
      files.push(@upgrade_description['Rpm3000Filename'])
      #Verify each file exists
      files.each do |file|
        fpath = @firmware_files_path + file.to_s
	#Log::GeneralLogger.error fpath				
        raise ArgumentError, fpath if !File.file?(fpath)
      end     
    rescue ArgumentError => e
      Log::GeneralLogger.error "HcuUpgrade #{e} does not exist" 
      status = false
    rescue 
      Log::GeneralLogger.error "HcuUpgrade #{filepath} xml error"   
      status = false
    end
    status
  end
  # +upgrade?+ simulates a firmware upgrade initiated by user
  # === Parameters
  def upgrade
    begin
			sleep(5)
			Thread.pass
			upgrade_hcu
			#Simulate a reboot
			@hcu.send_hcu_online_rpc(Hcu::ONLINE_UPGRADING)
      upgrading = upgrade_rpms(@hcu.rpm_delegator.rpms)
			#If no RPMS are upgraging then send the HCU online ready event. If RPM's are upgrading,
			#the callback (rpm_upgrade_complete) will take care of the event
			@hcu.send_hcu_online_rpc(Hcu::ONLINE_READY) if !upgrading 
    rescue ArgumentError => e
      Log::GeneralLogger.error "HcuUpgrade #{e} does not exist"         
    end
  end
	
  # +rpm_upgrade_complete?+ callback used by rpms to indicate when they have completed an upgrade
  # === Parameters
  # * slot_number of the calling rpm 
	def rpm_upgrade_complete(slot_number)
		rpm = @rpms_in_upgrade.delete(slot_number)
		Log::GeneralLogger.error("HcuUpgrade::rpm_upgrade_complete unknown rpm") if rpm.nil?
		if @rpms_in_upgrade.empty?
			#All RPM's have finished upgrading so send notification
			Log::GeneralLogger.info("All devices completed upgrades")			
			@hcu.send_hcu_online_rpc(Hcu::ONLINE_READY)			
		end
	end
	
  # +upgrade_rpms?+ simulates upgrade of all rpms, returns true if any devices are in upgrade
  # === Parameters
  # * rpms hash of all rpms in system
	# Raises ArgumentError if the xml does not contain versions	
  def upgrade_rpms(rpms)
	  version = Hash.new
    version['RPM1000'] = @upgrade_description['Rpm1000Version']
    version['RPM2000'] = @upgrade_description['Rpm2000Version']
    version['RPM3000'] = @upgrade_description['Rpm3000Version']				
    raise ArgumentError, "Upgrade package description does not have HcuVersion" if version.has_value?(nil)
		upgrading = false
		@rpms_in_upgrade = Array.new 
    rpms.each_value do |rpm|
		  #call the rpm install software and send the proper version and the callback method for the rpm to call when the upgrade is complete
			#Returns true if performing an upgrade
		  upgrading |= rpm.install_software(version[rpm.model].to_s, self)
			#If the RPM is upgrading stick it in an array. This array is checked when the upgrade complete callback is called
			@rpms_in_upgrade.push(rpm.slot_number) if upgrading
		end
		return upgrading
  end  
	
private 
  # +upgrade_hcu?+ simulates a hcu firmware upgrade
  # === Parameters
  # * hcu reference to hcu so that the upgrade can get the device type and update db with firmware versions
	# Raises ArgumentError if the xml does not contain HcuVersion
  def upgrade_hcu
    version = @upgrade_description['HcuVersion'].to_s
    raise ArgumentError, "Upgrade package description does not have HcuVersion" if version.nil?
		@hcu.fw_version = version if @hcu.fw_version < version 
		@hcu.save
  end  
end
