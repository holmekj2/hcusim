iptables -t nat -A PREROUTING -p tcp -i eth0 -d 192.168.56.121 --dport 2056 -j DNAT --to 192.168.56.120:50001
iptables -A FORWARD -p tcp -i eth0 -d 192.168.56.121 --dport 50001 -j ACCEPT

iptables -t nat -A PREROUTING -p tcp -i eth0 -d 192.168.56.122 --dport 2056 -j DNAT --to 192.168.56.120:50002
iptables -A FORWARD -p tcp -i eth0 -d 192.168.56.122 --dport 50002 -j ACCEPT

