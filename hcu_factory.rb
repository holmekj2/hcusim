require 'hcu_builder'
require 'event'
require "Rpc/Intercepter/rpc_override"
require "Rpc/HcuRpc/rpc_handler"

module HcuFactory
  def self.create_hcu(hcuid, rpc_socket)
    #Create an Event Sender object
    @event_sender = EventSender.new()

    @hcu_builder = HcuBuilder.new(hcuid)
    @hcu = @hcu_builder.hcu
    @hcu.print_cfg(false)
    

    #Create the RPC override mechanism
    rpc_intercepter = RpcOverride.new
    @rpc_handler = RpcHandler.new(@hcu, @hcu.rpm_delegator, @hcu.hsm, @event_sender, rpc_socket, rpc_intercepter)
    @rpc_handler_thread = Thread.new{@rpc_handler.start}
  end
  
  def self.get_hcu
    @hcu
  end
  
  def self.get_event_sender
    @event_sender
  end
  
  def self.get_rpc_handler_thread
    @rpc_handler_thread
  end
  def self.shutdown
    @rpc_handler.shutdown
  end
end
