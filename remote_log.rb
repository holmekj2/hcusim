require 'socket'               
require 'log'
require 'thread'
#This class opens a socket server and awaits a client that wants log data. After a client connects
#RemoteLog tells Log to forward all log messages to it. It then sends them over to the client. Once
#the client disconnects RemoteLog then tells Log to redirect messages to the default output. 
class RemoteLog
  attr_reader :connected
  def initialize(port)
    @port = port
    @connected = false
    @server = TCPServer.open(@port)  
    Thread.new{start_remote}
  end

  def start_remote
    begin
      while true
        wait_for_connection
        send_data if @connected
      end
    rescue
      retry
    end
    #send_data
  end

  private
  def wait_for_connection
    #Wait for a client connection. We only allow one client at a time now. 
    begin      
      @client = @server.accept       # Wait for a client to connect
      @connected = true
      Log::GeneralLogger.info "Connected to remote socket. Redirecting log"
    rescue
      Log::GeneralLogger.error "RemoteLog error waiting for connection"
    end
  end

  def send_data
    begin
      #create_pipe    
      rd, wr = IO.pipe
      #Tell the log class to redirect output to the pipe      
      Log.redirect_to_pipe(wr)
      #Sit in a loop and collect from the pipe and output to the socket. Wait for an exception to get out of loop. 
      while @connected
        @client.puts(rd.gets)
        #puts "RemoteLog: #{rd.gets}"
      end
    rescue Exception => e
      #Once the socket is shutdown we'll get an exception when we try to write to it. We'll probably
      #drop the last message in the pipe but I don't really care. 
      #Tell the log class to redirect output to wherever it was going before the redirect to the pipe
      Log.redirect_to_default
      @client.close                 
      @client.nil
      @connected = false
      #Shutdown both ends of the pipe
      rd.close
      wr.close      
    end
    return 0
  end
end

