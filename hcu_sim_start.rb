#!/usr/bin/ruby
require 'rubygems'
require 'active_record'
require 'hcu_factory'
require 'apputils/hcu_network_utils'
require 'log'
require 'remote_log'
require 'remote_commands/server/remote_command_server'
#require 'ruby-debug'


version = "0.300"
#ruby -w scripts/start_hcusim.rb 4
#Uncomment this to see sql logging
#ActiveRecord::Base.logger = Logger.new(STDERR)
#ActiveRecord::Base.colorize_logging = false
#This probably should be moved to a yml config file (e.g. database.yml)
#ActiveRecord::Base.establish_connection(
#    :adapter => "mysql",
#    :database  => "hcusim_development",
#    :host => "localhost",
#    :user => "root"
#)
#Default to a 400 unless the HCU id is specified
#ARGV[0] is the id of the HCU to start
#
if ARGV[0].nil?
  hcuid = 1
else
  hcuid = ARGV[0].to_i   
end

#Set the RPC socket to rpc_base_socket + the hcuid
#Set this parameter to zero to use the portmapper
#In Bedrock in Device.java in the HAL, change int port = pc.getPort to int port = x
rpc_base_socket = 50000
drb_base_socket = 60000
if ARGV[1].nil?
  rpc_socket = rpc_base_socket + hcuid
  drb_socket = drb_base_socket + hcuid  
else
  rpc_socket = ARGV[1].to_i
end

#Start the remote logger
remote_log_base_port = 25000
remote_log_port = remote_log_base_port + hcuid
Log::GeneralLogger.info "Starting remote log on #{remote_log_port}"
RemoteLog.new(remote_log_port)

database = "database/hcusim.sqlite3"
ActiveRecord::Base.establish_connection(
  :adapter => "sqlite3",
  :database  => database,
  :pool => 5,
  :timeout => 5000
)

#Catch a Ctrl C or kill command and kill off parent and all child processes 
Signal.trap("INT") do
  Log::GeneralLogger.info "Shutting down Hcu Simulator..."
  DRb.stop_service
  #Send a -9. 9 indicates a kill signal. Negative indicates kill off the process group
  Process.kill(-9, 0)
end
Signal.trap("TERM") do
  Log::GeneralLogger.info "Shutting down Hcu Simulator..."
  DRb.stop_service
  #Send a -9. 9 indicates a kill signal. Negative indicates kill off the process group
  Process.kill(-9, 0)
end

HcuFactory.create_hcu(hcuid, rpc_socket)
Log::GeneralLogger.info "Starting HCU sim #{version} on #{HcuFactory.get_hcu.hcu_ip} port #{rpc_socket}"

Log::GeneralLogger.info "Starting DRb on #{drb_socket}"
DRb.start_service "druby://:#{drb_socket}", RemoteCommandServer.new

HcuFactory.get_rpc_handler.join
DRb.thread.join



