module HcuFileUtils
  #Get first file in the list. Excludes directories. 
  def HcuFileUtils.get_next_file(file_directory, file_list)
    file = nil
    begin
      file = file_list.shift
      if file == nil
        break
      end  
    end until File.file?(file_directory + file)
    return file
  end
end
