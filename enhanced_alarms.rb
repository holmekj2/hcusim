require 'rubygems'
require 'active_support'

def get_enhanced_alarm_data
  ea = nil
  File.open("enhanced_alarms.json", "r") do |f|
    j = f.gets
    ea = ActiveSupport::JSON.decode(j)
  end
  ea
end

ea = get_enhanced_alarm_data
puts ea.class
p ea

