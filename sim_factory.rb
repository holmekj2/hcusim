#!/usr/bin/ruby
require 'rubygems'
require 'active_record'
require 'hcu_factory'
require 'apputils/hcu_network_utils'
require 'log'
require 'remote_log'
require 'remote_commands/server/remote_command_server'

class HcuSim
  attr_reader :sim_number, :rpc_port, :sim_pid, :drb_port, :log_port
  DRB_BASE_PORT = 60000
  RPC_BASE_PORT = 50000
  REMOTE_LOG_PORT = 25000
  def initialize(sim_number)    
    @sim_number = sim_number
    @drb_port = DRB_BASE_PORT + sim_number
    @rpc_port = RPC_BASE_PORT + sim_number
    @log_port = REMOTE_LOG_PORT + sim_number
    @sim_pid = 0
  end
  def start
    Thread.new{start_sim}
  end
  def stop
    Process.kill("TERM", @sim_pid)    
  end  
  def is_running?
    @sim_pid > 0
  end
  def kill_sim
    pid = Process.pid
    DRb.stop_service
    SimFactory.shutdown(@sim_number)
    sleep 1
    Process.kill(9, pid)
  end
  
  private
  def start_sim
    pid = fork
    if pid == nil
      #Child process
      #registr a signal listener to terminate process
      Signal.trap("TERM") do
        kill_sim
      end
      begin 
        SimFactory.create_new_sim(@sim_number, @rpc_port, @drb_port, @log_port)
      rescue => e
        puts "Failed to create sim"
        puts e.message
      end
    else
      @sim_pid = pid
      #Parent process
      Process.wait(@sim_pid)
      @sim_pid = 0
    end      
  end
end

module SimFactory
  #This is blocking so call in context of new thread
  def self.create_new_sim(hcu_id, rpc_socket, drb_socket, remote_log_socket)    
    RemoteLog.new(remote_log_socket)
    Log::GeneralLogger.info "Starting remote log on #{remote_log_socket}"  
    HcuFactory.create_hcu(hcu_id, rpc_socket)
    Log::GeneralLogger.info "Starting HCU sim #{@version} on #{HcuFactory.get_hcu.hcu_ip} port #{rpc_socket}"

    Log::GeneralLogger.info "Starting DRb on #{drb_socket}"
    Log.redirect_to_file(hcu_id)
    DRb.start_service "druby://:#{drb_socket}", RemoteCommandServer.new

    HcuFactory.get_rpc_handler_thread.join
    DRb.thread.join  
  end
  def self.shutdown(hcu_id)
    HcuFactory.shutdown
  end
end

#hcusim = HcuSim.new(1)
#hcusim.start
#sleep(5)
#hcusim.stop

#SimFactory.init
#SimFactory.create_new_sim(1, 50001, 60001, 25001)
