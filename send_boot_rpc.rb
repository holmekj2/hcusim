require 'Rpc/HcuRpc/Rpc'

hcuBootRpc = HcuBootRpc.new
sip = ARGV[0]
hip = ARGV[1]
uid = ARGV[2].to_i
boot_status = ARGV[3].to_i

ret = hcuBootRpc.send_hcu_online(sip, hip, uid, boot_status)
if ret != 0
  puts "Boot rpc error: #{ret} on #{hip}"
else
  puts "Successfully sent boot rpc on #{hip}"
end

