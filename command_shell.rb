class CommandShell
  #chain is a chain of all objects that may handle commands
  def initialize(command_chain)
    @command_chain = command_chain
  end
  def help
    puts "All commands are used in the following format: set_rpm_uid, rpm=x, uid=y"
    puts "For arguements that are to be treated as something other than string suffix the arg name with (i) or (f) e.g. set_rpm_uid, rpm(i)=x, uid(i)=y
    #Create a base object. We'll compare this base object against the user defined objects to find the
    #methods that the user has defined.
    object = Object.new
    object_methods = object.methods
    @command_chain.each do |delegate|
      delegate_methods = delegate.methods
      unique_methods = delegate_methods - object_methods
      puts "Methods for #{delegate.class}"
      unique_methods.each {|m| puts "\t#{m}"}       
    end
  end
  def send_message(method, args=nil)
    handler = nil
    @command_chain.each do |delegate|     
      if delegate.respond_to?(method)
        handler = delegate
        break
      end
    end
    if handler != nil
      if (args != nil)
        handler.send(method, args)
      else
        handler.send(method)
      end
    else
      puts "Unknown command. Type help for help."
    end
  end
  def shell
    print "HcuSim$ "
    command_string = gets
    parse_command(command_string)
  end
  def parse_command(command_string)
    split_command = command_string.split(',')
    #puts "split_command: #{split_command}"
    method = split_command.shift    
    puts "split_args: #{split_command}"
    args = {}
    split_command.each do |arg|
      arg.split('=')
      
    end
    #args = []    
    #args << 
    #send_message(,  
  end
end

