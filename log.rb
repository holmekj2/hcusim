require 'log4r'
#This module implements creates Log4r loggers (similar to Log4j). It has two loggers which can be accessed from 
#any class. For example require Log in your class and then use "Log::GeneralLogger.info "my log msg" to write to the log. 
#Log by default writes to stdout. It also can be redirected to send log messages to a pipe (e.g. for the RemoteLog). 
module Log
  include Log4r
  GeneralLogger = Logger.new('generallog') 
  EventLogger = Logger.new('eventlog')   
  GeneralLogger.outputters = Outputter.stdout 
  EventLogger.outputters = Outputter.stdout   
  
  #Comment out these lines to see debug logging
  #GeneralLogger.level = Log4r::INFO  
  #EventLogger.level = Log4r::INFO
  def Log.redirect_to_pipe(pipe)      
    logpipe = IOOutputter.new('pipeOutputter', pipe)
    GeneralLogger.outputters = logpipe 
    EventLogger.outputters = logpipe
  end
  def Log.redirect_to_default
    GeneralLogger.outputters = Outputter.stdout 
    EventLogger.outputters = Outputter.stdout   
    Log::GeneralLogger.info "Redirecting log to stdout"    
  end
  def Log.redirect_to_file(hcu_id)
    file_outputter = FileOutputter.new('fileOutputter', {:filename => "./logs/Hcu#{hcu_id}.log", :trunc => true})
    event_file_outputter = FileOutputter.new('fileOutputter', {:filename => "./logs/Hcu#{hcu_id}Event.log", :trunc => true})    
    GeneralLogger.outputters = file_outputter
    EventLogger.outputters = event_file_outputter  
    Log::GeneralLogger.info "Redirecting log to stdout"    
  end
end
