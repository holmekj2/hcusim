require 'performance_helper'

class MacTrakPerformanceData
  attr_reader :packets, :channels, :start_time, :stop_time
  def initialize
    @channels = []
    @standard_channels = []
    @channel_id_index = 1
    @start_time, @stop_time = PerformanceHelper.create_timestamps
    @packets = []
    create_standard_channels
  end 

  def create_port_data(port_uid)
    #Create channels
    @standard_channels.each do |c|
      port_channel = {:channel_id => @channel_id_index, :port_uid => port_uid}
      port_channel = port_channel.merge(c)
      channels << port_channel
      #Create packets. Given that an RPM can generate 1800 packets in 15 minutes (2per second * 60sec * 15min) and we have 32 channels on each RPM (4 channels * 8 ports) we create 56 packets per channel (1800/32)
      #56.times do |i|
      #20.times do |i|
      #  packets << create_packet(@channel_id_index)
      #end
      @channel_id_index += 1
    end
  end
  
  def create_packet(channel_id)
    packet = {}
    packet[:channel_id] = channel_id
    #Create a timestamp in the 15 minute interval (900 seconds)
    packet[:timestamp] = rand(900) + @start_time
    packet[:mac_address_low] = rand(2147483648)
    packet[:mac_address_high] = rand(63335)
    packet[:codewords] = rand(5) + 5
    packet[:correctables] = rand(3)
    packet[:uncorrectables] = rand(3)
    packet[:mer_equalized_db100] = rand() * 6 + 3000
    packet[:mer_unequalized_db100] = packet[:mer_equalized_db100] - 2
    packet[:carrier_level_db100] = rand() * 2 
    packet[:impulse_stddev100] = rand() * 10 
    packet[:impulse_peak_level] = rand() * 10 - 25  
    packet[:impulse_peak_floor] = rand() * 5 - 50
    packet
  end
  
  def create_standard_channels
    #These are the same channels as in spectral_performance_data
    #5.0-14.75 -50
    #15 - 18.25 -1.5 #3.2 wide channel @ 16.6MHz 
    #18.5 - 19.75 -50
    #20-23.25 -1.0 #3.2 wide channel @ 21.6MHz
    #23.50 - 25.75 -50
    #26 - 32.5 2.0 #6.4 wide channel @ 29.2MHz
    #32.75 - 33.75 -50
    #34.0 - 37.25 0.5 #3.2 wide channel @ 35.6MHz
    @standard_channels << {:frequency_hz => 16600000, :symbolrate_ksps => 3200, :modtype => 1}   
    @standard_channels << {:frequency_hz => 21600000, :symbolrate_ksps => 3200, :modtype => 2}   
    @standard_channels << {:frequency_hz => 29200000, :symbolrate_ksps => 6400, :modtype => 3}   
    @standard_channels << {:frequency_hz => 35600000, :symbolrate_ksps => 3200, :modtype => 4}   
  end    
end

#pd = MacTrakPerformanceData.new
#pd.create_port_data(103) 
#pd.create_port_data(104) 
#p pd.channels
#pd.packets.each do |packet|
#  p packet
#end
