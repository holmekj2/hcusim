#!/bin/bash

if [ -z "$1" ]
then 
  hcus=2
else
  hcus=$1
fi

for i in $(seq 1 1 $hcus)
do
  gnome-terminal --title "HCU$i" -e "ruby hcu_sim_start.rb $i"
done
