require 'thread'
#require 'Rpc/SaRpc/RpmsaRpc'
require 'Rpc/HcuRpc/Rpc'
require 'constants/generic_constants'

class SpectrumAnalyzer
  MAX_POINTS = 500
  def initialize(server_ip, server_port, logging = false)
    begin
    @logging = logging    
    puts "SpectrumAnalyzer called #{server_ip}, #{server_port}"
    @sequence_number = 0
    @socket = TCPSocket.open(server_ip, server_port) 
    @xdr_sa = XdrSa.new
    @xdr_header = XdrHeader.new
    puts "xdrsa #{@xdr_sa}"
    rescue
      puts "Error opening SpectrumAnalyzer socket"
    end
  end  
  def disconnect
    @run_multi = false
    @socket.close
  end
  def start_multi_trace(trace_request)
    puts "start_multi_trace called"
    @trace_request = trace_request
    @sequence_number += 1
    @run_multi = true
    Thread.new{continuous_update(@trace_request[:ulInterval])}    
    return 0
  end
  def halt_multi_trace
    @run_multi = false
    return 0
  end
  def get_single_trace(trace_request)
    @trace_request = trace_request
    @sequence_number += 1  
    send_data
  end
  def continuous_update(update_rate_ms)
    while @run_multi
      xdr_output = send_data
      sleep(update_rate_ms / 1e6)    
    end
  end
private
  def send_data
    #Get data and xdr pack it. Return an xdr stream. 
    @raw_data = Array.new(MAX_POINTS) {|index| -50.0 + 10.0*rand}
    #puts @raw_data
    trace = convert_data
    xdr_output = @xdr_sa.encode_sadata(trace, @trace_request)
    num_bytes = @socket.write(xdr_output)      
    receive_ack
  end
  def convert_data
    trace = Hash.new
    trace[:ulMsgSeqNum] = @sequence_number
    trace[:nDataValid] = 1
    trace[:nUncal] = 0
    trace[:dwTimeSpan] = 0
    trace[:dwTestPntComp] = 0
    trace[:usNumPoints] = MAX_POINTS
    trace[:ulPackedVals_len] = MAX_POINTS / 2         
    trace[:dwXRes] = 0.005 #5KHz for now
    trace[:dwXStart] = @trace_request[:dwStartFreq]
    trace[:dwXStep] = (@trace_request[:dwStopFreq] - @trace_request[:dwStartFreq]) / trace[:usNumPoints]
    max = @raw_data.max
    min = @raw_data.min    
    trace[:dwYBase] = min 
    #Divide dwell level range into an evenly spaced 16 bit number
    trace[:dwYStep] = (max - min) / 65535;    
    #Set min step size
    trace[:dwYStep] = 0.0001 if trace[:dwYStep] < 0.0001
    step_data = []
    @raw_data.each do |datum|
      #Convert floating point to step values
      step_data << (datum - min) / trace[:dwYStep]
    end
    #Stick two words of converted data [0, 65535] into 32 bit packed word.
    trace[:ulPackedVals_val] = []
    #TODO fix this for odd length
    for i in 1..(trace[:usNumPoints] / 2 )
      low_word = step_data.shift.round
      high_word = step_data.shift.round * 0x10000
      trace[:ulPackedVals_val] << high_word + low_word      
    end
    return trace
  end
  #Wait for the event ack. Return the ack sequence number 
  def receive_ack
    seq = 0
    begin
        header = ""                        
        while header.length < GenericConstants::HEADER_LENGTH
          data = @socket.recv(GenericConstants::HEADER_LENGTH)        
          header << data
        end
        #Send the header off for decoding
        ack_header = @xdr_header.hdr_decode(header)        
        payload_length = ack_header[0]
        payload = ""
        while payload.length < (payload_length - GenericConstants::HEADER_LENGTH)
          data = @socket.recv(payload_length - GenericConstants::HEADER_LENGTH)     
          payload << data
        end
        seq = @xdr_sa.decode_sa_ack(payload, payload_length - GenericConstants::HEADER_LENGTH)
    rescue Exception
      puts "Error in SA receive_ack"
    end
    puts "Received event ack" if @logging
  end
 
end  

#sa = SpectrumAnalyzer.new(0,0)
#trace_request = Hash.new
#trace_request[:ulInterval] = 0
#trace_request[:dwStopFreq] = 50.0
#trace_request[:dwStartFreq] = 5.0
#trace = sa.get_single_trace(trace_request)
#puts trace.to_yaml
