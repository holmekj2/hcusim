require 'thread'
require 'constants/event_ids'

class ThreadedMessageQueue
  def initialize()
    @mutex = Mutex.new
    @queue = []
  end
  def send_message(message)
    @mutex.synchronize do
      @queue.push(message)
    end
  end
  #Non blocking receive. Returns nil if empty.
  def receive_message()
    message = nil
    @mutex.synchronize do
      message = @queue.pop()
    end
    return message
  end
  def number_messages()
    length = nil 
    @mutex.synchronize do
      length = @queue.length()
    end
    return length
  end
  def empty?()
    empty = nil
    @mutex.synchronize do
      empty = @queue.empty?()
    end
    return empty
  end
end

#Message queue to handle incoming HCU events. This class was written to handle multiple senders but only one receiver. That means multiple threads may be sending events,
#but only one thread is reading. 
class EventMessageQueue
  def initialize
    #Rather than worrying about sorting queues based on priority, we'll just have three queues for each of the
    #priorities: high, medium, and low
    @high_priority_mq = ThreadedMessageQueue.new
    @medium_priority_mq = ThreadedMessageQueue.new
    @low_priority_mq = ThreadedMessageQueue.new
    @queue_map = {EventIds::EVENT_PRIORITY_HIGH => @high_priority_mq, 
      EventIds::EVENT_PRIORITY_MEDIUM => @medium_priority_mq, EventIds::EVENT_PRIORITY_LOW => @low_priority_mq}
    @blocked_receiver_thread = nil
    @possible_priorities = [EventIds::EVENT_PRIORITY_HIGH, EventIds::EVENT_PRIORITY_MEDIUM, EventIds::EVENT_PRIORITY_LOW]
  end
  #put an event in the queue based on priority. 
  def send_event(event)
    priority = event.priority
    if !@possible_priorities.include?(priority)
      Log::EventLog.error "Invalid event priority. Defaulting to :low"
      priority = EventIds::EVENT_PRIORITY_LOW
    end
    @queue_map[priority].send_message(event)
    #This probably should be mutex locked. I don't think there will be issues since calling wakeup on a running thread doesn't do anything but ya never know. 
    if @blocked_receiver_thread != nil
      sleeper = @blocked_receiver_thread
      @blocked_receiver_thread = nil
      sleeper.wakeup()
    end
  end
  #Returns the highest priority event. Blocks until there is an event.
  def receive_event()
    #This ain't the prettiest code, but hey its 6am on my own time. God I'm a loser. 
    while true
      event = @high_priority_mq.receive_message()
      if event != nil
        return event
      elsif
        event = @medium_priority_mq.receive_message()       
        if event != nil
          return event
        end
      elsif
        event = @low_priority_mq.receive_message()       
        if event != nil
          return event
        end
      else
        #No events are ready. Give another thread the dance floor. send_event will wake up the receiving thread when an event is available
        @blocked_receiver_thread = Thread.current
        Thread.stop
      end
    end
  end
end
