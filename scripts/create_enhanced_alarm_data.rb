#active_support is needed for the xml conversion of the enhanced_alarm hash
#gem install activesupport
require 'rubygems'
require 'active_support/all'

#Create a single enhanced alarm point
def create_point(freq)
  point = Hash.new
  point[:frequency] = freq.to_s
  point[:level] = '-4700'
  point[:direction] = '1'
  point[:violated] = '1'
  point[:min] = '-500'
  point[:max] = '-400'
  point[:avg] = '-490'
  point[:number_violations] = '2'
  return point
end

#Create enhanced alarm stats for a monitoring plan of the given number points
def create_enhanced_alarm(threshold_number, monplan_length)
  ea = {}
  ea[:threshold] = threshold_number.to_s
  ea[:number_scans] = '10'
  ea[:points] = []
  freq = 1000
  monplan_length.times do |i|
    ea[:points] << create_point(freq)
    freq += 50
  end
  
  return ea
end

#Create a hash that we'll convert to xml later
enhanced_alarms = {}
enhanced_alarms[:monplan_major] = '1'
enhanced_alarms[:monplan_minor] = '0'
enhanced_alarms[:enhanced_alarms] = []
#Number of thresholds to create. Initially this was four, but it took forever for the sim to load the data so scaled it back to one shared by all thresholds. 
1.times do |i|
  enhanced_alarms[:enhanced_alarms] <<  create_enhanced_alarm(i, 241)
end

#Write data to file
File.open("enhanced_alarms.json", "w") do |f|
   #f.write(enhanced_alarms.to_xml(:root => 'EnhancedAlarms'))
   f.write(enhanced_alarms.to_json)
end


    
