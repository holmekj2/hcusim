#!/usr/bin/ruby
require 'fileutils'

begin
  if ARGV.length != 1
    raise ArgumentError
  end
  FileUtils.cp('/etc/network/interfaces.static', '/etc/network/interfaces')
  file = File.open('/etc/network/interfaces', "r")
  #file = File.open("interfaces", "r")
  #Read entire file to string
  s = file.read
  file.close
  #puts s
  s.gsub!(/^address.*/, "address " + ARGV[0])
  file = File.open('/etc/network/interfaces', "w")
  #file = File.open("interfaces", "w")
  file.write(s)
  system("shutdown -r 0")
rescue ArgumentError
  puts "Usage change_ip_permanent newip"
rescue
  puts "Error accessing file. This must be run as sudo!!!"
  if file != nil
    file.close
  end
end

