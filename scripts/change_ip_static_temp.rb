#!/usr/bin/ruby
puts "THIS MUST BE RUN AS SUDO!!!"
begin
  if ARGV.length != 1
    raise ArgumentError
  end
  system("ifconfig eth1 " + ARGV[0])  
rescue ArgumentError
  puts "Usage change_ip_permanent newip"
rescue
  puts "This must be run as sudo!!!"
end

