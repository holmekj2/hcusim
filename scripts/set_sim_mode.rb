#!/usr/bin/ruby
#set_sim_mode.rb

$run = true
def show_menu
  puts 
  puts "1. Base HCU sim"
  puts "2. RPC overrides sim"
  puts "Q. Quit"
  puts
end

def base_sim
  puts "Setting sim to base"
  system('git checkout master')
end

def override_sim
  puts "Setting sim to rpc overrides"
  system('git checkout origin/RpcOverrides')
end

def get_user_input
  print "Enter selection: "
  selection = gets
  selection.chomp!
  case selection 
  when '1'
    base_sim
  when '2'
    override_sim
  when 'q', 'Q'
    puts "Exiting"
    $run = false
  else
    puts "Unrecognized command"
    show_menu
  end
end

show_menu
while $run
  get_user_input
end
