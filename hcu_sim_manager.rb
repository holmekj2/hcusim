require 'rubygems'
require 'orderedhash'
require 'sim_factory'
require 'hcu'

Signal.trap("TERM") do
  HcuSimManager.shutdown
end

Signal.trap("INT") do
  HcuSimManager.shutdown
end

class HcuSimException < Exception
end

#test only
# class HcuSim
  # attr_reader :rpc_port, :sim_pid, :drb_port, :log_port
  # def initialize(number)
    # @number
    # @running = false
    # @sim_pid = 0
    # @rpc_port = number + 50000
    # @drb_port = number + 60000
    # @log_port = number + 25000
  # end
  # def start
    # @running = true
    # @sim_pid = @number
  # end
  # def stop
    # @running = false
    # @sim_pid = 0
  # end
  # def is_running?
    # @running
  # end
# end

module HcuSimManager
  database = "database/hcusim.sqlite3"
  ActiveRecord::Base.establish_connection(:adapter => "sqlite3",:database  => database,:pool => 5,:timeout => 5000)
  version = "0.4.0"

  def self.init
    @hcus = OrderedHash.new
    Hcuconfig.all.map{|h| @hcus[h.id] = HcuSim.new(h.id)}
  end

  def self.start(number)
    validate_sim(number)
    validate_already_running(number)
    #Start a new sim in a new thread and wait a couple of seconds for it to start up
    @hcus[number].start
    sleep(1)
    validate_running(number)
  end

  def self.stop(number)
    validate_sim(number)
    if @hcus[number].is_running?
      @hcus[number].stop
      sleep(1)
    end
  end

  def self.list_sims
    all = OrderedHash.new
    @hcus.each do |k, v|
      all[k] = {:number => k, :running => v.is_running?, :sim_pid => v.sim_pid, :rpc_port => v.rpc_port, :drb_port => v.drb_port, :remote_log_port => v.log_port}
    end
    all
  end

  def self.get_running
    @hcus.select{|k,v| v.is_running?}
  end

  def self.get_available
    @hcus.select{|k,v| !v.is_running?}
  end

  def self.get_drb_port(number)
    return hcu[number].drb_port
  end

  def self.is_running?(hcu_number)
    @hcus[hcu_number].is_running?
  end

  private
  def self.validate_sim(number)
    if !@hcus.has_key?(number)
      raise HcuSimException, "Simulator #{number} does not exist"
    end
  end

  def self.validate_running(number)
    if !@hcus[number].is_running?
      raise HcuSimException, "Simulator #{number} is not running"
    end
  end

  def self.validate_already_running(number)
    if @hcus[number].is_running?
      raise HcuSimException, "Simulator #{number} is already running"
    end
  end

  def self.shutdown
    puts "Shutting down Hcu simulators..."
    #Send a -9. 9 indicates a kill signal. Negative indicates kill off the process group
    Process.kill(-9, 0)
  end

end

require 'hcu_sim_manager'

module HcuSimProcessor
  @commands = {}
  def self.commands
    @commands
  end
  def self.register(name, mod)
    @commands[name.downcase] = mod
  end
  def self.process(user_command_string)
    args = user_command_string.split
    args = args.map{|s| s.downcase}
    user_command = args.shift
    if user_command == 'help'
      help
    elsif !@commands.has_key?(user_command)
      puts "Unknown command"
    elsif args[-1] == 'help'
      @commands[user_command].new.help(args)
    else
      @commands[user_command].new.process(args)
    end
  end
  def self.help
    @commands.each_key do |c|
      puts c
    end
  end
end

module ParseUtils
  #Returns an array based on a string representing an array, range, or integer. Returns nil if any of these patterns are not patched
  def self.convert_arg_to_int_array(s)
    array = nil
    case s
    #match on a string representing a rang
    when /(\d+)\.\.(\d+)/
      range = $1.to_i..$2.to_i
      array = range.to_a
    when  /\[(.*)\]/
      array = []
      $1.split(',').map{|e| array <<  e.to_i}
    when /(\d+)/
      array = []
      array << $1.to_i
    end
    array
  end
end

#A mixin for any command group (See List)
module CommandGroup
  def process(args)
    command = args.shift
    if command == nil or !self.respond_to?(command)
      puts "Unknown command #{command}"
      puts self.help(nil)
    else
      self.send(command, args)
    end
  end
end

#Add manager commands in this file
class Start
  def process(args)
    if args[0] == 'all'
      start_all
    elsif
      hcus = ParseUtils.convert_arg_to_int_array(args[0])
      start_multi(hcus) if hcus != nil
    else
      puts "Invalid arguments"
      help(args)
    end
  end

  def help(args)
    puts 'usage: start 1, start [1,3,4], start 1..20, start all'
  end

  private
  def start_all
    HcuSimManager.get_available.each do |h|
      start(h[0])
    end
  end
  def start_multi(hcus)
    hcus.each {|h| start(h)}
  end
  def start(hcu)
    begin
      HcuSimManager.start(hcu)
      puts "Started simulator #{hcu}"
    rescue HcuSimException => e
      print "Could not start simulator #{hcu}: "
      puts e.message
    end
  end
end
HcuSimProcessor.register('start', Start)

class Stop
  def process(args)
    if args[0] == 'all'
      stop_all
    elsif
      hcus = ParseUtils.convert_arg_to_int_array(args[0])
      stop_multi(hcus) if hcus != nil
    else
      puts "Invalid arguments"
      help(args)
    end
  end
  def help(args)
    puts 'usage: stop 1, stop [1,3,4], stop 1..20, stop all'
  end
  private
  def stop_all
    HcuSimManager.get_running.each do |h|
      stop(h[0])
    end
  end
  def stop_multi(hcus)
    hcus.each {|h| stop(h)}
  end
  def stop(hcu)
    begin
      HcuSimManager.stop(hcu)
      puts "Stopped simulator #{hcu}"
    rescue HcuSimException => e
      print "Could not stop simulator #{hcu}: "
      puts e.message
    end
  end
end
HcuSimProcessor.register('stop', Stop)


#A command group
class List
  include CommandGroup
  def all(args)
    p HcuSimManager.list_sims
  end
  def running(args)
    all = HcuSimManager.list_sims
    r = []
    all.each{|k,v| r << k if v[:running]}
    p r
  end
  def available(args)
    all = HcuSimManager.list_sims
    a = []
    all.each{|k, v| a << k if !v[:running]}
    p a
  end
  def help(args)
    puts "usage: list all, list running, list available"
  end
end
HcuSimProcessor.register('list', List)

class ViewLog
  def process(args)
    if args[0] == 'all'
      stop_all
    elsif
      hcus = ParseUtils.convert_arg_to_int_array(args[0])
      log_multi(hcus) if hcus != nil
    else
      puts "Invalid arguments"
      help(args)
    end
  end

  private
  def log_all
    HcuSimManager.get_running.each do |h|
      log(h[0])
    end
  end
  def log_multi(hcus)
    hcus.each {|h| log(h)}
  end
  def log(hcu)
    if HcuSimManager.is_running?(hcu)
      cmd = "gnome-terminal --title Hcu#{hcu} -e 'tail -f logs/Hcu#{hcu}.log'"
      begin
        system(cmd)
      rescue
      end
    else
      puts "Simulator #{hcu} not running "
    end
  end
  def help(args)
    puts "open a new terminal with tail -f of HCU's log file"
    puts 'usage: log 1, log [1,3,4], log 1..20, log all'
  end
end
HcuSimProcessor.register('log', ViewLog)

class Quit
  def process(args)
    HcuSimManager.shutdown
  end

  def help(args)
  end
end
HcuSimProcessor.register('quit', Quit)


HcuSimManager.init
loop do
  print "HcuSimManager>"
  STDOUT.flush
  s = gets
  HcuSimProcessor.process(s)
end

