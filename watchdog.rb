require 'event'
require 'log'

class Watchdog
  attr_reader :tid
  def initialize(hcu_uid, update_in_seconds)
    @update_in_seconds = update_in_seconds
    @hcu_uid = hcu_uid
    if @update_in_seconds != nil and @update_in_seconds != 0 
      @tid = Thread.new{startwd()}
    else
      Log::GeneralLogger.warn "No watchdog interval found"
    end
  end
  def set_update(update_in_seconds)
    @update_in_seconds = update_in_seconds
    #Kill off the current update thread and restart based on the new update time
    begin
      Thread.kill(@tid)
    rescue
      #Don't do anything if the thread hasn't already been started
    end
    @tid = Thread.new{update}    
  end
private
  def startwd
    Log::GeneralLogger.debug "Watchdog update thread started"
    while true
      #Send the watchdog event with a payload indicating when the next update will be. 
      payload = [Time.now.to_i + @update_in_seconds, 0]
      Event.new(@hcu_uid, EventIds::EVENT_HCU_WATCHDOG, EventIds::EVENT_PRIORITY_HIGH, payload)
      sleep(@update_in_seconds)
      Log::GeneralLogger.debug "Watchdog event sent"
    end
  end  
end







