require 'active_record'
require 'rpm'

#Database accessor (ORM)
class Portconfig < ActiveRecord::Base
  belongs_to :rpmconfig
end

class Port < DelegateClass(Portconfig)
  #Accept an active record (RpmConfig). We delegate all the parameters to the active record so we don't have to redefine all the parameters in this class.  
  def initialize(portconfig)
    @portconfig = portconfig
    #This initializes the delegation for the config info
    super(@portconfig)
  end
end
