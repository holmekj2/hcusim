hcus = 60004..60004
rpms = 1..15
ports = 1..8
thresholds = 1..4
hcus.each do |h|
  rpms.each do |r|
    ports.each do |p|
      thresholds.each do |t|
        s = "ruby threshold_event.rb #{h} #{r} #{p} #{t} 1"
        puts s
        system s
      end
    end
  end
end
