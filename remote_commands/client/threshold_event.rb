require 'drb'

def print_usage()
  puts 'threshold_event drbport rpm_number(1-based) port_number(1-based) threshold_number(1-4) unlatched=0/latched=1'
  puts 'threshold_event drbport port_label threshold_number(1-4) unlatched=0/latched=1'
  puts 'threshold_event drbport file'  
end

def send_event(rpm_number, port_number, threshold_number, latched)
  event_server = DRbObject.new nil, "druby://:#{$drb_port}"
  event_server.threshold_event(rpm_number, port_number, threshold_number, latched)
  #puts "send_event: ", uid, " ", event_id, " ", payload
end

def process_event_by_number(rpm_number, port_number, threshold_number, latched)
  send_event(rpm_number, port_number, threshold_number, latched)
end

def process_event_by_label(port_label, threshold_number, latched)
  event_server = DRbObject.new nil, "druby://:#{$drb_port}"
  rv = event_server.threshold_event_by_port_label(port_label, threshold_number, latched)
  puts rv if !rv.nil?
    
end

def process_event_file_line(line)
  r = []
  sl = line.split()
  #only parse non-empty lines
  if sl.length > 1
    sl.each do |s|
      r << Integer(s)
    end
  end
end

#event file should have entries of the format
#uid event_id payload1 payload2 ...
def process_event_file(filename)
  begin
    File.open(filename, "r").each do |line|
      args = process_event_file_line(line)      
      if !args.nil?
        send_event(args[0], args[1], args[2], args[3])      
      end
    end
  rescue ArgumentError
    puts "ex1"  
    puts "Invalid event file"
  rescue
    puts "ex2"
    print_usage()
  end
end

$drb_port = 9000
#One argument means user wants us to read from file
if ARGV.length == 2
  $drb_port = ARGV[0]
  process_event_file(ARGV[1])  
elsif ARGV.length == 4
  $drb_port = ARGV[0]
  process_event_by_label(ARGV[1], ARGV[2].to_i, ARGV[3].to_i)  
elsif ARGV.length == 5
  $drb_port = ARGV[0]
  process_event_by_number(ARGV[1].to_i, ARGV[2].to_i, ARGV[3].to_i, ARGV[4].to_i)  
else
  print_usage()
end

