require 'drb'

def print_usage()
  puts 'port_uid drbport rpm_number port_number'
end

def port_uid(drb_port, rpm_number, port_number)
  server = DRbObject.new nil, "druby://:#{drb_port}"
  puts server.get_port_uid(rpm_number, port_number)
end

drb_port = 9000
#One argument means user wants us to read from file
if ARGV.length == 0
  print_usage()
else
  drb_port = ARGV[0]  
  rpm_number = ARGV[1].to_i
  port_number = ARGV[2].to_i
end
port_uid(drb_port, rpm_number, port_number)

