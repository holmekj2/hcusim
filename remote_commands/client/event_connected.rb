require 'drb'

def print_usage()
  puts 'event_connected drbport'
end

def event_connected?
  rc_server = DRbObject.new nil, "druby://:#{$drb_port}"
  connected = rc_server.event_connected?
  puts "Event interface connected: #{connected}"
end

$drb_port = 9000
#One argument means user wants us to read from file
if ARGV.length == 1
  $drb_port = ARGV[0]
  event_connected?
else
  print_usage()
end

