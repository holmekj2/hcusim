require 'drb'

def print_usage()
  puts 'print_cfg drbport [extended]'
end

def print_cfg(drb_port, extended)
  server = DRbObject.new nil, "druby://:#{drb_port}"
  server.print_cfg(extended)  
end

drb_port = 9000
#One argument means user wants us to read from file
extended = 1
if ARGV.length == 0
  print_usage()
else
  drb_port = ARGV[0]  
  extended = ARGV[1]
end
print_cfg(drb_port, extended)

