require 'drb'

def print_usage()
  puts 'event_remote_command drbport [file | uid, event_id, payload1, ..., payload5]'
end

def send_event(uid, event_id, payload)
  event_server = DRbObject.new nil, "druby://:#{$drb_port}"
  event_server.new_event(uid, event_id, payload)
  #puts "send_event: ", uid, " ", event_id, " ", payload
end

def process_event_command_line(uid, event_id, payload)
  send_event(uid, event_id, payload)
end

def process_event_file_line(line)
  r = []
  sl = line.split()
  #only parse non-empty lines
  if sl.length > 1
    sl.each do |s|
      r << Integer(s)
    end
  end
end

#event file should have entries of the format
#uid event_id payload1 payload2 ...
def process_event_file(filename)
  begin
    File.open(filename, "r").each do |line|
      args = process_event_file_line(line)      
      if !args.nil?
        send_event(args[0], args[1], args[2..-1])      
      end
    end
  rescue ArgumentError
    puts "ex1"  
    puts "Invalid event file"
  rescue
    puts "ex2"
    print_usage()
  end
end

$drb_port = 9000
#One argument means user wants us to read from file
if ARGV.length == 2
  $drb_port = ARGV[0]
  process_event_file(ARGV[1])  
#Up to five payload arguments may be passed  
elsif ARGV.length > 2 and ARGV.length <= 8 
  $drb_port = ARGV[0]
  process_event_command_line(ARGV[1], ARGV[2], ARGV[3..-1])  
else
  print_usage()
end

