require 'event'
require 'hcu_factory'
require 'port'
require 'drb'
require 'log'

class RemoteCommandServer
  #Send new event. 
  #Device uid
  #Event ID
  #payload array
  def new_event(uid, event_id, payload)
    Event.new(uid, event_id, EventIds::EVENT_PRIORITY_LOW, payload) 
  end

  def threshold_event(rpm_number, port_number, threshold_number, latched)
    port_uid = get_port_uid(rpm_number, port_number)
    payload = []
    payload << threshold_number - 1
    payload << latched
    payload << 1 #Threshold stats are always available on the sim
    Event.new(port_uid, EventIds::EVENT_RPM1000_LIM_VIOL, EventIds::EVENT_PRIORITY_MEDIUM, payload) 
  end

  def event_connected?
    HcuFactory.get_event_sender.connected?
  end
  def threshold_event_by_port_label(port_label, threshold_number, latched)
    rv = nil
    port = Portconfig.find_by_label(port_label)
    if port.nil?
      rv = "threshold_event_by_port_label cannot find port #{port_label}"
      Log::GeneralLogger.error rv
      return rv
    end
    payload = []
    payload << threshold_number - 1
    payload << latched
    payload << 1 #Threshold stats are always available on the sim
    Event.new(port.uid, EventIds::EVENT_RPM1000_LIM_VIOL, EventIds::EVENT_PRIORITY_MEDIUM, payload) 
    rv
  end
  
  def print_cfg(extended)
    HcuFactory.get_hcu.print_cfg(extended)
  end
  
  #Both 1-based
  def get_port_uid(rpm_number, port_number)
    HcuFactory.get_hcu.rpm_delegator.rpms[rpm_number].ports[port_number-1].uid
  end

end

