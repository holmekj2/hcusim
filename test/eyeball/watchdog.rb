require 'watchdog'

def nominal_case
  puts "#{Thread.list}"
  w = Watchdog.new(1, 5)
  puts "#{Thread.list}"
  sleep(12)
  w.set_update(2)
  puts "#{Thread.list}"
  t = w.tid
  t.join
end

def empty_update
  puts "#{Thread.list}"
  w = Watchdog.new(1, 0)
  puts "#{Thread.list}"
  sleep(7)
  w.set_update(2)
  puts "#{Thread.list}"
  t = w.tid
  t.join
end

empty_update
