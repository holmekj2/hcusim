require 'command_shell'

class Test1
  def hello1
    puts "Hello1"
  end
  def user_hello(x)
    puts "User: x"
  end
end

class Test2
  def hello1
    puts "Test2::Hello1"
  end
  def hello2
    puts "Hello2"
  end
  def user_hello(x)
    puts "User: x"
  end
end

def send_test
  test1 = Test1.new
  test2 = Test2.new
  chain = []
  chain << test1
  chain << test2
  cs = CommandShell.new(chain)
  cs.help
  cs.send_message(:hello3)
end

def parse_test
  chain = []
  cs = CommandShell.new(chain)
  cs.shell
end

parse_test
