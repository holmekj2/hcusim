require 'rubygems'
require 'test/unit'
require 'Rpc/Intercepter/rpc_override'

class TestEventReceiver < Test::Unit::TestCase
  def setup 
    @rpc_override = RpcOverride.new('test/unit/override_test_data.txt')
  end
  
  def teardown
  end
  
  def test_respond_to
    y1 = {:method => 'rpm1000cfgputstate', :rpm => 2, :port => 3}
    y2 = {:method => 'hcugetslotinfo'}
    assert(@rpc_override.respond_to?(y1))  
    assert(@rpc_override.respond_to?(y2))      
    n1 = {:method => 'rpm1000cfgputstate', :rpm => 2, :port => 2}
    n2 = {:method => 'pm1000cfgputstate', :rpm => 2, :port => 3}
    n3 = {:method => 'rpm1000cfgputstate', :rpm => 3, :port => 3}
    n4 = {:method => 'rpm1000cfgputstate', :rpm => 3, :port => 4}
    assert(!@rpc_override.respond_to?(n1))  
    assert(!@rpc_override.respond_to?(n2))  
    assert(!@rpc_override.respond_to?(n3))  
    assert(!@rpc_override.respond_to?(n4))          
  end
  def test_returns
    m1 = {:method => 'rpm1000cfgputstate', :rpm => 2, :port => 3}  
    assert(@rpc_override.respond_to?(m1))      
    ret = @rpc_override.send(m1[:method], m1)
    assert_equal(ret[:ulReturn], 0x80041001)
    m2 = {:method => 'hcugetslotinfo'}    
    assert(@rpc_override.respond_to?(m2))      
    ret = @rpc_override.send(m2[:method], m2)
    assert_equal(ret[:ulReturn], 0x80041001)
    assert_equal(ret[:ulRpm], 1)
  end
  def test_number_fails
    m1 = {:method => 'rpm1000cfgputstate', :rpm => 2, :port => 3}  
    assert(@rpc_override.respond_to?(m1))   
    ret = @rpc_override.send(m1[:method], m1)
    assert_equal(ret[:ulReturn], 0x80041001) 
    #Need to redefine the hash since the rpc_override will manipulate it
    m1 = {:method => 'rpm1000cfgputstate', :rpm => 2, :port => 3}      
    assert(@rpc_override.respond_to?(m1))      
    ret = @rpc_override.send(m1[:method], m1)    
    assert_equal(ret[:ulReturn], 0x80041001)    
    m1 = {:method => 'rpm1000cfgputstate', :rpm => 2, :port => 3}      
    assert(!@rpc_override.respond_to?(m1))              
  end
  def test_types
    m1 = {:method=>'test_method'} 
    assert(@rpc_override.respond_to?(m1))    
    ret = @rpc_override.send(m1[:method], m1)
    assert_equal(ret[:floatt], 1.6)     
    assert_equal(ret[:stringt], 'string')     
    assert_equal(ret[:intt], 1)     
    assert_equal(ret[:hext], 0x1234)                 
  end
  
end
 
