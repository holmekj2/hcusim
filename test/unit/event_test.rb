require 'event'
require 'test/unit'

class TestEventReceiver < Test::Unit::TestCase
  def setup 
    @event_queue = []
  end
  
  def teardown
    #Event.remove_observer(self)
  end
  
  def test_event_creation
    event = Event.new(1, 2, [3,4,5], :low)
    assert_equal(1, event.source_uid)
    assert_equal(2, event.event_type)    
    assert_equal([3,4,5], event.payload)
    assert_equal(:low, event.priority)        
  end
  
  #Test the creation and formation of events
  def test_event_observer
    Event.add_observer(self)  
    #Event.new(source_uid, event_type, payload, priority)
    events = []
    events << Event.new(1,2,[3,4,5],0)
    events << Event.new(2,3,[4,5,6,7,8],1)
    events << Event.new(3,4,nil,2)
    #At this point, update should have been called three times.
    @event_queue.reverse_each do |e|
      assert_equal(events.pop, e)
    end        
  end
  
  #Test socket side of things
  def test_event_send
  end
  
  #Callback for observer
  def update(event)
    @event_queue << event
  end
end

#Helper class for test_event_send
#class EventReceiver
#  def initialize(port)
#    #Open socket server to allow new connections. 
#    @server = TCPServer.open(myport)
#    #Spawn new thread to listen for connection and receive data 
#    listener = Thread.new{listen}
#  end

#  def listen
#    begin
#        #Wait for connection
#        socket = @server.accept
#        @connected = true
#        while @connected
          #Wait for incoming data
#          sequence_number, event = read_message(socket)
          #There's a better way to do this but I only want to send an ack if the event is properly queued
#          if status == 0
#            send_acknowledge(socket, sequence_number)
#          end
#        end
#    rescue
#      puts "Error in event_driver::listen"
#    end
#  end
#end



