require 'yaml'

class RpmFeatures
  attr_reader :features
  def initialize
    @qamtrak = {:name => "QAMTrak", :state => "Permanent", :days_remaining => "N/A"}
    @mactrak = {:name => "MACTrak", :state => "Evaluation", :days_remaining => "30"}    
    @spectrum_view = {:name => "Spectrum View", :state => "Disabled", :days_remaining => "N/A"}        
    @features = [@qamtrak, @mactrak, @spectrum_view]
  end
end
