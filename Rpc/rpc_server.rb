require 'yaml'

class RpcReceiver
  attr_accessor :connected
  #rpc_handler is a chain of possible handlers
  def initialize(rpc_handlers, cmd_pipe, rsp_pipe)
    @connected = true
    @cmd_pipe = cmd_pipe
    @rsp_pipe = rsp_pipe
    #The rpc_handler input can be an array or just a single object. 
    @rpc_handlers = []
    @rpc_handlers << rpc_handlers
    #Flatten the array in case the input was a list
    @rpc_handlers.flatten!
  end
  def wait_for_messages()
    while @connected
      #The passer puts the length of data first (in a string) and then the actual data      
      length = @cmd_pipe.gets
      message = @cmd_pipe.read(length.to_i) 
      #De-yaml the data. 
      args = YAML.load(message)
      #args is a hash. The method call is always located with the :method key
      Log::GeneralLogger.debug "Rpc message received: #{args[:method]}, args: #{args}"
      ret = nil
      @rpc_handlers.each do |rpc_handler|
        #Check the chain of handlers for the first one to handle the requested method. The RPC override
        #method uses a special function (handles?) to determine if it responds_to a method because it checks
        #to see if it handles the response for the method and also the RPM and Port
        ret = rpc_handler.send args[:method], args
        break if ret != nil 
      end
      
      #Serialize the data for pipe transfer
      yret = YAML.dump(ret)
      #First write the length so the receiver knows how much data to grab
      @rsp_pipe.puts yret.length.to_s   
      #Send the data
      @rsp_pipe.write yret
        Log::GeneralLogger.debug "Rpc message returned: #{ret}"
    end
  end
end

class RpcPasser
  #Create a new receiver for RPC server. 
  #rpc_interface is the class singleton object for the RPC interface (C extension). 
  #cmd_pipe pipe to forward incoming rpc messages 
  #rsp_pipe pipe to receive responses on
  #These pipes are the channel to get back and forth bewteen the parent process (HCU sim application) and child process (RPC process)
  def initialize(rpc_interface, socket_port, cmd_pipe, rsp_pipe)
    @rpc_interface = rpc_interface.new(self)  
    @read_pipe = rsp_pipe
    @write_pipe = cmd_pipe
    @socket_port = socket_port
  end
  def start_service
    @rpc_interface.start_rpc_services(@socket_port)
  end
  def rpc_call(args)
    #Serialize the data for pipe transfer
    yargs = YAML.dump(args)
    #First write the length so the receiver knows how much data to grab
    @write_pipe.puts yargs.length.to_s   
    #Send the data
    @write_pipe.write yargs
    #Wait for return
    #The passer puts the length of data first (in a string) and then the actual data      
    length = @read_pipe.gets
    message = @read_pipe.read(length.to_i) 
    #De-yaml the data and return it
    args = YAML.load(message)
  end
end

class RpcServer
  #rpc_interface is the class singleton object for the RPC interface (C extension). 
  #rpc_handler is the object that will handle the RPC calls
  def initialize(rpc_interface, rpc_handler, socket_port)
    @rpc_interface = rpc_interface
    @rpc_handler = rpc_handler
    @socket_port = socket_port
  end  
  def start
    Log::GeneralLogger.debug "Starting RPC server on port #{@socket_port}"
    cmdrd, cmdwr = IO.pipe
    rsprd, rspwr = IO.pipe
    #We can't use a thread here because ruby thread scheduler will not task switch while in c. must use a fork
    pid = fork
    if pid == nil
      #Child process
      #TODO Need to figure out how to clean up this process upon parent process termination
      begin 
        cmdrd.close
        rspwr.close
        rpcPasser = RpcPasser.new(@rpc_interface, @socket_port, cmdwr, rsprd)
        rpcPasser.start_service
      rescue Exception
        Kernel.exit
      end
    else
      #Parent process      
      @child_pid = pid
      rpcReceiver = RpcReceiver.new(@rpc_handler, cmdrd, rspwr)    
      rpcReceiver.wait_for_messages
      Process.wait
      puts "RPC process killed"
    end
  end
  def shutdown
    Process.kill(9, @child_pid)
  end
end
