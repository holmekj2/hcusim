module RpcUtils
  def validate_string(s)
    if s == nil
      return ""
    end
    return s
  end
  #If the input is nil return 0 otherwise return the input
  def validate_int(i)
    return i ? i.to_i : 0
  end
  def validate_float(f)
    return f ? f.to_f : 0.0  
  end
end
