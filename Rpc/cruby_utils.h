#ifndef _CRUBYUTILS_H
#define _CRUBYUTILS_H

//C - Ruby helpers
#define SYM2CSTR(sym) rb_id2name(SYM2ID(sym))
#define CSTR2SYM(str) ID2SYM(rb_intern(str))
#define SETHASHMETHOD(hash, method) rb_hash_aset(hash, CSTR2SYM("method"), rb_str_new2(method))

#endif
