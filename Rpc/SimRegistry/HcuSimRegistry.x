program HCUSIMREGISTRYSVR {
	version HCUSIMREGISTRYSVRVERS { 
               /* Register the IP and port of a sim. Used by sim. */
               int registerSim(int) = 1;
               /* Get a sim port given its IP. Return -1 if sim at ip is not found */
               int getSimPort(string) = 2;
    } = 1;
} = 0x200000ff;
