/*
 * Please do not edit this file.
 * It was generated using rpcgen.
 */

#ifndef _HCUSIMREGISTRY_H_RPCGEN
#define _HCUSIMREGISTRY_H_RPCGEN

#include <rpc/rpc.h>


#ifdef __cplusplus
extern "C" {
#endif


#define HCUSIMREGISTRYSVR 0x200000ff
#define HCUSIMREGISTRYSVRVERS 1

#if defined(__STDC__) || defined(__cplusplus)
#define registerSim 1
extern  int * registersim_1(int *, CLIENT *);
extern  int * registersim_1_svc(int *, struct svc_req *);
#define getSimPort 2
extern  int * getsimport_1(char **, CLIENT *);
extern  int * getsimport_1_svc(char **, struct svc_req *);
extern int hcusimregistrysvr_1_freeresult (SVCXPRT *, xdrproc_t, caddr_t);

#else /* K&R C */
#define registerSim 1
extern  int * registersim_1();
extern  int * registersim_1_svc();
#define getSimPort 2
extern  int * getsimport_1();
extern  int * getsimport_1_svc();
extern int hcusimregistrysvr_1_freeresult ();
#endif /* K&R C */

#ifdef __cplusplus
}
#endif

#endif /* !_HCUSIMREGISTRY_H_RPCGEN */
