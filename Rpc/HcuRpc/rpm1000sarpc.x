/*
    .x file for the RPM1000 Spectrum Analyzer RPC Server
*/

 const RPM_SA_MAX_DATA_PNTS         =   500;
 const RPM_SA_MAX_PACKED_VALS       =   250;
 const RPM_DEMOD_MAX_DATA_PNTS = 512;
 const RPM_FFT_MAX_DATA_PNTS   = 1024;
 const RPM_EQ_MAX_DATA_PNTS    =  64;

const MAX_PREAMBLE_LENGTH_BYTES_RPC = 384;
const MAX_PREAMBLES_PER_PROFILE_RPC = 2;
const MAX_IUCS_PER_PROFILE_RPC = 5;

struct Rpm1000SACnctInfo
{
    unsigned short usRpmId;
    unsigned short usPortId;
    char strSCIpAddr [20];
    unsigned short usSocketPort;
};

struct Rpm1000SATrcReq
{
    unsigned short usRpmId;
    unsigned short usPortId;
    unsigned short usSocketPort;   /* used to identify specific SA connection */
    unsigned long  ulRequestId;
    unsigned long  ulConfigVersId;
    int            nTraceMode;
    double         dwCenterFreq;
    double         dwFreqSpan;
    double         dwStartFreq;
    double         dwStopFreq;
    double         dwDesiredReflevel;
    double         dwActualRefLevel;
    int            nRBW;
    int            nVBW;
    int            nDetMode;
    unsigned long  ulDwellTime;
    int            nTrigMode;
    double         dwTrigLevel;
    unsigned long  ulTrigDelay;
    unsigned long  ulTimeSpan;
    unsigned long  ulInterval;
};

struct Rpm1000SAGenInfo
{
    unsigned short usRpmId;
    unsigned short usPortId;
    unsigned short usSocketPort;   /* used to identify specific SA connection */
};


struct Rpm1000SAPackedTrace
{

    unsigned long ulMsgSeqNum;
    int             nDataValid;
    int             nUncal;
    double          dwTimeSpan;
    double          dwTestPntComp;
    Rpm1000SATrcReq Config;
    double          dwXStart;
    double          dwXStep;
    double          dwXRes;
    double          dwYBase;
    double          dwYStep;
	unsigned short  usNumPoints;
    unsigned long   ulPackedVals<RPM_SA_MAX_PACKED_VALS>;
};



/* Server->HCU Demod request structure */
struct Rpm3000DemodReq
{
    unsigned short usRpmId; /* 1 - 15 */
    unsigned short usPortId; /*0 - 7*/
    unsigned short usSocketPort; /*Port to return data on*/
    unsigned long ulRequestId; /*ID of this request*/
    unsigned long frequency; /*Hz*/
    short carrierLevel; /*dBmV * 100 */
    unsigned short symbolRate; /*MBPS * 1000 */
    unsigned short attenuation; /*0-50, Auto (0xffff) */
    unsigned short mode; /*Auto=0, QPSK=1, QAM16=2, QAM64=3, QAM32=4 */
    unsigned long timeout; /*Trigger timeout (us) */
    unsigned long interval; /*Interval between demod requests (us)*/
};

struct Rpm3000DemodReq_Channel
{
    unsigned short usRpmId; /* 1 - 15 */
    unsigned short usPortId; /*0 - 7*/
    int            channelId; /* From unique value from an hcu generated table */
    unsigned short usSocketPort; /*Port to return data on*/
    unsigned long ulRequestId; /*ID of this request*/
    unsigned long timeout; /*Trigger timeout (us) */
    unsigned long interval; /*Interval between demod requests (us)*/
    unsigned long mode;
};

struct ConstellationReturnData
{
    short numErredSymbols;         /*Num of symbols with errors */
    /*I,Q points normalized to [-32768, 32767]. A set of i,q pts are packed */
    /*in a unsigned 32 word as i -> bits 31 - 16, q -> 15 - 0. */
    /*RPM Multiplies demod output by 2^14 to normalize data */
    long constellationData<RPM_DEMOD_MAX_DATA_PNTS>;
};

struct FftReturnData
{
    unsigned long startFreq; /*Hz */
    unsigned long stepFreq; /*Hz */
    /*FFT level data in dBmV * 100. Two 16 bit signed levels are packed into a long word as */
    /*data point 0 fftData[0] bits 0 - 15, data point 1 fftData[0] bits 31 - 16, data point 2 fftData[1] bits 0 - 15, ... */
    long fftData<RPM_FFT_MAX_DATA_PNTS>;
};

struct EqReturnData
{
    /*Equalizer data – future use Tap range [-1, 1] normalized by 2^15. Two 16 bit words are packed into a long word as   */
    /*data point 0 eqData[0] bits 0 - 15, data point 1 eqData[0] bits 31 - 16, data point 2 eqData[1] bits 0 - 15, ... */
    long eqData<RPM_EQ_MAX_DATA_PNTS>;
};

struct ImpulseNoisePeaks
{
    int symbolPosition; /* [0, packetLength_symbols] */
    float symbolError_dBc; /* Level in dBc */
};
struct ImpulseNoiseData
{
    int packetLength_symbols; /* Total symbols in packet */
    float averageSymbolError_dBc; /* Impulse noise floor dBc */
    float maxSymbolError_stdDeviations; /* Largest peak in std deviations */
    float maxSymbolError_dBc; /* Largest peak in dBc */
    ImpulseNoisePeaks peaks<>; /* Not sorted */
};

/* HCU->Server Demod data return structure */
struct Rpm3000DemodReturn
{
    unsigned long ulMsgSeqNum; /*Sequence number of demod returned data */
    unsigned long timeStamp; /*GMT Time stamp of when data is received. */
    Rpm3000DemodReq config; /*Associated configuration request this data corresponds to */
    short status; /*Demod status (0=OK, 1=timed out) */
    short carrierLevel; /*dBmV * 100 */
    short carrierOffset; /*Delta of expected and actual center freq in Hz */
    short symbolOffset; /*Delta of expected and actual symbol baud rate in Hz */
    short mer; /*Modulation error rate (dB * 100) */
    short mode; /*Modulation type (QPSK=1, QAM16=2, QAM64=3) */
    unsigned long macAddressLow; /*MAC address low 32 bits - Future use */
    unsigned long macAddressHigh; /*MAC address high 16 bits - Future use */
    ConstellationReturnData constellationData;
    FftReturnData fftData;
    EqReturnData eqData;
};
/* Version 2 created for HCU 2.5 */
struct Rpm3000DemodReturn_2
{
    struct Rpm3000DemodReturn   classic;

    short attenuation_db;

    unsigned short codewordErrorsUncorrectable;
    unsigned short codewordErrorsCorrectable;
    unsigned short codewordErrorsNone;
    short equalizedMer; /*Equalized MER (dB * 100). Only valid in MACTrak */
    ImpulseNoiseData impulseNoiseData;
};struct DetectedChannelInfo{    int                 channelId;    unsigned long       frequency_hz;    unsigned short      symbolRate_ksps;    short               modType<2>; /* Modulation type (QPSK=1, QAM16=2, QAM64=3, QAM32=4) */    short               carrierLevel_dbmv100;    bool                macTrakAvailable; /* Does this ch have valud Ucd? */};

struct DetectedChannels{    struct DetectedChannelInfo  channels<>;
    unsigned short              attenuation_db;
    double                      testpointComp_db;    unsigned long               status; /* status = INTF_ERR_PENDING 0x80043003 if server should poll again */
    /* 0 = all licensed, 0x80042021 = QamTrak not licensed,
    0x80042022 = MacTrak not licensed, 0x80042023 = MacTrak monitoring not licensed */
    unsigned long               licensing;};

struct DetectedChannelsWithPortInfo
{
	struct DetectedChannels channels;
	unsigned short          rpm;  /*  range: 1 - 16 */
	unsigned short          port; /* ramge: 0-7 */
	unsigned long           uid;  /* port uid as assigned by PathTrak */
    char                    portLabel[40];
};

struct DetectedChannelsWithPortInfoArray
{
	struct DetectedChannelsWithPortInfo channels<>;
};

struct RpmPort {
    unsigned short rpm;
    unsigned short port;
};

struct RpmPortWithClear {
    unsigned short rpm;
    unsigned short port;
    bool           clear;
};

struct DocsisPreamble_rpc
{
    unsigned short docsisType;
    char preamble[MAX_PREAMBLE_LENGTH_BYTES_RPC];
};

struct IntervalUsageCode_rpc
{
    unsigned short code;
    unsigned short docsisType;
    unsigned short modulationType;
    unsigned short diffEncodingEnabled;
    unsigned short fecT;
    unsigned short fecK;
    unsigned short scramblerSeed;
    unsigned short maxBurst;
    unsigned short guardTime;
    unsigned short lastCw;
    unsigned short scramblerEnabled;
    unsigned short preambleLength;
    unsigned short preambleOffset;
    unsigned short preambleType;
    unsigned short interleaverDepth;
    unsigned short interleaverBlockSize;
};


struct Rpm3000QamFieldviewReq {
	unsigned short usRpmId;       /* 1 - 15 */
    unsigned short usPortId;      /*0 - 7*/
    unsigned short usSocketPort;  /*Port to return data on*/
    unsigned long  ulRequestId;   /*ID of this request*/
    unsigned long  frequency;     /*Hz*/
    unsigned short symbolRate;    /*MBPS * 1000 */
    unsigned short mode;          /*Auto=0, QPSK=1, QAM16=2, QAM64=3, QAM32=4 */
    unsigned long  timeout;       /*Trigger timeout (us) */
    unsigned long  interval;      /*Interval between demod requests (us) (unused) */

	DocsisPreamble_rpc      docsisPreambleRpm[MAX_PREAMBLES_PER_PROFILE_RPC];
    IntervalUsageCode_rpc   intervalUsageCodeRpm[MAX_IUCS_PER_PROFILE_RPC];
    unsigned short          slotSize;


	unsigned long          packetLengthMinMiniSlots;
	unsigned long          packetLengthMaxMiniSlots;
};



program RPM1000SASVR {
  version RPM1000SASVRVERS {
    unsigned long      rpm1000SAConnect ( Rpm1000SACnctInfo )       =  1;
    unsigned long      rpm1000SADisconnect ( Rpm1000SAGenInfo )     =  2;
    unsigned long      rpm1000SAReqSnglTrace ( Rpm1000SATrcReq )    =  3;
    unsigned long      rpm1000SAAbortSnglTrace ( Rpm1000SAGenInfo)  =  4;
    unsigned long      rpm1000SAReqMultiTrace ( Rpm1000SATrcReq)    =  5;
    unsigned long      rpm1000SAHaltMultiTrace ( Rpm1000SAGenInfo ) =  6;

    unsigned long      rpm3000DemodConnect ( Rpm1000SACnctInfo )       =  7;
    unsigned long      rpm3000DemodDisconnect ( Rpm1000SAGenInfo )     =  8;
    unsigned long      rpm3000DemodReqSnglTrace ( Rpm3000DemodReq )    =  9;
    unsigned long      rpm3000DemodAbortSnglTrace ( Rpm1000SAGenInfo)  =  10;
    unsigned long      rpm3000DemodReqMultiTrace ( Rpm3000DemodReq)    =  11;
    unsigned long      rpm3000DemodHaltMultiTrace ( Rpm1000SAGenInfo ) =  12;
  } = 1;  version RPM1000SASVRVERS_2 {    struct DetectedChannels  rpm3000DemodConnect ( struct Rpm1000SACnctInfo )       =  7;
    unsigned long      rpm3000DemodReqMultiTrace ( Rpm3000DemodReq)    =  11;
    unsigned long              rpm3000DemodReqMultiTraceChannel ( struct Rpm3000DemodReq_Channel )    =  14;

    /* Ucd calls */
    unsigned long startUcdPortScan( struct RpmPortWithClear ) = 15;

    /* after calling startUcdPortScan, call getDetectedChannels until it returns ok */
    struct DetectedChannels getDetectedChannels( struct RpmPort )  = 16;  } = 2;

  version RPM1000SASVRVERS_3 {

	unsigned long              rpm3000QamFieldviewConnect( struct Rpm1000SACnctInfo )  = 17;
	unsigned long              rpm3000QamFieldviewReqMultiTrace( struct Rpm3000QamFieldviewReq ) = 18;
	unsigned long              rpm3000QamFieldviewDisconnect( struct Rpm1000SAGenInfo )  = 19;


	/*
		Note: return structure will have one entry for each active port. However, there
		may be 0 detected channels on that port.
	*/
	struct DetectedChannelsWithPortInfoArray getAllDetectedChannels ( void ) = 20;

  } = 3;
} = 0x20000007;

/*
    .x file for the RPM11000 Spectrum Analyzer socket data
    The contents of this file match the contents of the file:
        Rpm1000sa_X.h
*/

