require 'Rpc/HcuRpc/Rpc'
require 'Rpc/rpc_server'
require 'Rpc/HcuRpc/hcu_rpc_handler'
require 'Rpc/HcuRpc/rpmgen_rpc_handler'
require 'Rpc/HcuRpc/rpmsa_rpc_handler'
require 'Rpc/HcuRpc/archive_rpc_handler'
require 'Rpc/HcuRpc/hsm_rpc_handler'

class RpcHandler
  def initialize(hcu, rpm_delegator, hsm, event_sender, socket_port, secondary_rpc_handlers=nil)
    @hcu_rpc_handler = HcuRpcHandler.new(hcu, event_sender) 
    @rpmgen_rpc_handler = RpmgenRpcHandler.new(rpm_delegator) 
    @rpmsa_rpc_handler = RpmsaRpcHandler.new(rpm_delegator) 
    @archive_rpc_handler = ArchiveRpcHandler.new(hcu) 
    @hsm_rpc_handler = HsmRpcHandler.new(hsm) 
    #Create a chain of RPC handlers.
    rpc_handlers = [secondary_rpc_handlers, @hcu_rpc_handler, @rpmgen_rpc_handler, @rpmsa_rpc_handler, @hsm_rpc_handler, @archive_rpc_handler, self]
    #Flatten the array in case the input (secondary_rpc_handlers) was an array 
    rpc_handlers.flatten!
    @rpc_server = RpcServer.new(Rpc, rpc_handlers, socket_port)
  end
  def start
    @rpc_server.start
  end
  def shutdown
    @rpc_server.shutdown
  end

end
