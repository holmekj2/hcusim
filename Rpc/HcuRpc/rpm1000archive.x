/*
    .x file for the RPM1000 Archive Data RPC interface
*/

const RPM1000_ARCH_MAX_DATA_PNTS=500;

const SUMTRACE_MSGID        = 1;
const TIMETRACE_MSGID       = 2;
const MULTTRACE_MSGID       = 3;
const MULTDAYTIMEWIND_MSGID = 4;
const MULTFREQTRACE_MSGID   = 5;

struct Rpm1000ArchDirInfo_s {
	long Reserved;
	int nDataValid;
	unsigned long BeginTime;
	unsigned long EndTime;
	unsigned int uBeginMonPlanMajRev;
	unsigned int uEndMonPlanMajRev;
	unsigned long ulIntervalTime;
	unsigned int uNumDataPnts;
};
typedef Rpm1000ArchDirInfo_s Rpm1000ArchDirInfo;

struct Rpm1000RetArchDirInfo_s
{
    unsigned long ulReturn;
    Rpm1000ArchDirInfo dirInfo;
};
typedef Rpm1000RetArchDirInfo_s Rpm1000RetArchDirInfo;

struct Rpm1000ArchULong_s
{
	unsigned short usRpmId;
    unsigned short usPortId;
    unsigned long  ulULong;
};
typedef struct Rpm1000ArchULong_s Rpm1000ArchULong;

struct Rpm1000ArchLong_s
{
	unsigned short usRpmId;
    unsigned short usPortId;
    unsigned long  lLong;
};
typedef struct Rpm1000ArchLong_s Rpm1000ArchLong;

struct Rpm1000ArchCnctInfo_s
{
	unsigned short usRpmId;
    unsigned short usPortId;
    char           strIpAddr[20];
    unsigned short usSocketPort;
};
typedef struct Rpm1000ArchCnctInfo_s Rpm1000ArchCnctInfo;

struct Rpm1000ArchGenInfo_s
{
	unsigned short usRpmId;
    unsigned short usPortId;
};
typedef struct Rpm1000ArchGenInfo_s Rpm1000ArchGenInfo;

struct Rpm1000ArchSumTrcInfo_s
{
	unsigned short usRpmId;
    unsigned short usPortId;
    unsigned long  ulStartTime;
    unsigned long  ulStopTime;
    short          sStatId;
};
typedef struct Rpm1000ArchSumTrcInfo_s Rpm1000ArchSumTrcInfo;

struct Rpm1000ArchTimeTrcInfo_s
{
	unsigned short usRpmId;
    unsigned short usPortId;
    unsigned long  ulStartTime;
    unsigned long  ulStopTime;
    unsigned long  ulInterval;
    short          sStatId;
    double         dFrequency;
};
typedef struct Rpm1000ArchTimeTrcInfo_s Rpm1000ArchTimeTrcInfo;

struct Rpm1000ArchMultTrcInfo_s
{
	unsigned short usRpmId;
    unsigned short usPortId;
    unsigned long  ulStartTime;
    unsigned long  ulStopTime;
    unsigned long  ulInterval;
    short          sStatId;
};
typedef struct Rpm1000ArchMultTrcInfo_s Rpm1000ArchMultTrcInfo;

/* Multi day time window request structure */
struct Rpm1000ArchMultDTWTrcInfo_s
{
	unsigned short usRpmId;
    unsigned short usPortId;
    unsigned long  ulStartTime;
    unsigned long  ulStopTime;
	unsigned long  ulWindowOpen;
	unsigned long  ulWindowClose;
    short          sStatId;
};
typedef struct Rpm1000ArchMultDTWTrcInfo_s Rpm1000ArchMultDTWTrcInfo;

/* Multi frequency time trace request structure */
struct Rpm1000ArchMultFreqTimeTrcInfo_s
{
	unsigned short usRpmId;
    unsigned short usPortId;
    unsigned long  ulStartTime;
    unsigned long  ulStopTime;
    unsigned long  ulInterval;
    short          sStatId;
    double         dStartFreq;
	double		   dStopFreq;
};
typedef struct Rpm1000ArchMultFreqTimeTrcInfo_s Rpm1000ArchMultFreqTimeTrcInfo;


/* return structures */

struct Rpm1000ArchReadInfo_s
{
	unsigned short usRpmId;
    unsigned short usPortId;
};
typedef struct Rpm1000ArchReadInfo_s Rpm1000ArchReadInfo;

struct Rpm1000ArchDataPntFreq_s
{
    double         dwStat;
    double         dwFreq;
};
typedef struct Rpm1000ArchDataPntFreq_s Rpm1000ArchDataPntFreq;

struct Rpm1000ArchDataPntTime_s
{
    double         dwStat;
    unsigned long  ulTime;
};
typedef struct Rpm1000ArchDataPntTime_s Rpm1000ArchDataPntTime;

struct Rpm1000ArchSumTrace_s
{
    unsigned long  ulMsgSeqNum;
    short          sHandle;
	int		       nDataValid;
	unsigned long  ulStartTime;
    unsigned long  ulStopTime;
    unsigned long  ulNumTraces;
    unsigned int   uStartMonPlanMajRev;
    unsigned int   uStopMonPlanMajRev;
	int		       nStatID;
    Rpm1000ArchDataPntFreq data<RPM1000_ARCH_MAX_DATA_PNTS>;
};
typedef struct Rpm1000ArchSumTrace_s Rpm1000ArchSumTrace;

struct Rpm1000ArchTimeTrace_s
{
    unsigned long  ulMsgSeqNum;
    short          sHandle;
	int		       nDataValid;
	unsigned long  ulStartTime;
    unsigned long  ulStopTime;
    unsigned long  ulNumTraces;
    unsigned long  ulIntervalTime;
    unsigned int   uStartMonPlanMajRev;
    unsigned int   uStopMonPlanMajRev;
	int		       nStatID;
    double	       dwFreq;
    Rpm1000ArchDataPntTime data<RPM1000_ARCH_MAX_DATA_PNTS>;
};
typedef struct Rpm1000ArchTimeTrace_s Rpm1000ArchTimeTrace;

struct Rpm1000ArchMultTrace_s
{
    unsigned long   ulMsgSeqNum;
    short           sHandle;
	int		        nDataValid;
	unsigned int    uTraceNo;
    unsigned int 	uNumTraces;
	unsigned long	ulStartTime;
    unsigned long 	ulStopTime;
    unsigned long   ulNumTraces;
	unsigned long	ulIntervalTime;
	unsigned int 	uStartMonPlanMajRev;
    unsigned int 	uStopMonPlanMajRev;
	int		        nStatID;
    Rpm1000ArchDataPntFreq data<RPM1000_ARCH_MAX_DATA_PNTS>;
};
typedef struct Rpm1000ArchMultTrace_s Rpm1000ArchMultTrace;

/* Multi day time window return structure */
struct Rpm1000ArchMultDTWTrace_s
{
    unsigned long   ulMsgSeqNum;
    short           sHandle;
	int		        nDataValid;
	unsigned int    uTraceNo;
    unsigned int 	uNumTraces;
	unsigned long	ulStartTime;
    unsigned long 	ulStopTime;
    unsigned long	ulWindowOpen;
    unsigned long   ulWindowClose;
    unsigned long   ulNumTraces;
	unsigned int 	uStartMonPlanMajRev;
    unsigned int 	uStopMonPlanMajRev;
	int		        nStatID;
    Rpm1000ArchDataPntFreq data<RPM1000_ARCH_MAX_DATA_PNTS>;
};
typedef struct Rpm1000ArchMultDTWTrace_s Rpm1000ArchMultDTWTrace;

/* Multi frequency time trace return structure */
struct Rpm1000ArchMultFreqTimeTrace_s
{
    unsigned long   ulMsgSeqNum;
    short           sHandle;
	int		        nDataValid;
	unsigned int    uTraceNo;
    unsigned int 	uNumTraces;
	unsigned long	ulStartTime;
    unsigned long 	ulStopTime;
    double			dStartFreq;
    double		    dStopFreq;
    unsigned long   ulNumTraces;
	unsigned long	ulIntervalTime;
	unsigned int 	uStartMonPlanMajRev;
    unsigned int 	uStopMonPlanMajRev;
	int		        nStatID;
    Rpm1000ArchDataPntTime data<RPM1000_ARCH_MAX_DATA_PNTS>;
};
typedef struct Rpm1000ArchMultFreqTimeTrace_s Rpm1000ArchMultFreqTimeTrace;

struct QamTrakChannelInfo

{

    unsigned int channelInfoId;
    unsigned int portUid;
    unsigned int frequency_hz;
    unsigned int symbolRate_ksps;
    unsigned int modulationType; /* 1=qpsk, 2=qam16, 3=qam64, 4=qam32 */
};


struct QamTrakPacket
{
    unsigned int   channelInfoId;
    unsigned long  timestamp_s; /* seconds since Unix Epoch */
    unsigned long macAddressLow; /*MAC address low 32 bits*/
    unsigned long macAddressHigh; /*MAC address high 16 bits*/
    unsigned int   codewordsTotal;
    unsigned int   codewordsCorrectable;
    unsigned int   codewordsUncorrectable;
    unsigned short merEqualized_dB100;
    unsigned short merUnequalized_dB100;
    unsigned short carrierLevel_dBmV100;
    unsigned short impulseNoisePeak_stdev100;
    unsigned short impulseNoisePeak_level_dBc100;
    unsigned short impulseNoisePeak_floor_dBc100;
};

struct DocsisMonitoringData_rpc
{
	unsigned long ulRet;
	unsigned long intervalStart_s;
	unsigned long intervalStop_s;
	QamTrakChannelInfo channelInfo<>;
	QamTrakPacket packets<>;
};

struct Rpm1000ArchRetHandle_s
{
    unsigned long   ulRet;
    short           sHandle;
};
typedef Rpm1000ArchRetHandle_s Rpm1000ArchRetHandle;

typedef short sHandle;

typedef unsigned long ulArchMsgSeqNum;

const RPC_MAX_MEAS_PER_PORT = 250;
const RPC_MAX_RPM_PORTS     = 120; /* 15 rpms x 8 ports */

struct Rpm3000ArchiveStats
{
	double avgLinLevel;
    double maxLinLevel;
    double minLinLevel;
    double numLvl1Exc;
    double numLvl2Exc;
    double numLvl3Exc;
    double numLvl4Exc;    
};

struct Rpm3000ArchiveBundleSet
{
	unsigned long rpm;
	unsigned long port;
	unsigned long portUid;
	unsigned long majorRev;
	unsigned long minorRev;
	unsigned long numberOfScans;
	unsigned long startTime;
	unsigned long stopTime;
	double   frequencies<RPC_MAX_MEAS_PER_PORT>;
	struct Rpm3000ArchiveStats archStats<RPC_MAX_MEAS_PER_PORT>;
};

struct Rpm3000ArchiveBundleResponse_s
{
	unsigned long moreDataSets; /* contains -1 if there is no data set to return.
	                                         0 if this is the last data set.
	                                        >0 if there are more data sets stored.
	                            */
	string bundleId<>;   /* The value to pass back in the corresponding
	                     ArchiveBundleClear. Opaque to caller.
	                   */
	struct Rpm3000ArchiveBundleSet sets<RPC_MAX_RPM_PORTS>;
};
typedef Rpm3000ArchiveBundleResponse_s Rpm3000ArchiveBundleResponse;

program RPM1000ARCHIVESVR {
  version RPM1000ARCHIVESVRVERS {
    unsigned long         rpm1000ArchConnect ( Rpm1000ArchCnctInfo )             	     =  1;
    unsigned long         rpm1000ArchDisconnect ( Rpm1000ArchGenInfo )         		  	 =  2;
    unsigned long         rpm1000ArchPurge( Rpm1000ArchGenInfo )               		  	 =  3;
    Rpm1000RetArchDirInfo rpm1000ArchFindDirInfoFirst( Rpm1000ArchULong)       		  	 =  4;
    Rpm1000RetArchDirInfo rpm1000ArchFindDirInfoNext( Rpm1000ArchLong    )     		  	 =  5;
    Rpm1000ArchRetHandle  rpm1000ArchReqSumTrace ( Rpm1000ArchSumTrcInfo )            	 =  6;
    Rpm1000ArchRetHandle  rpm1000ArchReqTimeTrace ( Rpm1000ArchTimeTrcInfo )             =  7;
    Rpm1000ArchRetHandle  rpm1000ArchReqMultiTrace ( Rpm1000ArchMultTrcInfo )            =  8;
	Rpm1000ArchRetHandle  rpm1000ArchReqMultiDTWTrace( Rpm1000ArchMultDTWTrcInfo )       =  9;
	Rpm1000ArchRetHandle  rpm1000ArchReqMultiTimeTrace( Rpm1000ArchMultFreqTimeTrcInfo ) = 10;
    Rpm1000ArchRetHandle  rpm1000ArchReqMultiNext ( Rpm1000ArchGenInfo )                 = 11;
    unsigned long         rpm1000ArchAbortMulti ( Rpm1000ArchGenInfo )                   = 12;
  } = 1;

  version RPM1000ARCHIVESVRVERS_2 {
  	unsigned long         rpm3000DocsisMonitoringConfigGet( void )                       = 13;
  	unsigned long         rpm3000DocsisMonitoringConfigSet( unsigned long )              = 14;

  	/* returns the oldest entire interval */
  	DocsisMonitoringData_rpc  rpm3000DocsisMonitoringDataGet( void )                     = 15;
  	unsigned long         rpm3000DocsisMonitoringDataClear( unsigned long interval )     = 16;  	
  } = 2;
  
  version RPM1000ARCHIVESVRVERS_3 {
  	/* Webview RPC call to return the entire archive data for one interval */
  	Rpm3000ArchiveBundleResponse  rpm3000ArchiveBundleGet( void )                        = 17;
  	unsigned long                 rpm3000ArchiveBundleClear( string bundleId )           = 18;
  } = 3;
  
} = 0x20000008;


