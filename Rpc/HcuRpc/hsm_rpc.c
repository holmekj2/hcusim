#include <rpc/rpc.h>
#include "hsmrpc.h"
#include "ruby.h"
#include "../cruby_utils.h"
#include <stdio.h>

//This is the RPC server main calls
extern int hsmRpcSvrMain ();

//Class singleton (i.e. a ruby class object)
VALUE hsmRpc;
//Object singleton. Using this implies that this class is a singleton class since all objects will use this global.
//This is necessary if something other than Ruby is calling c methods (e.g. RPC server). 
VALUE rpcInstance;

VALUE initHsm(VALUE self, VALUE robject)
{
  //Assign the Ruby object the implementing Ruby object
  rpcInstance = robject;
  rb_iv_set(self, "@robject", robject);
  return self;
}

//VALUE startHcuRpcService(VALUE self)
//registerProtocol
//0 (don't register with portmapper, IPPROTO_UDP, or IPPROTO_TCP
void startHsmRpcService(unsigned long registerProtocol)
{
  //Start the RPC service
  hsmRpcSvrMain(registerProtocol);
}

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hsmRetString15 *hsmgetserialnumber_1_svc(void *v, struct svc_req *s)
    {
        static hsmRetString15 string15;
        string15.str[14] = 0;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hsmgetserialno");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        string15.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* serial = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("str")));
        strncpy(string15.str, serial, sizeof(string15.str) - 1);        
        return (&string15);
    }
/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hsmRetString10 *hsmgetfirmwarerev_1_svc(void *v, struct svc_req *s)
    {
        static hsmRetString10 string10;
        string10.str[9] = 0;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hsmgetfirmwarerev");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        string10.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* st = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("str")));
        strncpy(string10.str, st, sizeof(string10.str) - 1);        
        return (&string10);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *hsmcfgconnect_1_svc(void *v, struct svc_req *s)
    {
        static unsigned long retVal = 0;
        return (&retVal);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *hsmcfgdisconnect_1_svc(void *v, struct svc_req *s)
    {
        static unsigned long retVal = 0;
        return (&retVal);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *hsmputtelemetryfreq_1_svc (double *dFreq, struct svc_req *s)
    {
        static u_long ulRet;
      
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hsmputtelemtryfreq");
        rb_hash_aset(methodin, CSTR2SYM("frequency"), rb_float_new(*dFreq));                
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ulRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        return (&ulRet);

    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hsmRetDouble *hsmgettelemetryfreq_1_svc(void *v, struct svc_req *s)
    {
        static hsmRetDouble retVal;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hsmgettelemetryfreq");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        retVal.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        retVal.dDouble = NUM2DBL(rb_hash_aref(rreturn, CSTR2SYM("telemetryFreq")));
        return (&retVal);
    }
/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *hsmputtelemetrylevel_1_svc (unsigned long *ulLevel, struct svc_req *s)
    {
      
        static u_long ulRet;
      
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hsmputtelemtrylevel");
        rb_hash_aset(methodin, CSTR2SYM("level"), INT2FIX(*ulLevel));                
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ulRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        return (&ulRet);
        
    }
/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hsmRetULong *hsmgettelemetrylevel_1_svc(void *v, struct svc_req *s)
    {
        static hsmRetULong retVal;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hsmgettelemetrylevel");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        retVal.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        retVal.ulNum = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("telemetryLevel")));
        return (&retVal);
		
    }
/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hsmRetShort *hsmgetstate_1_svc(void *v, struct svc_req *s)
    {
        static hsmRetShort retVal;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hsmgetstate");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        retVal.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        retVal.sNum = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("state")));
        return (&retVal);
    }
/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    u_long *hsmputstate_1_svc (u_short *sState, struct svc_req *s)
    {
		
        static u_long ulRet = 0;
      
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hsmputstate");
        rb_hash_aset(methodin, CSTR2SYM("state"), INT2FIX(*sState));                
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ulRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        return (&ulRet);
    }


/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hsmRetULong *hsmgetuid_1_svc(void *v, struct svc_req *s)
    {
        static hsmRetULong rv;
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hsmgetuid");
        VALUE rreturn = rb_hash_new();
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        //Translate return to C
        rv.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));        
        rv.ulNum = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulNum")));      
        return (&rv);
    }


/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *hsmputuid_1_svc (unsigned long *id, struct svc_req *s)
    {
        static u_long ulRet;
      
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hsmputuid");
        rb_hash_aset(methodin, CSTR2SYM("uid"), INT2FIX(*id));        
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ulRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        return (&ulRet);
    }
/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *hsmrouteconnect_1_svc(void *v, struct svc_req *s)
    {
        static unsigned long retVal = 0;
        return (&retVal);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *hsmroutedisconnect_1_svc(void *v, struct svc_req *s)
    {
        static unsigned long retVal = 0;
        return (&retVal);
    }
/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *hsmroute_1_svc (hsmRouteInfo * routeInfo, struct svc_req *s)
    {
        static unsigned long retVal = 0;
        return (&retVal);
    }
/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    u_long *hsmstoproute_1_svc (broadcastPortId * portId, struct svc_req *s)
    {
        static unsigned long retVal = 0;
        return (&retVal);
    }
/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *hsmmodifyparameters_1_svc (hsmRouteInfo * routeInfo, struct svc_req *s)
    {
        static unsigned long retVal = 0;
        return (&retVal);
    }
/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hsmRoutingList *hsmgetroutinglist_1_svc(void *v, struct svc_req *s)
    {
        static hsmRoutingList retVal;
        retVal.ulReturn = 0;
        return (&retVal);
    }
/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hsmRouteInfoRet *hsmgetparams_1_svc (broadcastPortId * portId, struct svc_req *s)
    {
        static hsmRouteInfoRet routeInfo;
        routeInfo.ulReturn = 0;
        return (&routeInfo);
    }
/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *hsmsvcconnect_1_svc(void *v, struct svc_req *s)
    {
        static unsigned long retVal = 0;
        return (&retVal);
    }
/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *hsmsvcdisconnect_1_svc(void *v, struct svc_req *s)
    {
        static unsigned long retVal = 0;
        return (&retVal);
    }
/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hsmRetString10 *hsmsvcgetfirmwarerev_1_svc(void *v, struct svc_req *s)
    {
		return hsmgetfirmwarerev_1_svc(v,s);
    }
/*****************************************************************************/
/* Unused                                                                    */
/*****************************************************************************/
    hsmRetString10 *hsmsvcgetbootfirmwarerev_1_svc(void *v, struct svc_req *s)
    {
        static hsmRetString10 retVal;
        retVal.ulReturn = 0;
        return (&retVal);
    }
/*****************************************************************************/
/* Unused                                                                    */
/*****************************************************************************/
    hsmRetFileInfo *hsmsvcfindfwfilefirst_1_svc(void *v, struct svc_req *s)
    {
        static hsmRetFileInfo retVal;
        retVal.ulReturn = 0;
        return (&retVal);
    }
/*****************************************************************************/
/* Unused                                                                    */
/*****************************************************************************/
    hsmRetFileInfo *hsmsvcfindfwfilenext_1_svc(void *v, struct svc_req *s)
    {
        static hsmRetFileInfo retVal;
        retVal.ulReturn = 0;
        return (&retVal);
    }
/*****************************************************************************/
/* Unused                                                                    */
/*****************************************************************************/
    unsigned long *hsmsvcinstallfw_1_svc (hsmFileInfo * fileInfo, struct svc_req *s)
    {
        static unsigned long retVal = 0;
        return (&retVal);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hsmRetShort *hsmgetbroadcaststate_1_svc (void* v, struct svc_req *s)
    {
        static hsmRetShort retVal;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hsmgetbroadcaststate");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        retVal.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        retVal.sNum = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("state")));
        return (&retVal);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *hsmputbroadcaststate_1_svc (unsigned short *hsmState, struct svc_req *s)
    {
        static u_long ulRet = 0;
      
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hsmputbroadcaststate");
        rb_hash_aset(methodin, CSTR2SYM("state"), INT2FIX(*hsmState));                
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ulRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        return (&ulRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hsmRetString40 *hsmgetmodelno_1_svc (void *v, struct svc_req *s)
    {
        static hsmRetString40 string40;
        string40.str[39] = 0;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hsmgetmodelno");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        string40.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* model = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("str")));
        strncpy(string40.str, model, sizeof(string40.str) - 1);        
        return (&string40);
    }


/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *hsmputlabel_1_svc (char **sLabel, struct svc_req *s)
    {
        static u_long ulRet;
      
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hsmputlabel");
        rb_hash_aset(methodin, CSTR2SYM("str"), rb_str_new2(*sLabel));        
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ulRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        return (&ulRet);
    }


/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hsmRetString40 *hsmgetlabel_1_svc (void* v, struct svc_req *s)
    {
        static hsmRetString40 string40;
        string40.str[39] = 0;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hsmgetlabel");
        VALUE rreturn = rb_hash_new();
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        //Translate return to C
        string40.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* model = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("str")));
        strncpy(string40.str, model, sizeof(string40.str) - 1);        
        return (&string40);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hsmRetString *hsmgetlabel_3_svc (void* v, struct svc_req *s)
    {
        static hsmRetString retStr = {0, 0};
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hsmgetlabel");
        VALUE rreturn = rb_hash_new();
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        //Translate return to C
        retStr.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* label = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("str")));
        retStr.str = label;
        return (&retStr);

    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *hsmputnotes_1_svc (char **sNotes, struct svc_req *s)
    {
        static u_long ulRet;
      
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hsmputnotes");
        rb_hash_aset(methodin, CSTR2SYM("str"), rb_str_new2(*sNotes));        
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ulRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        return (&ulRet);
    }


/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hsmRetString1024 *hsmgetnotes_1_svc (void* v, struct svc_req *s)
    {
		
        static hsmRetString1024 string40;
        string40.str[1023] = 0;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hsmgetnotes");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        string40.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* model = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("str")));
        strncpy(string40.str, model, sizeof(string40.str) - 1);        
        return (&string40);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hsmRetString *hsmgetnotes_3_svc (void* v, struct svc_req *s)
    {
        static hsmRetString retStr = {0, 0};
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hsmgetnotes");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        retStr.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* label = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("str")));
        retStr.str = label;
        return (&retStr);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *hsmputlocation_1_svc (char **sLocation, struct svc_req *s)
    {
        static u_long ulRet;
      
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hsmputlocation");
        rb_hash_aset(methodin, CSTR2SYM("str"), rb_str_new2(*sLocation));        
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ulRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        return (&ulRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hsmRetString255 *hsmgetlocation_1_svc (void* v, struct svc_req *s)
    {
        static hsmRetString255 string40;
        string40.str[254] = 0;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hsmgetlocation");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        string40.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* model = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("str")));
        strncpy(string40.str, model, sizeof(string40.str) - 1);        
        return (&string40);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hsmRetString *hsmgetlocation_3_svc (void* v, struct svc_req *s)
    {
        static hsmRetString retStr = {0, 0};
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hsmgetlocation");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        retStr.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* label = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("str")));
        retStr.str = label;
        return (&retStr);

    }

/*****************************************************************************/
/* removes local nodes only (no remotevirtual nodes)                         */
/*****************************************************************************/
    unsigned long *hsmremoveall_1_svc (void *v, struct svc_req *s)
    {
        static unsigned long retVal = 0;
        return (&retVal);
    }

/*****************************************************************************/
/* removes local and remote virtual nodes (used only on master)              */
/*****************************************************************************/
    unsigned long *hsmremoveallcomprehensive_1_svc (void *v, struct svc_req *s)
    {
        static unsigned long retVal = 0;
        return (&retVal);
    }

//--------------------------------------------------------
//! @details
//! Server request to add physical HSM
//--------------------------------------------------------
    unsigned long *hsmaddphysicalhsm_2_svc (void *v, struct svc_req *s)
    {
        static unsigned long retVal = 0;
        return (&retVal);
    }
    //--------------------------------------------------------
    //! @details
    //! Server request to add virtual HSM
    //--------------------------------------------------------
    unsigned long *hsmaddvirtualhsm_2_svc (virtualhsmAddConfiguration *configuration, struct svc_req *s)
    {
        static unsigned long retVal = 0;
        return (&retVal);
    }
    //--------------------------------------------------------
    //! @details
    //! Server request to remove virtual HSM
    //--------------------------------------------------------
    unsigned long *hsmremovehsm_2_svc (void *v, struct svc_req *s)
    {
        static unsigned long retVal = 0;
        return (&retVal);
    }

