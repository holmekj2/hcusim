require 'Rpc/rpc_utils'
require 'log'

class ArchiveRpcHandler
  include RpcUtils
  #Pass in an instance of a Hcu class that handles the RPC requests.
  def initialize(hcu)
    @hcu = hcu
    @docsis_last_request_time = 0
  end
  def rpm3000archivebundleget(args)
    Log::GeneralLogger.info "rpm3000archivebundleget called"  
    data_sets = @hcu.get_spectral_performance_data
    data = {}
    data[:more_data] = 0
    data[:data_sets] = data_sets
    data[:number_sets] = data_sets.length
    data[:bundle_id] = Time.now.utc.to_i.to_s
    data
  end
  def rpm3000archivebundleclear(args)
    ret = {:ulReturn => 0}
  end
  def rpm3000docsismonitoringdataget(args)
    Log::GeneralLogger.info "rpm3000docsismonitoringdataget called"  
    performance_data = @hcu.get_mactrak_performance_data
    data = {}
    data[:return] = 0
    data[:start_time] = performance_data.start_time
    t = Time.now.to_i
    if t - @docsis_last_request_time < 15
      #If there is less than 15 seconds between request indicate that there isn't more data. We only want to send one
      #packet per 15 minute interval
      data[:return] = -1
    else
      @docsis_last_request_time = t
    end
    data[:stop_time] = performance_data.stop_time
    data[:channels] = performance_data.channels
    #It was taking forever to transfer this data thru the pipe so it is being generated in the c interface
    #data[:packets] = performance_data.packets
    Log::GeneralLogger.info "rpm3000docsismonitoringdataget returning #{data[:channels].length}"  
    data
  end

end






