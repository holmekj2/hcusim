#include "ruby.h"
#include <rpc/rpc.h>
#include "hdr.h"
#include <stdio.h>

VALUE xdrHeader;

static VALUE xdr_header_init(VALUE self)
{
  return self;
}

//Put hdr in xdr outputStream
static void xdrHeaderEncode(Hdr* hdr, char* outputStream)
{
    //create xdr instance
    XDR xdrOut;
    //Point the xdr object at the input buffer
    xdrmem_create(&xdrOut, outputStream, HDR_LEN, XDR_ENCODE);    
    xdr_Hdr(&xdrOut, hdr);
    //Output stream now contains xdr data
}

//Hdr is the decoded data
static void xdrHeaderDecode(char* inputStream, Hdr* hdr)
{
    //create xdr instance
    XDR xdrIn;
    //Point the xdr object at the input buffer
    xdrmem_create(&xdrIn, inputStream, HDR_LEN, XDR_DECODE);    
    xdr_Hdr(&xdrIn, hdr);
    //hdr now contains the data. 
}
//Returns an array. [0] is the length, [1] is the msg Id
static VALUE xdr_header_decode(VALUE self, VALUE r_input_stream)
{
    char* inputStream;
    Hdr hdr; 
    VALUE arr;
    //inputStream = (char*) RARRAY(r_input_stream)->ptr;
    inputStream = STR2CSTR(r_input_stream);
    xdrHeaderDecode(inputStream, &hdr);
    arr = rb_ary_new2(2);
    rb_ary_push(arr, INT2FIX(hdr.msgLen));
    rb_ary_push(arr, INT2FIX(hdr.msgId));
    return arr;
}

//Return output stream. Encode msg length and ID into header
static VALUE xdr_header_encode(VALUE self, VALUE msg_length, VALUE msg_id)
{
    char outputStream[HDR_LEN + 1];
    Hdr hdr; 
    int i;
    hdr.msgLen = NUM2INT(msg_length);
    hdr.msgId = NUM2INT(msg_id);
    xdrHeaderEncode(&hdr, outputStream);
    //return rb_ary_new4(HDR_LEN / sizeof(VALUE), (VALUE*)outputStream);
    //return rb_ary_new2(HDR_LEN);
    return rb_str_new(outputStream, HDR_LEN);
}

void initXdrHeader()
{
  xdrHeader = rb_define_class("XdrHeader", rb_cObject);

  rb_define_method(xdrHeader, "initialize", xdr_header_init, 0);
  rb_define_method(xdrHeader, "hdr_decode", xdr_header_decode, 1);
  //xdrCommon.decode(in)
  rb_define_method(xdrHeader, "hdr_encode", xdr_header_encode, 2);
}
