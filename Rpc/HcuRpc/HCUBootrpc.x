/*
    .x file for the HCU config RPC Server 
*/

struct HcuBootId_s
{
    unsigned long ulReturn;
    char          HcuIpAdder[20];
	unsigned long ulHcuUID;
};
typedef struct HcuBootId_s HcuBootIdInfo;


program HCUBOOTSVR {
	version HCUBOOTSVRVERS {
	       unsigned long HcuOnLine( HcuBootIdInfo )          = 1;
	} = 1;
} = 0x2000000f;

