require 'Rpc/rpc_utils'
require 'Rpc/Intercepter/rpc_override'
require 'log'

class HsmRpcHandler
  include RpcUtils
  #Pass in an instance of a RPM delegate class that handles the RPC requests.
  def initialize(hsm)
    @hsm = hsm    
  end
  def hsmgetserialno(args)
    serial_number = @hsm.serial_number
    serial_number = validate_string(serial_number)
    ret = {:ulReturn => 0, :str => serial_number}
  end
  def hsmgetfirmwarerev(args)
    fw_version = @hsm.fw_version
    fw_version = validate_string(fw_version)   
    ret = {:ulReturn => 0, :str => fw_version}
  end
  def hsmgettelemetryfreq(args)
    telemetry_frequency = @hsm.telemetry_frequency
    telemetry_frequency = validate_float(telemetry_frequency)
    ret = {:ulReturn => 0, :telemetryFreq => telemetry_frequency}
  end
  def hsmputtelemtryfreq(args)
    @hsm.telemetry_frequency = args[:frequency]
    @hsm.save
    ret = {:ulReturn => 0}
  end
  def hsmgettelemetrylevel(args)
    telemetry_level = @hsm.telemetry_level
    telemetry_level = validate_int(telemetry_level)
    ret = {:ulReturn => 0, :telemetryLevel => telemetry_level}
  end
  def hsmputtelemtrylevel(args)
    @hsm.telemetry_level = args[:level]
    @hsm.save
    ret = {:ulReturn => 0}
  end
  def hsmgetstate(args)
    state = @hsm.device_enabled
    state = validate_int(state)
    ret = {:ulReturn => 0, :state => state}
  end
  def hsmputstate(args)
    @hsm.device_enabled = args[:state]
    @hsm.save
    ret = {:ulReturn => 0}
  end
  def hsmgetbroadcaststate(args)
    state = @hsm.broadcast_enabled
    state = validate_int(state)
    ret = {:ulReturn => 0, :state => state}
  end
  def hsmputbroadcaststate(args)
    @hsm.broadcast_enabled = args[:state]
    @hsm.save
    ret = {:ulReturn => 0}
  end
  def hsmgetuid(args)
    uid = @hsm.uid
    uid = validate_int(uid)
    ret = {:ulReturn => 0, :ulNum => uid}
  end
  def hsmputuid(args)
    @hsm.uid = args[:uid]
    @hsm.save
    ret = {:ulReturn => 0}
  end
  def hsmgetmodelno(args)
    model = @hsm.model
    model = validate_string(model)
    ret = {:ulReturn => 0, :str => model}
  end
  def hsmputlabel(args)
    @hsm.label = args[:str]
    @hsm.save
    ret = {:ulReturn => 0}
  end
  def hsmgetlabel(args)
    label = @hsm.label
    label = validate_string(label)
    ret = {:ulReturn => 0, :str => label}
  end
  def hsmputlocation(args)
    @hsm.location = args[:str]
    @hsm.save
    ret = {:ulReturn => 0}
  end
  def hsmgetlocation(args)
    location = @hsm.location
    location = validate_string(location)
    ret = {:ulReturn => 0, :str => location}
  end
  def hsmputnotes(args)
    @hsm.notes = args[:str]
    @hsm.save
    ret = {:ulReturn => 0}
  end
  def hsmgetnotes(args)
    notes = @hsm.notes
    notes = validate_string(notes)
    ret = {:ulReturn => 0, :str => notes}
  end
  
  #Called if the RPC handler does not implement a method
  def method_missing(method, args=nil)
    #Log::GeneralLogger.fatal "Rpmgen RPC does not handle: #{method}"
    return nil
  end
end



  
