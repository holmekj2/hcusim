#include <rpc/rpc.h>
#include "rpm1000archive.h"
#include "ruby.h"
#include "../cruby_utils.h"
#include <stdio.h>

//This is the RPC server main calls
extern int rpmArchiveRpcSvrMain ();

//Class singleton (i.e. a ruby class object)
VALUE rpmArchiveRpc;
//Object singleton. Using this implies that this class is a singleton class since all objects will use this global.
//This is necessary if something other than Ruby is calling c methods (e.g. RPC server). 
VALUE rpcInstance;

VALUE initRpcArchive(VALUE self, VALUE robject)
{
  //Assign the Ruby object the implementing Ruby object
  rpcInstance = robject;
  rb_iv_set(self, "@robject", robject);
  return self;
}

void startArchiveRpcService(unsigned long registerProtocol)
{
  //Start the RPC service
  rpmArchiveRpcSvrMain(registerProtocol);
}

static void createPacket(QamTrakPacket* packet, int start_time, int channelId);

static Rpm3000ArchiveBundleSet* bundleSets = 0;
static double* frequencies[120];
static Rpm3000ArchiveStats* archStatsSets[120];
static QamTrakChannelInfo* qamTrakChannels = 0; 
static QamTrakPacket* qamTrakPackets = 0; 

static MAX_NUMBER_PORTS = 120;
static MAX_NUMBER_MON_PLAN_POINTS = 250;



/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
/****************************************************************************/
/*                                                                          */
/****************************************************************************/
    u_long *rpm1000archconnect_1_svc (Rpm1000ArchCnctInfo * pcnctinfo, struct svc_req *s)
    {
        static u_long ulRet = 0;
        return (&ulRet);
    }

/****************************************************************************/
/*                                                                          */
/****************************************************************************/
    u_long *rpm1000archdisconnect_1_svc (Rpm1000ArchGenInfo * pgeninfo, struct svc_req *s)
    {
        static u_long ulRet = 0;
        return (&ulRet);
    }
    
    u_long * rpm3000archivebundleclear_3_svc(char **bundleId, struct svc_req *s)
    {
        static u_long ulRet = 0;
        //We'll free our arrays created on the bundleget call
        free(bundleSets);
        bundleSets = 0;
        int i = 0;
        for (i = 0; i < MAX_NUMBER_PORTS; ++i)
        {
            free(frequencies[i]);
            frequencies[i] = 0;
        }
        
        for (i = 0; i < MAX_NUMBER_PORTS; ++i)
        {
            free(archStatsSets[i]);
            archStatsSets[i] = 0;
        }
        
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm3000archivebundleclear");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ulRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        
        return &ulRet;
    }
    
    Rpm3000ArchiveBundleResponse * rpm3000archivebundleget_3_svc(void *v, struct svc_req *s)
    {
        static Rpm3000ArchiveBundleResponse rv;
        static char bundleId[100];
        int i = 0;
        //Create the arrays we need. These will be freed on the bundle clear
        //We'll make space for a 250 point monitoring plan and 120 ports
        if (bundleSets == 0)
        {
            bundleSets = malloc(MAX_NUMBER_PORTS * sizeof(Rpm3000ArchiveBundleSet));
        }
        for (i = 0; i < MAX_NUMBER_PORTS; ++i)
        {
            frequencies[i] = malloc(MAX_NUMBER_MON_PLAN_POINTS * sizeof(double));
        }
        for (i = 0; i < MAX_NUMBER_PORTS; ++i)
        {
            archStatsSets[i] = malloc(MAX_NUMBER_MON_PLAN_POINTS * sizeof(Rpm3000ArchiveStats));
        }

        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm3000archivebundleget");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);     
        rv.moreDataSets = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("more_data")));
        char* bId = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("bundle_id")));
        strncpy(bundleId, bId, 99);
        rv.bundleId = bundleId;
        rv.sets.sets_len = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("number_sets")));
        rv.sets.sets_val = bundleSets;
        VALUE dataSets = rb_hash_aref(rreturn, CSTR2SYM("data_sets"));
        VALUE dataSet;
        int index = 0;

        while ((dataSet = rb_ary_shift(dataSets)) != Qnil)
        {
            bundleSets[index].rpm = NUM2UINT(rb_hash_aref(dataSet, CSTR2SYM("rpm")));
            bundleSets[index].port = NUM2UINT(rb_hash_aref(dataSet, CSTR2SYM("port")));
            bundleSets[index].portUid = NUM2UINT(rb_hash_aref(dataSet, CSTR2SYM("port_uid")));
            bundleSets[index].majorRev = NUM2UINT(rb_hash_aref(dataSet, CSTR2SYM("major_rev")));
            bundleSets[index].minorRev = NUM2UINT(rb_hash_aref(dataSet, CSTR2SYM("minor_rev")));
            bundleSets[index].numberOfScans = NUM2UINT(rb_hash_aref(dataSet, CSTR2SYM("number_scans")));
            bundleSets[index].startTime = NUM2UINT(rb_hash_aref(dataSet, CSTR2SYM("start_time")));
            bundleSets[index].stopTime = NUM2UINT(rb_hash_aref(dataSet, CSTR2SYM("stop_time")));      
            VALUE vfrequencies = rb_hash_aref(dataSet, CSTR2SYM("frequencies"));
            VALUE vfrequency;
            int frequencyindex = 0;
            bundleSets[index].frequencies.frequencies_len = RARRAY(vfrequencies)->len; 
            bundleSets[index].frequencies.frequencies_val = frequencies[index];     
            while ((vfrequency = rb_ary_shift(vfrequencies)) != Qnil)
            {
                frequencies[index][frequencyindex] = NUM2DBL(vfrequency);
                frequencyindex++;
            }


            VALUE varchStats = rb_hash_aref(dataSet, CSTR2SYM("stats"));
            VALUE varchStat;
            int archIndex = 0;
            bundleSets[index].archStats.archStats_len = RARRAY(varchStats)->len;            
            bundleSets[index].archStats.archStats_val = archStatsSets[index];     
            while ((varchStat = rb_ary_shift(varchStats)) != Qnil)
            {
                archStatsSets[index][archIndex].maxLinLevel = NUM2DBL(rb_hash_aref(varchStat, CSTR2SYM("max_level")));      
                archStatsSets[index][archIndex].minLinLevel = NUM2DBL(rb_hash_aref(varchStat, CSTR2SYM("min_level")));      
                archStatsSets[index][archIndex].avgLinLevel = NUM2DBL(rb_hash_aref(varchStat, CSTR2SYM("avg_level")));      
                archStatsSets[index][archIndex].numLvl1Exc = NUM2DBL(rb_hash_aref(varchStat, CSTR2SYM("threshold1_exceeded")));                      
                archStatsSets[index][archIndex].numLvl2Exc = NUM2DBL(rb_hash_aref(varchStat, CSTR2SYM("threshold2_exceeded")));                      
                archStatsSets[index][archIndex].numLvl3Exc = NUM2DBL(rb_hash_aref(varchStat, CSTR2SYM("threshold3_exceeded")));                                      
                archStatsSets[index][archIndex].numLvl4Exc = NUM2DBL(rb_hash_aref(varchStat, CSTR2SYM("threshold4_exceeded")));                      
                archIndex++;
            }
            
            index++;            
        }
        return &rv;
    }

    

    DocsisMonitoringData_rpc * rpm3000docsismonitoringdataget_2_svc(void *v, struct svc_req *s)
    {
        static DocsisMonitoringData_rpc rv = {0, 0, 0, {0, 0}, {0, 0}};
        static int MAX_NUMBER_CHANNELS = 120 * 4; //120 ports at 4 channels
        static int MAX_NUMBER_PACKETS = 27000; //15 rpms at 2 packets per second * 900 seconds 

        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm3000docsismonitoringdataget");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);     
        rv.ulRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("return")));
        if (rv.ulRet == -1) {
			//No more data
        	rv.ulRet = 0x8004100d;
        	rv.channelInfo.channelInfo_len = 0;
        	rv.channelInfo.channelInfo_val = 0;
        	rv.packets.packets_len = 0;
        	rv.packets.packets_val = 0;
			return &rv;
		}

        unsigned long startTime = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("start_time")));
        rv.intervalStart_s = startTime;
        rv.intervalStop_s = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("stop_time")));

        VALUE channels = rb_hash_aref(rreturn, CSTR2SYM("channels"));
        VALUE channel;
        int channelIndex = 0;
        rv.channelInfo.channelInfo_len = RARRAY(channels)->len;
        if(qamTrakChannels == 0)
        {
            qamTrakChannels = malloc(MAX_NUMBER_CHANNELS * sizeof(QamTrakChannelInfo));        
        }
        rv.channelInfo.channelInfo_val = qamTrakChannels;  
        int packetIndex = 0;  
        if(qamTrakPackets == 0)
        {
            qamTrakPackets = malloc(MAX_NUMBER_PACKETS * sizeof(QamTrakPacket)); 
        }
        rv.packets.packets_val = qamTrakPackets;            
        while ((channel = rb_ary_shift(channels)) != Qnil)
        {
			unsigned int channelId = NUM2UINT(rb_hash_aref(channel, CSTR2SYM("channel_id")));
            qamTrakChannels[channelIndex].channelInfoId = channelId;
            qamTrakChannels[channelIndex].portUid = NUM2UINT(rb_hash_aref(channel, CSTR2SYM("port_uid")));
            qamTrakChannels[channelIndex].frequency_hz = NUM2UINT(rb_hash_aref(channel, CSTR2SYM("frequency_hz")));
            qamTrakChannels[channelIndex].symbolRate_ksps = NUM2UINT(rb_hash_aref(channel, CSTR2SYM("symbolrate_ksps")));
            qamTrakChannels[channelIndex].modulationType = NUM2UINT(rb_hash_aref(channel, CSTR2SYM("modtype")));  
            //We are putting four channels on each port and 32 channels per RPM3000 (8ports*four channels)
            //2*900seconds(15minute) = 1800 packets per RPM per 15 minutes. 1800/32 = 56 packets per channel  
            int i = 0;
            for (i = 0; i < 56; ++i)
            {
				createPacket(&qamTrakPackets[packetIndex], startTime, channelId);
				packetIndex++;
			}
            channelIndex++;
        }       
        rv.packets.packets_len = packetIndex;     

        /*
        VALUE packets = rb_hash_aref(rreturn, CSTR2SYM("packets"));
        VALUE packet;
        int packetIndex = 0;
        rv.packets.packets_len = RARRAY(packets)->len;
        printf("rpm3000docsismonitoringdataget-2 %d %d\n", rv.packets.packets_len, MAX_NUMBER_PACKETS);                        
        if(qamTrakPackets == 0)
        {
            qamTrakPackets = malloc(MAX_NUMBER_PACKETS * sizeof(QamTrakPacket)); 
        }
        rv.packets.packets_val = qamTrakPackets;            
        while ((packet = rb_ary_shift(packets)) != Qnil)
        {
            qamTrakPackets[packetIndex].channelInfoId = NUM2UINT(rb_hash_aref(packet, CSTR2SYM("channel_id")));
            qamTrakPackets[packetIndex].timestamp_s = NUM2UINT(rb_hash_aref(packet, CSTR2SYM("timestamp")));
            qamTrakPackets[packetIndex].macAddressLow = NUM2UINT(rb_hash_aref(packet, CSTR2SYM("mac_address_low")));
            qamTrakPackets[packetIndex].macAddressHigh = NUM2UINT(rb_hash_aref(packet, CSTR2SYM("mac_address_high")));
            qamTrakPackets[packetIndex].codewordsTotal = NUM2UINT(rb_hash_aref(packet, CSTR2SYM("codewords")));
            qamTrakPackets[packetIndex].codewordsCorrectable = NUM2UINT(rb_hash_aref(packet, CSTR2SYM("correctables")));
            qamTrakPackets[packetIndex].codewordsUncorrectable = NUM2UINT(rb_hash_aref(packet, CSTR2SYM("uncorrectables")));
            qamTrakPackets[packetIndex].merEqualized_dB100 = NUM2UINT(rb_hash_aref(packet, CSTR2SYM("mer_equalized_db100")));
            qamTrakPackets[packetIndex].merUnequalized_dB100 = NUM2UINT(rb_hash_aref(packet, CSTR2SYM("mer_unequalized_db100")));
            qamTrakPackets[packetIndex].carrierLevel_dBmV100 = NUM2UINT(rb_hash_aref(packet, CSTR2SYM("carrier_level_db100")));
            qamTrakPackets[packetIndex].impulseNoisePeak_stdev100 = NUM2UINT(rb_hash_aref(packet, CSTR2SYM("impulse_stddev100")));
            qamTrakPackets[packetIndex].impulseNoisePeak_level_dBc100 = NUM2UINT(rb_hash_aref(packet, CSTR2SYM("impulse_peak_level")));
            qamTrakPackets[packetIndex].impulseNoisePeak_floor_dBc100 = NUM2UINT(rb_hash_aref(packet, CSTR2SYM("impulse_peak_floor")));
            packetIndex++;
        }      
        */
        return &rv;
    }
    
/****************************************************************************/
/*                                                                          */
/****************************************************************************/
    u_long *rpm1000archpurge_1_svc (Rpm1000ArchGenInfo * pgeninfo, struct svc_req *s)
    {
        static u_long ulRet = 0;
        return (&ulRet);
    }

/****************************************************************************/
/*                                                                          */
/****************************************************************************/
    Rpm1000RetArchDirInfo *rpm1000archfinddirinfofirst_1_svc (Rpm1000ArchULong * pgenulong, struct svc_req *s)
    {
        static Rpm1000RetArchDirInfo dirinfo;
        return (&dirinfo);
    }

/****************************************************************************/
/*                                                                          */
/****************************************************************************/
    Rpm1000RetArchDirInfo *rpm1000archfinddirinfonext_1_svc (Rpm1000ArchLong * pgenlong, struct svc_req *s)
    {
        static Rpm1000RetArchDirInfo dirinfo;
        return (&dirinfo);
    }

/****************************************************************************/
/*                                                                          */
/****************************************************************************/
    Rpm1000ArchRetHandle *rpm1000archreqsumtrace_1_svc (Rpm1000ArchSumTrcInfo * psumtrcinfo, struct svc_req *s)
    {
        static Rpm1000ArchRetHandle ulRet;
        return (&ulRet);
    }

/****************************************************************************/
/*                                                                          */
/****************************************************************************/
    Rpm1000ArchRetHandle *rpm1000archreqtimetrace_1_svc (Rpm1000ArchTimeTrcInfo * ptimetrcinfo, struct svc_req *s)
    {
        static Rpm1000ArchRetHandle ulRet;
        return (&ulRet);
    }

/****************************************************************************/
/*                                                                          */
/****************************************************************************/
    Rpm1000ArchRetHandle *rpm1000archreqmultitrace_1_svc (Rpm1000ArchMultTrcInfo * pmulttrcinfo, struct svc_req *s)
    {
        static Rpm1000ArchRetHandle ulRet;
        return (&ulRet);
    }

/****************************************************************************/
/*                                                                          */
/****************************************************************************/
    Rpm1000ArchRetHandle *rpm1000archreqmultinext_1_svc (Rpm1000ArchGenInfo * pgeninfo, struct svc_req *s)
    {
        static Rpm1000ArchRetHandle ulRet;
        return (&ulRet);
    }

/****************************************************************************/
/*                                                                          */
/****************************************************************************/
    u_long *rpm1000archabortmulti_1_svc (Rpm1000ArchGenInfo * pgeninfo, struct svc_req *s)
    {
        static u_long ulRet = 0;
        return (&ulRet);
    }

/****************************************************************************/
/*                                                                          */
/****************************************************************************/
    Rpm1000ArchRetHandle *rpm1000archreqmultidtwtrace_1_svc (Rpm1000ArchMultDTWTrcInfo * pmulttrcinfo, struct svc_req *s)
    {
        static Rpm1000ArchRetHandle ulRet;
        return (&ulRet);
    }

/****************************************************************************/
/*                                                                          */
/****************************************************************************/
    Rpm1000ArchRetHandle *rpm1000archreqmultitimetrace_1_svc (Rpm1000ArchMultFreqTimeTrcInfo * pmulttrcinfo, struct svc_req *s)
    {
        static Rpm1000ArchRetHandle ulRet;
        return (&ulRet);
    }


    u_long * rpm3000docsismonitoringconfigget_2_svc(void *v, struct svc_req *s)
    {
        static u_long ret = 1;

        return &ret;
    }

    u_long * rpm3000docsismonitoringconfigset_2_svc(u_long *new_value, struct svc_req *s)
    {
        static u_long ret = 0;
        return &ret;
    }

	static size_t s_dmdCurrentPosition = 0;
    static size_t s_dmdExpectedInterval = 0;

    u_long * rpm3000docsismonitoringdataclear_2_svc(u_long *interval, struct svc_req *s)
    {
        static u_long ret = 0;
        free(qamTrakChannels);
        qamTrakChannels = 0;
        free(qamTrakPackets);
        qamTrakPackets = 0;

        return &ret;
    }
    
static float createRandFloat()
{
    return rand()% 100 / 100.0;
}

static void createPacket(QamTrakPacket* packet, int start_time, int channelId)
{

    packet->channelInfoId = channelId;
    packet->timestamp_s = rand() % 900 + start_time;
    packet->macAddressLow = rand();
    packet->macAddressHigh = rand() % 65535;
    packet->codewordsTotal = rand() % 5 + 5;
    packet->codewordsCorrectable = rand() % 3;
    packet->codewordsUncorrectable = rand() % 3;
    packet->merEqualized_dB100 = createRandFloat() * 300 + 3000;
    packet->merUnequalized_dB100 = packet->merEqualized_dB100 - 2 + createRandFloat() * 100;
    packet->carrierLevel_dBmV100 = createRandFloat() * 2 * 100;
    packet->impulseNoisePeak_stdev100 = rand() % 10 + createRandFloat() * 1000;
    packet->impulseNoisePeak_level_dBc100 = createRandFloat() * +100 + 2500;
    packet->impulseNoisePeak_floor_dBc100 = createRandFloat() * + 100 + 5000;
}
    

