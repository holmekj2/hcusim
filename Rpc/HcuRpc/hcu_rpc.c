#include <rpc/rpc.h>
#include "hcurpc.h"
#include "ruby.h"
#include "../cruby_utils.h"
#include <stdio.h>

//This is the RPC server main calls
extern int hcuRpcSvrMain ();

//Class singleton (i.e. a ruby class object)
VALUE hcuRpc;
//Object singleton. Using this implies that this class is a singleton class since all objects will use this global.
//This is necessary if something other than Ruby is calling c methods (e.g. RPC server). 
VALUE rpcInstance;

VALUE initHcu(VALUE self, VALUE robject)
{
  //Assign the Ruby object the implementing Ruby object
  rpcInstance = robject;
  rb_iv_set(self, "@robject", robject);
  return self;
}

//VALUE startHcuRpcService(VALUE self)
//registerProtocol
//0 (don't register with portmapper, IPPROTO_UDP, or IPPROTO_TCP
void startHcuRpcService(unsigned long registerProtocol)
{
  //Start the RPC service
  hcuRpcSvrMain(registerProtocol);
  //return Qnil;  
}

//void Init_HcuRpc() {
  //Create mapping from ruby to c. Ruby object will be named XdrCommon
//  hcuRpc = rb_define_class("HcuRpc", rb_cObject);

//  rb_define_method(hcuRpc, "initialize", init, 1);
//  rb_define_method(hcuRpc, "start_rpc_service", startRpcService, 0);  
//}    

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *hcusyncconfig_1_svc(void *v, struct svc_req *s)
    {
        static unsigned long ulReturn;
        ulReturn = 0;
        return (&ulReturn);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hcuRetString40 *hcugetmodelno_1_svc (void *v, struct svc_req *s)
    {
        static hcuRetString40 string40;
        string40.str[39] = 0;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hcugetmodelno");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        string40.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* model = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("str")));
        strncpy(string40.str, model, sizeof(string40.str) - 1);        
        return (&string40);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hcuRetString15 *hcugetserialno_1_svc (void *v, struct svc_req *s)
    {
        static hcuRetString15 string15;
        string15.str[14] = 0;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hcugetserialno");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        string15.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* serial = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("str")));
        strncpy(string15.str, serial, sizeof(string15.str) - 1);        
        return (&string15);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    u_long *hcuputserialno_1_svc (char **str, struct svc_req *s)
    {
        static u_long ulRet;
      
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hcuputserialno");
        rb_hash_aset(methodin, CSTR2SYM("str"), rb_str_new2(*str));        
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ulRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        return (&ulRet);
    }

/******************************************************************************************************************/
/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hcuRetString40 *hcugetsbcmodelno_1_svc (void *v, struct svc_req *s)
    {
        static hcuRetString40 string40;
        string40.str[39] = 0;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hcugetsbcmodelno");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        string40.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* model = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("str")));
        strncpy(string40.str, model, sizeof(string40.str) - 1);        
        return (&string40);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hcuRetString10 *hcugetsbcflashsize_1_svc (void *v, struct svc_req *s)
    {
        static hcuRetString10 string10;
        string10.str[9] = 0;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hcugetsbcflashsize");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        string10.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* st = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("str")));
        strncpy(string10.str, st, sizeof(string10.str) - 1);        
        return (&string10);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hcuRetString10 *hcugetsbcdramsize_1_svc (void *v, struct svc_req *s)
    {
        static hcuRetString10 string10;
        string10.str[9] = 0;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hcugetsbcdramsize");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        string10.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* st = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("str")));
        strncpy(string10.str, st, sizeof(string10.str) - 1);        
        return (&string10);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hcuRetString10 *hcugetsbcfirmwarerev_1_svc (void *v, struct svc_req *s)
    {
        static hcuRetString10 string10;
        string10.str[9] = 0;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hcugetsbcfirmwarerev");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        string10.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* st = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("str")));
        strncpy(string10.str, st, sizeof(string10.str) - 1);        
        return (&string10);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hcuRetString10 *hcugetswrev_1_svc (void *v, struct svc_req *s)
    {
        static hcuRetString10 string10;
        string10.str[9] = 0;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hcugetswrev");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        string10.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* st = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("str")));
        strncpy(string10.str, st, sizeof(string10.str) - 1);        
        return (&string10);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hcuRetUShort *hcugetnumharddisk_1_svc (void *v, struct svc_req *s)
    {
        static hcuRetUShort ushort;
        //TODO 
        ushort.ulReturn = 0;
        ushort.usNum = 1;
        return (&ushort);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hcuRetDiskInfo *hcugetharddiskinfo_1_svc (u_short * num, struct svc_req *s)
    {
        static hcuRetDiskInfo_s hdinfo;
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hcugetharddiskinfo");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        hdinfo.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        hdinfo.usedSpace = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("usedSpace")));
        hdinfo.freeSpace = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("freeSpace")));
        hdinfo.capacity = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("capacity")));                        
        return (&hdinfo);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    u_long *hcuputlabel_1_svc (char **str, struct svc_req *s)
    {
        static u_long ulRet;
      
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hcuputlabel");
        rb_hash_aset(methodin, CSTR2SYM("str"), rb_str_new2(*str));        
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ulRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        return (&ulRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hcuRetString40 *hcugetlabel_1_svc (void *v, struct svc_req *s)
    {
        static hcuRetString40 string40;
        string40.str[39] = 0;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hcugetlabel");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        string40.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* model = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("str")));
        strncpy(string40.str, model, sizeof(string40.str) - 1);        
        return (&string40);
    }
    
/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hcuRetString *hcugetlabel_4_svc (void *v, struct svc_req *s)
    {
        static hcuRetString retStr = {0, 0};
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hcugetlabel");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        retStr.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* label = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("str")));
        retStr.str = label;
        return (&retStr);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hcuRetString *hcugetlocation_4_svc (void *v, struct svc_req *s)
    {
        static hcuRetString retStr = {0, 0};
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hcugetlocation");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        retStr.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* label = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("str")));
        retStr.str = label;
        return (&retStr);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hcuRetString *hcugetnotes_4_svc (void *v, struct svc_req *s)
    {
        static hcuRetString retStr = {0, 0};
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hcugetnotes");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        retStr.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* label = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("str")));
        retStr.str = label;
        return (&retStr);
    }
    

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    u_long *hcuputlocation_1_svc (char **loc, struct svc_req *s)
    {
        static u_long ulRet;
      
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hcuputlocation");
        rb_hash_aset(methodin, CSTR2SYM("location"), rb_str_new2(*loc));        
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ulRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        return (&ulRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hcuRetString255 *hcugetlocation_1_svc (void *v, struct svc_req *s)
    {
        static hcuRetString255 string255;
        string255.str[254] = 0;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hcugetlocation");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        string255.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* location = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("str")));
        strncpy(string255.str, location, sizeof(string255.str) - 1);        
        return (&string255);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    u_long *hcuputnotes_1_svc (char **str, struct svc_req *s)
    {
        static u_long ulRet;
      
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hcuputnotes");
        rb_hash_aset(methodin, CSTR2SYM("str"), rb_str_new2(*str));        
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ulRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        return (&ulRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hcuRetString1024 *hcugetnotes_1_svc (void *v, struct svc_req *s)
    {
        static hcuRetString1024 string1024;
        string1024.str[1023] = 0;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hcugetnotes");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        string1024.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* notes = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("str")));
        strncpy(string1024.str, notes, sizeof(string1024.str) - 1);        
        return (&string1024);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    u_long *hcuputuid_1_svc (u_long * id, struct svc_req *s)
    {
        static u_long ulRet;
      
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hcuputuid");
        rb_hash_aset(methodin, CSTR2SYM("uid"), INT2FIX(*id));        
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ulRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        return (&ulRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hcuRetULong *hcugetuid_1_svc (void *v, struct svc_req *s)
    {
        static hcuRetULong ulong;
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hcugetuid");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ulong.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        ulong.ulNum = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulNum")));        
        return (&ulong);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hcuRetUShort *hcugetnumioports_1_svc (void *v, struct svc_req *s)
    {
        static hcuRetUShort ushort;
        
        //Translate return to C
        ushort.ulReturn = 0;
        ushort.usNum = 1;        
        return (&ushort);
    }
    
///*****************************************************************************/
///*                                                                           */
///*****************************************************************************/
    unsigned long *hcuputioportinfo_1_svc (hcuIoPortInfo * pPortInfo, struct svc_req *s)
    {
        static unsigned long pRet = 0;
        return (&pRet);
    }
    
/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hcuRetIoPortInfo *hcugetioportinfo_1_svc (unsigned short *usPortId, struct svc_req *s)
    {

        static hcuRetIoPortInfo portInfo;
        portInfo.PortNo = *usPortId;
        if (*usPortId != 0) {        
			portInfo.usDevTypeDetected = 3;
			portInfo.usDevTypeAssigned = 3;
			portInfo.usDevId = 0;
			portInfo.usState = 0;
			portInfo.usAutoDetect = 1;
			portInfo.usStatus = 0;
			portInfo.ulReturn = 0;
        }
        else
        { 
			portInfo.usDevTypeDetected = 0;
			portInfo.usDevTypeAssigned = 0;
			portInfo.usDevId = 0;
			portInfo.usState = 0;
			portInfo.usAutoDetect = 0;
			portInfo.usStatus = 0;
			portInfo.ulReturn = 0;
		}

        return (&portInfo);
    }
    

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hcuRetUShort *hcugetnumslots_1_svc (void *v, struct svc_req *s)
    {
        static hcuRetUShort ushort;

        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hcugetnumslots");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ushort.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        ushort.usNum = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("usNum")));        
        return (&ushort);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    u_long *hcuputslotinfo_1_svc (hcuSlotInfo_s * pslotinfo, struct svc_req *s)
    {
        static u_long ulRet;
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hcuputslotinfo");
        rb_hash_aset(methodin, CSTR2SYM("slot"), INT2FIX(pslotinfo->slotNo));        
        rb_hash_aset(methodin, CSTR2SYM("assigned_id"), INT2FIX(pslotinfo->usDevTypeAssigned));                
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ulRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        return (&ulRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hcuRetSlotInfo *hcugetslotinfo_1_svc (u_short * num, struct svc_req *s)
    {
        static hcuRetSlotInfo_s slotinfo;
        //slotinfo.ulReturn = pHcuObj->getSlotInfo (*num, &slotinfo);
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hcugetslotinfo");
        rb_hash_aset(methodin, CSTR2SYM("slot"), INT2FIX(*num));                
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        slotinfo.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));        
        slotinfo.slotNo = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("slotNo")));
        slotinfo.usDefaultVMECfg = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("usDefaultVMECfg")));        
        slotinfo.usVMEAddrMode = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("usVMEAddrMode")));        
        slotinfo.ulVMEAddr = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulVMEAddr")));        
        slotinfo.usVMEIRQ = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("usVMEIRQ")));                                
        slotinfo.usAutoDetect = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("usAutoDetect")));        
        slotinfo.usStatus = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("usStatus")));        
        slotinfo.usState = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("usState")));        
        slotinfo.usDevId = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("usDevId")));                                
        slotinfo.usDevTypeDetected = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("usDevTypeDetected")));        
        slotinfo.usDevTypeAssigned = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("usDevTypeAssigned")));                                
        
        return (&slotinfo);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    u_long *hcuputscnetworkaddr_1_svc (char **addr, struct svc_req *s)
    {
        static u_long ulRet;
      
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hcuputscnetworkaddr");
        rb_hash_aset(methodin, CSTR2SYM("addr"), rb_str_new2(*addr));        
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ulRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        return (&ulRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hcuRetString20 *hcugetscnetworkaddr_1_svc (void *v, struct svc_req *s)
    {
        static hcuRetString20 string20;
        string20.str[19] = 0;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hcugetscnetworkaddr");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        string20.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* notes = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("str")));
        strncpy(string20.str, notes, sizeof(string20.str) - 1);        
        return (&string20);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hcuRetString20 *hcugetscgatewayaddr_1_svc (void *v, struct svc_req *s)
    {
        static hcuRetString20 string20;
        string20.str[19] = 0;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hcugetscgatewayaddr");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        string20.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* notes = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("str")));
        strncpy(string20.str, notes, sizeof(string20.str) - 1);        
        return (&string20);
    }
    
/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    u_long *hcuputscgatewayaddr_1_svc (char **addr, struct svc_req *s)
    {
        static u_long ulRet = 0;
      
        //Stick the method call and args in a hash
        //VALUE methodin = rb_hash_new();
        //SETHASHMETHOD(methodin, "hcuputscnetworkaddr");
        //rb_hash_aset(methodin, CSTR2SYM("addr"), rb_str_new2(*addr));        
        //VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        //rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        //ulRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        return (&ulRet);
    }
    

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    u_long *hcuputwatchdogstate_1_svc (short *state, struct svc_req *s)
    {
        static u_long ulRet;
        //ulRet = pEvtMgr->putWatchdogState (*state);
        //TODO        
        ulRet = 0;        
        return (&ulRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hcuRetShort *hcugetwatchdogstate_1_svc (void *v, struct svc_req *s)
    {
        static hcuRetShort retShort;
        //retShort.ulReturn = pEvtMgr->getWatchdogState (&retShort.sNum);
        //TODO
        retShort.ulReturn = 0;        
        retShort.sNum = 1;
        return (&retShort);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    u_long *hcuputwatchdoginterval_1_svc (u_long * interval, struct svc_req *s)
    {
        static u_long ulRet;
      
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hcuputwatchdoginterval");
        rb_hash_aset(methodin, CSTR2SYM("interval"), INT2FIX(*interval));        
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ulRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        return (&ulRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hcuRetULong *hcugetwatchdoginterval_1_svc (void *v, struct svc_req *s)
    {
        static hcuRetULong ulong;

        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hcugetwatchdoginterval");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ulong.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        ulong.ulNum = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulNum")));        
        return (&ulong);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    u_long *hcuputtime_1_svc (u_long * time, struct svc_req *s)
    {
        static u_long ulRet;
        //pHcuObj->putHcuTime (*time);
        ulRet = 0;
        return (&ulRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hcuRetULong *hcugettime_1_svc (void *v, struct svc_req *s)
    {
        static hcuRetULong retulong;
        //retulong.ulReturn = pHcuObj->getHcuTime (&retulong.ulNum);
        //TODO
        retulong.ulReturn = 0;
        retulong.ulNum = 15;
        return (&retulong);
    }
    
/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    u_long *hcucfgconnect_1_svc (void *v, struct svc_req *s)
    {
        static u_long ulRet;
        //ulRet = pHcuObj->RPCConfigConnect ();
        //TODO
        ulRet = 0;
        return (&ulRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    u_long *hcucfgdisconnect_1_svc (void *v, struct svc_req *s)
    {
        static u_long ulRet;
        //ulRet = pHcuObj->RPCConfigDisconnect ();
        ulRet = 0;
        return (&ulRet);
    }
    
/*****************************************************************************/
/* Put SNMP State                                                            */
/*****************************************************************************/
    unsigned long *hcuputsnmpstate_1_svc (short *state, struct svc_req *s)
    {
        static unsigned long ret = 0;
        return &ret;
    }
/*****************************************************************************/
/* Get SNMP State                                                            */
/*****************************************************************************/
    hcuSnmpState *hcugetsnmpstate_1_svc (void *v, struct svc_req *s)
    {
        static hcuSnmpState_s state;
        state.ulReturn = 0;
        state.snmpState = 0;
        return &state;
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    u_long *hcuevtconnect_1_svc (hcuEvtCnctInfo * pCnctInfo, struct svc_req *s)
    {
        static u_long ulRet = 0;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hcuevtconnect");
        //ulRet = pEvtMgr->Connect (pCnctInfo->ipAddr, pCnctInfo->port);        
        rb_hash_aset(methodin, CSTR2SYM("ipAddr"), rb_str_new2(pCnctInfo->ipAddr));  
        rb_hash_aset(methodin, CSTR2SYM("port"), INT2FIX(pCnctInfo->port));                      
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ulRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));

        return (&ulRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    u_long *hcuevtdisconnect_1_svc (void *v, struct svc_req *s)
    {
        static u_long ulRet = 0;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hcuevtdisconnect");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ulRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));

        return (&ulRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    u_long *hcuevtenableeventtransfer_1_svc (void *v, struct svc_req *s)
    {
        static u_long ulRet = 0;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hcuevtenableeventtransfer");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ulRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));

        return (&ulRet);
    }
    
    /*****************************************************************************/
    /*                                                                           */
    /*****************************************************************************/
    u_long *hcuevtisconnected_1_svc (void *v, struct svc_req *s)
    {
        static u_long ulRet = 0;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hcuevtisconnected");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ulRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));

        return (&ulRet);
            
    }
    

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    u_long *hcuevtdisableeventtransfer_1_svc (void *v, struct svc_req *s)
    {
        static u_long ulRet = 0;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hcuevtdisableeventtransfer");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ulRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));

        return (&ulRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    u_long *hcuputhighwatermark_1_svc (u_long * mark, struct svc_req *s)
    {
        static u_long ulRet = 0;
        //ulRet = pEvtMgr->PutHighWaterMark (*mark);
        return (&ulRet);
    }
    
/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    u_long *resetinterfaceconnections_1_svc (void *v, struct svc_req *s)
    {
        static u_long ulRet = 0;
        return (&ulRet);

    }


/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    u_long *resethcu_1_svc (unsigned long * ulSeconds, struct svc_req *s)
    {
        static u_long ulRet = 0;
        return (&ulRet);
    }
    
/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    u_long *hcusvcconnect_1_svc (void *v, struct svc_req *s)
    {
        static u_long ulRet = 0;
        return (&ulRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    u_long *hcusvcdisconnect_1_svc (void *v, struct svc_req *s)
    {
        static u_long ulRet = 0;        
        return (&ulRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hcuRetString10 *hcusvcgetsoftwarerev_1_svc (void *v, struct svc_req *s)
    {
        static hcuRetString10 string10;
        string10.str[9] = 0;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hcugetsbcfirmwarerev");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        string10.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* st = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("str")));
        strncpy(string10.str, st, sizeof(string10.str) - 1);        
        return (&string10);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hcuRetFileInfo *hcusvcfindswfilefirst_1_svc (void *v, struct svc_req *s)
    {        
        static hcuRetFileInfo FileInfo;
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hcusvcfindswfilefirst");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        FileInfo.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* fn = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("filename")));
        strncpy(FileInfo.filename, fn, sizeof(FileInfo.filename) - 1);        
        FileInfo.usMajorRev = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("usMajorRev")));        
        FileInfo.usMinorRev = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("usMinorRev")));        
        FileInfo.ulTime = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulTime")));        
        FileInfo.ulSize = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulSize")));                                
        
        return (&FileInfo);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    hcuRetFileInfo *hcusvcfindswfilenext_1_svc (void *v, struct svc_req *s)
    {
        static hcuRetFileInfo FileInfo;
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hcusvcfindswfilenext");
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        FileInfo.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* fn = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("filename")));
        strncpy(FileInfo.filename, fn, sizeof(FileInfo.filename) - 1);        
        FileInfo.usMajorRev = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("usMajorRev")));        
        FileInfo.usMinorRev = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("usMinorRev")));        
        FileInfo.ulTime = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulTime")));        
        FileInfo.ulSize = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulSize")));                                
        
        return (&FileInfo);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *hcusvcinstallsw_1_svc (hcuFileInfo * FileInfo, struct svc_req *s)
    {
        static unsigned long ulReturn = 0;

        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "hcusvcinstallsw");
        rb_hash_aset(methodin, CSTR2SYM("filename"), rb_str_new2(FileInfo->filename));        
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        
        return (&ulReturn);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *hcusvcclean_1_svc (char **c, struct svc_req *s)
    {
        static u_long ulRet;
        ulRet = 0;              // TODO: Insert the call the the HCU object here
        //HcuClean ();
        return (&ulRet);
    }
    
    u_long *startucdsystemscan_2_svc(bool_t *b, struct svc_req *s)
    {
        static u_long ret = 0;
        return (&ret);
    }

    static char s_ucdDatabaseId[50];
    u_long *syncdatabase_2_svc(HcuDatabases *dbs, struct svc_req *s)
    {
        static u_long ret = 0;
        //TODO add this to ruby
        strncpy(s_ucdDatabaseId, dbs->databaseId, 50);
        return (&ret);
    }
    
    struct HcuDatabaseId* hcugetdatabaseid_2_svc(void *v, struct svc_req *s)
    {
        static struct HcuDatabaseId rv;

        rv.status = 0;
        rv.id = s_ucdDatabaseId;

        return &rv;
    }
    
    //TODO Add this to ruby
    static int s_maxSAs = 2;
    hcuRetUShort * hcugetrpm3000maxnumsas_3_svc(void *v, struct svc_req *s)
    {
        static hcuRetUShort rv;
        rv.ulReturn = 0;
        rv.usNum = s_maxSAs;
        return &rv;
    }

    u_long * hcusetrpm3000maxnumsas_3_svc(u_short * maxNumSAs, struct svc_req *s)
    {
        static u_long rv = 0;
        s_maxSAs = *maxNumSAs;
        return &rv;
    }
    
    

    
