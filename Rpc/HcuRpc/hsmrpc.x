struct hsmRetString_s
{
    unsigned long ulReturn;
    string        str<>;
};
typedef struct hsmRetString_s hsmRetString;

struct hsmRetString10_s
{
    unsigned long ulReturn;
    char          str[10];
};
typedef struct hsmRetString10_s hsmRetString10;

struct hsmRetString15_s
{
    unsigned long ulReturn;
    char          str[15];
};
typedef struct hsmRetString15_s hsmRetString15;

struct hsmRetString20_s
{
    unsigned long ulReturn;
    char          str[20];
};
typedef struct hsmRetString20_s hsmRetString20;

struct hsmRetString40_s
{
    unsigned long ulReturn;
    char          str[40];
};
typedef struct hsmRetString40_s hsmRetString40;

struct hsmRetString255_s
{
    unsigned long ulReturn;
    char          str[255];
};
typedef struct hsmRetString255_s hsmRetString255;

struct hsmRetString1024_s
{
    unsigned long ulReturn;
    char          str[1024];
};
typedef struct hsmRetString1024_s hsmRetString1024;

struct hsmRetShort_s
{
    unsigned long ulReturn;
    short         sNum;
};
typedef struct hsmRetShort_s hsmRetShort;

struct hsmRetUShort_s
{
    unsigned long  ulReturn;
    unsigned short usNum;
};
typedef struct hsmRetUShort_s hsmRetUShort;

struct hsmRetULong_s
{
    unsigned long  ulReturn;
    unsigned long  ulNum;
};
typedef struct hsmRetULong_s hsmRetULong;


struct hsmRetDouble_s
{
    unsigned long  ulReturn;
    double         dDouble;
};
typedef struct hsmRetDouble_s hsmRetDouble;

struct hsmRouteInfo_s
{
	unsigned short		usRpmId;
    unsigned short		usPortId;
	unsigned long		Timeout;
	unsigned long		ulDwellTime;
	double 		dwStartFreq;
	double		dwStopFreq;
	int 		nDetMode;
	int			nVBW;
	int			nRBW;

};
typedef struct hsmRouteInfo_s hsmRouteInfo;

struct hsmRouteInfoRet_s
{
    unsigned long 	ulReturn;
	unsigned short		usRpmId;
    unsigned short		usPortId;
	unsigned long		Timeout;
	unsigned long		ulDwellTime;
	double 		dwStartFreq;
	double		dwStopFreq;
	int 		nDetMode;
	int			nVBW;
	int			nRBW;

};
typedef struct hsmRouteInfoRet_s hsmRouteInfoRet;

struct hsmRoutingList_s
{
    unsigned long ulReturn;
    unsigned short  ulListLength;
	unsigned long usRouteList[50];
};

typedef struct hsmRoutingList_s hsmRoutingList;

struct hsmRetFileInfo_s {
    unsigned long  ulReturn;
    char           filename[255];
    unsigned short usMajorRev;
    unsigned short usMinorRev;
    unsigned long  ulTime;
    unsigned long  ulSize;
};
typedef struct hsmRetFileInfo_s hsmRetFileInfo;

struct hsmFileInfo_s {
    unsigned short usHsmId;
    char           filename[255];
};
typedef struct hsmFileInfo_s hsmFileInfo;

struct broadcastPortId_s {
     unsigned short usRpmId;
	 unsigned short usPortId;
};
typedef struct broadcastPortId_s broadcastPortId;

const MAX_IP_STRING_SIZE = 50;

struct virtualhsmConfiguration_s {
     int uid;
	 char ip[MAX_IP_STRING_SIZE];
};
typedef struct virtualhsmConfiguration_s virtualhsmConfiguration;

struct virtualhsmAddConfiguration_s {
	 char masterIp[MAX_IP_STRING_SIZE];
};
typedef struct virtualhsmAddConfiguration_s virtualhsmAddConfiguration;

struct virtualhsmAddReturn_s {
     int masterUid;
	unsigned long ulReturn;
};
typedef struct virtualhsmAddReturn_s virtualhsmAddReturn;

const MAX_NODE_ENTRY_LABEL_SIZE = 40;
struct virtualhsmNodeCatalogEntry_s
{
    int uid;
    char label[MAX_NODE_ENTRY_LABEL_SIZE];
};
typedef struct virtualhsmNodeCatalogEntry_s virtualhsmNodeCatalogEntry;

const MAX_NODE_CATALOG_SIZE = 128;

struct virtualhsmNodecatalog_s {
	 int hcuUid;
     int numberPorts;
	 virtualhsmNodeCatalogEntry nodes[MAX_NODE_CATALOG_SIZE]; /* 15 cards x 8 ports */
};
typedef struct virtualhsmNodecatalog_s virtualhsmNodeCatalog;

struct virtualhsmMasterOnlineReturn_s {
     unsigned long ulReturn;
	 virtualhsmNodeCatalog catalog;
};
typedef struct virtualhsmMasterOnlineReturn_s virtualhsmMasterOnlineReturn;

struct virtualHsmBroadcastSource_s
{
	int localHcuUid;
	int portUid;
};
typedef struct virtualHsmBroadcastSource_s virtualHsmBroadcastSource;

struct virtualHsmRouteInfo_s
{
	virtualHsmBroadcastSource source;
	hsmRouteInfo routeInfo;
	char portLabel[MAX_NODE_ENTRY_LABEL_SIZE];
};
typedef struct virtualHsmRouteInfo_s virtualHsmRouteInfo;

struct virtualHsmRestartRouteReturn_s
{
    unsigned long status;
	char portLabel[MAX_NODE_ENTRY_LABEL_SIZE];
};
typedef struct virtualHsmRestartRouteReturn_s virtualHsmRestartRouteReturn;

#define BROADCAST_SA_MAX_VALS 500
struct virtualHsmBroadcastData_s
{
	virtualHsmBroadcastSource source;
	short sData<>;
	unsigned short usOverflow;
};
typedef struct virtualHsmBroadcastData_s virtualHsmBroadcastData;

struct virtualHsmCreateBroadcastReturn_s
{
	bool broadcastState;
	unsigned long ulReturn;
};
typedef struct virtualHsmCreateBroadcastReturn_s virtualHsmCreateBroadcastReturn;

struct hsmOnline_s
{
    int uid;
    char ip[MAX_IP_STRING_SIZE];
};
typedef struct hsmOnline_s hsmOnline;

struct virtualHsmMasterOnline_s
{
    int masterUid;
    char masterIp[MAX_IP_STRING_SIZE];
};
typedef struct virtualHsmMasterOnline_s virtualHsmMasterOnline;

program HSMROUTESVR  {
	version HSMROUTESVRVERS {
	unsigned long	hsmRouteConnect( void )					= 1;
	unsigned long	hsmRouteDisconnect(void)				= 2;
    unsigned long	hsmRoute( hsmRouteInfo )				= 3;
	unsigned long	hsmStopRoute( broadcastPortId)			= 4;
    unsigned long	hsmModifyParameters( hsmRouteInfo )		= 5;
    hsmRoutingList	hsmGetRoutingList( void )				= 6;
	hsmRouteInfoRet	hsmGetParams( broadcastPortId )			= 7;
	unsigned long  hsmRemoveAll ( void )                    = 8;
	unsigned long  hsmRemoveAllComprehensive ( void )       = 9;
     } = 1;
} = 0x2000000c;




program HSMSERVICESVR {
    version HSMSERVICESVRVERS {
	unsigned long  hsmSvcConnect ( void )                      	=  1;
	unsigned long  hsmSvcDisconnect ( void )                   	=  2;
    hsmRetString10 hsmSvcGetFirmwareRev ( void )				=  3;
    hsmRetString10 hsmSvcGetBootFirmwareRev( void )	            =  4;
    unsigned long  hsmSvcInstallFW ( hsmFileInfo )				=  5;
    hsmRetFileInfo hsmSvcFindFWFileFirst ( void )				=  6;
    hsmRetFileInfo hsmSvcFindFWFileNext ( void )				=  7;
    } = 1;
} = 0x2000000d;

program HSMCFGSVR {
   	version HSMCFGSVRVERS  {
	unsigned long	hsmCfgConnect( void )					= 1;
	unsigned long	hsmCfgDisconnect( void )				= 2;
	hsmRetString15	hsmGetSerialNumber( void )				= 3;
	hsmRetString10	hsmGetFirmwareRev( void )				= 4;
	unsigned long	hsmPutTelemetryFreq( double )			= 5;
	hsmRetDouble	hsmGetTelemetryFreq(void)				= 6;
	unsigned long	hsmPutTelemetryLevel( unsigned long )	= 7;
	hsmRetULong		hsmGetTelemetryLevel( void )			= 8;
	hsmRetShort		hsmGetState( void )						= 9;
	unsigned long	hsmPutState( unsigned short )		    = 10;
	unsigned long   hsmPutUid ( unsigned long )             = 11;
	hsmRetULong     hsmGetUid ( void )                      = 12;
	hsmRetShort		hsmGetBroadcastState(void)			    = 13;
	unsigned long   hsmPutBroadcastState(unsigned short)    = 14;
	hsmRetString40  hsmGetModelNo ( void )                  = 15;
	unsigned long   hsmPutLabel ( string )                  = 16;
	hsmRetString40  hsmGetLabel ( void )                    = 17;
	unsigned long   hsmPutNotes ( string )                  = 18;
	hsmRetString1024  hsmGetNotes ( void )                  = 19;
	unsigned long   hsmPutLocation ( string )               = 20;
	hsmRetString255 hsmGetLocation ( void )                 = 21;


   	} = 1;
   	version HSMCFGSVRVERS_2  {
    unsigned long hsmAddPhysicalHsm( void )                    = 22;
    unsigned long hsmAddVirtualHsm( virtualhsmAddConfiguration ) = 23;
    unsigned long hsmRemoveHsm( void )                         = 24;
   	} = 2;
   	
    version HSMCFGSVRVERS_3 {
        hsmRetString hsmGetLabel ( void )                   = 17;
        hsmRetString hsmGetNotes ( void )                   = 19;
        hsmRetString hsmGetLocation ( void )                = 21;
    } = 3;
} = 0x2000000e;




