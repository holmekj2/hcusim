#include <rpc/rpc.h>
#include "rpm1000general.h"
#include "ruby.h"
#include "../cruby_utils.h"
#include <stdio.h>

//This is the RPC server main calls
extern int rpmgenRpcSvrMain ();

//Class singleton (i.e. a ruby class object)
VALUE rpmgenRpc;
//Object singleton. Using this implies that this class is a singleton class since all objects will use this global.
//This is necessary if something other than Ruby is calling c methods (e.g. RPC server). 
VALUE rpcInstance;

VALUE initRpmgen(VALUE self, VALUE robject)
{
  //Assign the Ruby object the implementing Ruby object
  rpcInstance = robject;
  rb_iv_set(self, "@robject", robject);
  return self;
}

//VALUE startRpmgenRpcService(VALUE self)
void startRpmgenRpcService(unsigned long registerProtocol)
{
  //Start the RPC service
  rpmgenRpcSvrMain(registerProtocol);
  //return Qnil;  
}

//void Init_RpmgenRpc() {
  //Create mapping from ruby to c. Ruby object will be named XdrCommon
//  rpmgenRpc = rb_define_class("RpmgenRpc", rb_cObject);

//  rb_define_method(rpmgenRpc, "initialize", init, 1);
//  rb_define_method(rpmgenRpc, "start_rpc_service", startRpcService, 0);  
//}    

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *rpm1000cfgconnect_1_svc (unsigned short *pusRpmId, struct svc_req *s)
    {
        static unsigned long uRet;
        //TODO        
        ////uRet = pRpmMgr->CfgConnect (*pusRpmId);
        return (&uRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *rpm1000cfgdisconnect_1_svc (unsigned short *pusRpmId, struct svc_req *s)
    {
        static unsigned long uRet;
        //TODO        
        ////uRet = pRpmMgr->CfgDisconnect (*pusRpmId);
        return (&uRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *rpm1000cfgputstate_1_svc (Rpm1000Short * pRpm1000Short, struct svc_req *s)
    {
        static unsigned long uRet;
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000cfgputstate");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(pRpm1000Short->usRpmId));                
        rb_hash_aset(methodin, CSTR2SYM("state"), INT2FIX(pRpm1000Short->sShort));                        
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        uRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        return (&uRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    Rpm1000RetShort *rpm1000cfggetstate_1_svc (unsigned short *pusRpmId, struct svc_req *s)
    {
        static Rpm1000RetShort uRet;
        ////uRet.ulReturn = pRpmMgr->GetState (*pusRpmId, &uRet.sShort);
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000cfggetstate");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(*pusRpmId));                
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        //Translate return to C
        uRet.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        uRet.sShort = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("state")));        
        
        return (&uRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    Rpm1000RetString40 *rpm1000cfggetmodelno_1_svc (unsigned short *pusRpmId, struct svc_req *s)
    {
        static Rpm1000RetString40 string40;
        string40.str[39] = 0;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000cfggetmodelno");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(*pusRpmId));                
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        string40.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* model = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("str")));
        strncpy(string40.str, model, sizeof(string40.str) - 1);        
        return (&string40);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    Rpm1000RetString15 *rpm1000cfggetserialno_1_svc (unsigned short *pusRpmId, struct svc_req *s)
    {
        static Rpm1000RetString15 retString;
        retString.str[14] = 0;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000cfggetserialno");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(*pusRpmId));                
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        retString.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* serial = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("str")));
        strncpy(retString.str, serial, sizeof(retString.str) - 1);        
        return (&retString);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    Rpm1000RetString10 *rpm1000cfggetfirmwarerev_1_svc (unsigned short *pusRpmId, struct svc_req *s)
    {
        static Rpm1000RetString10 retString;
        retString.str[9] = 0;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000cfggetfirmwarerev");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(*pusRpmId));                
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        retString.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* fwVersion = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("str")));
        strncpy(retString.str, fwVersion, sizeof(retString.str) - 1);        
        return (&retString);
    }

/*****************************************************************************/
/* Added for RPM 3000 firmware requests                                      */
/*****************************************************************************/
    Rpm1000RetString10 *rpm3000cfggetfirmwarerev_1_svc (Rpm3000Short *pFwReq, struct svc_req *s)
    {
        //uRet.ulReturn = pRpmMgr->GetFirmwareRev (pFwReq->usRpmId, pFwReq->sShort, uRet.str);
        static Rpm1000RetString10 retString;
        retString.str[9] = 0;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000cfggetfirmwarerev");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(pFwReq->usRpmId));                
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        retString.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* fwVersion = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("str")));
        strncpy(retString.str, fwVersion, sizeof(retString.str) - 1);        
        return (&retString);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    Rpm1000RetString10 *rpm1000cfggetbootfirmwarerev_1_svc (unsigned short *pusRpmId, struct svc_req *s)
    {
        static Rpm1000RetString10 retString;
        retString.str[9] = 0;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000cfggetfirmwarerev");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(*pusRpmId));                
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        retString.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* fwVersion = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("str")));
        strncpy(retString.str, fwVersion, sizeof(retString.str) - 1);        
        return (&retString);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *rpm1000cfgputlabel_1_svc (Rpm1000String40 * pRpmIdStr, struct svc_req *s)
    {
        static unsigned long uRet;
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000cfgputlabel");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(pRpmIdStr->usRpmId));
        rb_hash_aset(methodin, CSTR2SYM("str"), rb_str_new2(pRpmIdStr->str));
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        uRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        return (&uRet);
    }
    
/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *rpm1000cfgputlabel_2_svc (Rpm1000String * pRpmIdStr, struct svc_req *s)
    {
        static unsigned long uRet;
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000cfgputlabel");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(pRpmIdStr->usRpmId));
        rb_hash_aset(methodin, CSTR2SYM("str"), rb_str_new2(pRpmIdStr->str));
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        uRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        return (&uRet);
    }
    

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    Rpm1000RetString40 *rpm1000cfggetlabel_1_svc (unsigned short *pRpmId, struct svc_req *s)
    {
        static Rpm1000RetString40 ret;
        ret.str[39] = 0;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000cfggetlabel");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(*pRpmId));                
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ret.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* label = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("str")));
        strncpy(ret.str, label, sizeof(ret.str) - 1);        
        return (&ret);
        
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    Rpm1000RetString *rpm1000cfggetlabel_2_svc (unsigned short *pRpmId, struct svc_req *s)
    {
        static Rpm1000RetString ret = {0, 0};

        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000cfggetlabel");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(*pRpmId));                
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ret.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* label = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("str")));
        ret.str = label;
        return (&ret);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *rpm1000cfgputnotes_1_svc (Rpm1000String1024 * pRpmIdStr, struct svc_req *s)
    {
        static unsigned long uRet;
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000cfgputnotes");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(pRpmIdStr->usRpmId));
        rb_hash_aset(methodin, CSTR2SYM("str"), rb_str_new2(pRpmIdStr->str));
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        uRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        return (&uRet);
        
    }
    
/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *rpm1000cfgputnotes_2_svc (Rpm1000String * pRpmIdStr, struct svc_req *s)
    {
        static unsigned long uRet;
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000cfgputnotes");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(pRpmIdStr->usRpmId));
        rb_hash_aset(methodin, CSTR2SYM("str"), rb_str_new2(pRpmIdStr->str));
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        uRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        return (&uRet);

    }


/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    Rpm1000RetString1024 *rpm1000cfggetnotes_1_svc (unsigned short *pRpmId, struct svc_req *s)
    {
        static Rpm1000RetString1024 ret;
        ret.str[1023] = 0;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000cfggetnotes");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(*pRpmId));                
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ret.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* notes = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("str")));
        strncpy(ret.str, notes, sizeof(ret.str) - 1);        
        return (&ret);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    Rpm1000RetString *rpm1000cfggetnotes_2_svc (unsigned short *pRpmId, struct svc_req *s)
    {
        static Rpm1000RetString ret;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000cfggetnotes");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(*pRpmId));                
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ret.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* notes = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("str")));
        ret.str = notes;
        //strncpy(ret.str, notes, sizeof(ret.str) - 1);        
        return (&ret);
    }    

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    Rpm1000RetFreqs *rpm1000cfggetfreqrange_1_svc (unsigned short *pRpmId, struct svc_req *s)
    {
        static Rpm1000RetFreqs uRet;
        ////uRet.ulReturn = pRpmMgr->GetState (*pusRpmId, &uRet.sShort);
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000cfggetfreqrange");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(*pRpmId));                
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        //Translate return to C
        uRet.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        uRet.dMinFreq = NUM2DBL(rb_hash_aref(rreturn, CSTR2SYM("dMinFreq")));        
        uRet.dMaxFreq = NUM2DBL(rb_hash_aref(rreturn, CSTR2SYM("dMaxFreq")));                
        
        return (&uRet);
        
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    Rpm1000RetULong *rpm1000cfggetcalibtime_1_svc (unsigned short *pRpmId, struct svc_req *s)
    {
        static Rpm1000RetULong uRet;
        //uRet.ulReturn = pRpmMgr->GetCalibTime (*pRpmId, &uRet.ulLong);
        //TODO
        uRet.ulLong = 0;
        return (&uRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    Rpm1000RetULong *rpm1000cfggetcalibduetime_1_svc (unsigned short *pRpmId, struct svc_req *s)
    {
        static Rpm1000RetULong uRet;
        //uRet.ulReturn = pRpmMgr->GetCalibDueTime (*pRpmId, &uRet.ulLong);
        return (&uRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *rpm1000cfgputuid_1_svc (Rpm1000ULong * pRpmIdUl, struct svc_req *s)
    {
        static unsigned long uRet;
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000cfgputuid");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(pRpmIdUl->usRpmId));                
        rb_hash_aset(methodin, CSTR2SYM("uid"), INT2FIX(pRpmIdUl->ulLong));                        
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        uRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        return (&uRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    Rpm1000RetULong *rpm1000cfggetuid_1_svc (unsigned short *pRpmId, struct svc_req *s)
    {
        static Rpm1000RetULong uRet;
        ////uRet.ulReturn = pRpmMgr->GetState (*pusRpmId, &uRet.sShort);
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000cfggetuid");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(*pRpmId));                
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        //Translate return to C
        uRet.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        uRet.ulLong = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("uid")));        
        
        return (&uRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    Rpm1000RetUShort *rpm1000cfggetnumports_1_svc (unsigned short *pRpmId, struct svc_req *s)
    {
        static Rpm1000RetUShort uRet;
        //uRet.ulReturn = pRpmMgr->GetNumPorts (*pRpmId, &uRet.usShort);
        uRet.usShort = 8;
        return (&uRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *rpm1000cfgputportstate_1_svc (Rpm1000PortShort * pRpmPortIdSh, struct svc_req *s)
    {
        //TODO
        static unsigned long uRet = 0;
        //uRet = pRpmMgr->PutPortState (pRpmPortIdSh->usRpmId, pRpmPortIdSh->usPortId, pRpmPortIdSh->sShort);
        return (&uRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    Rpm1000RetShort *rpm1000cfggetportstate_1_svc (Rpm1000Port * pRpmPortId, struct svc_req *s)
    {
        //TODO
        static Rpm1000RetShort uRet;
        //uRet.ulReturn = pRpmMgr->GetPortState (pRpmPortId->usRpmId, pRpmPortId->usPortId, &uRet.sShort);
        uRet.ulReturn = 0;        
        uRet.sShort = 1;
        return (&uRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *rpm1000cfgputportlabel_1_svc (Rpm1000PortString40 * pRpmPortIdStr, struct svc_req *s)
    {
        static unsigned long uRet;
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000cfgputportlabel");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(pRpmPortIdStr->usRpmId));
        rb_hash_aset(methodin, CSTR2SYM("port"), INT2FIX(pRpmPortIdStr->usPortId));        
        rb_hash_aset(methodin, CSTR2SYM("str"), rb_str_new2(pRpmPortIdStr->str));
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        uRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        return (&uRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    Rpm1000RetString40 *rpm1000cfggetportlabel_1_svc (Rpm1000Port * pRpmPortId, struct svc_req *s)
    {
        static Rpm1000RetString40 ret;
        ret.str[39] = 0;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000cfggetportlabel");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(pRpmPortId->usRpmId));                
        rb_hash_aset(methodin, CSTR2SYM("port"), INT2FIX(pRpmPortId->usPortId));                        
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ret.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* label = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("str")));
        strncpy(ret.str, label, sizeof(ret.str) - 1);        
        return (&ret);
      
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *rpm1000cfgputportlabel_2_svc (Rpm1000PortString * pRpmPortIdStr, struct svc_req *s)
    {
        static unsigned long uRet;
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000cfgputportlabel");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(pRpmPortIdStr->usRpmId));
        rb_hash_aset(methodin, CSTR2SYM("port"), INT2FIX(pRpmPortIdStr->usPortId));        
        rb_hash_aset(methodin, CSTR2SYM("str"), rb_str_new2(pRpmPortIdStr->str));
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        uRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        return (&uRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    Rpm1000RetString *rpm1000cfggetportlabel_2_svc (Rpm1000Port * pRpmPortId, struct svc_req *s)
    {
        static Rpm1000RetString ret;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000cfggetportlabel");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(pRpmPortId->usRpmId));                
        rb_hash_aset(methodin, CSTR2SYM("port"), INT2FIX(pRpmPortId->usPortId));                        
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ret.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* label = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("str")));
        ret.str = label;
        return (&ret);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *rpm1000cfgputportlocation_1_svc (Rpm1000PortString255 * pRpmPortIdStr, struct svc_req *s)
    {
        static unsigned long uRet = 0;
        //uRet = pRpmMgr->PutPortLocation (pRpmPortIdStr->usRpmId, pRpmPortIdStr->usPortId, pRpmPortIdStr->str);
        return (&uRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    Rpm1000RetString255 *rpm1000cfggetportlocation_1_svc (Rpm1000Port * pRpmPortId, struct svc_req *s)
    {
        static Rpm1000RetString255 uRet;
        //uRet.ulReturn = pRpmMgr->GetPortLocation (pRpmPortId->usRpmId, pRpmPortId->usPortId, uRet.str);
        return (&uRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *rpm1000cfgputportlocation_2_svc (Rpm1000PortString * pRpmPortIdStr, struct svc_req *s)
    {
        static unsigned long uRet = 0;
        //uRet = pRpmMgr->PutPortLocation (pRpmPortIdStr->usRpmId, pRpmPortIdStr->usPortId, pRpmPortIdStr->str);
        return (&uRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    Rpm1000RetString *rpm1000cfggetportlocation_2_svc (Rpm1000Port * pRpmPortId, struct svc_req *s)
    {
        static Rpm1000RetString uRet;
        //uRet.ulReturn = pRpmMgr->GetPortLocation (pRpmPortId->usRpmId, pRpmPortId->usPortId, uRet.str);
        uRet.ulReturn = 0;        
        char* c = "";
        uRet.str = c;
        return (&uRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *rpm1000cfgputportnotes_1_svc (Rpm1000PortString1024 * pRpmPortIdStr, struct svc_req *s)
    {
        static unsigned long uRet;
        //uRet = pRpmMgr->PutPortNotes (pRpmPortIdStr->usRpmId, pRpmPortIdStr->usPortId, pRpmPortIdStr->str);
        return (&uRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    Rpm1000RetString1024 *rpm1000cfggetportnotes_1_svc (Rpm1000Port * pRpmPortId, struct svc_req *s)
    {
        static Rpm1000RetString1024 uRet;
        //uRet.ulReturn = pRpmMgr->GetPortNotes (pRpmPortId->usRpmId, pRpmPortId->usPortId, uRet.str);
        return (&uRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *rpm1000cfgputportnotes_2_svc (Rpm1000PortString * pRpmPortIdStr, struct svc_req *s)
    {
        static unsigned long uRet;
        //uRet = pRpmMgr->PutPortNotes (pRpmPortIdStr->usRpmId, pRpmPortIdStr->usPortId, pRpmPortIdStr->str);
        return (&uRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    Rpm1000RetString *rpm1000cfggetportnotes_2_svc (Rpm1000Port * pRpmPortId, struct svc_req *s)
    {
        static Rpm1000RetString uRet;
        uRet.ulReturn = 0;
        char* c = "";
        uRet.str = c;
        return (&uRet);
    }
/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *rpm1000cfgputporttestpntcomp_1_svc (Rpm1000PortDouble * pRpmPortIdDbl, struct svc_req *s)
    {
        static unsigned long uRet;
        //uRet = pRpmMgr->PutPortTestPntComp (pRpmPortIdDbl->usRpmId, pRpmPortIdDbl->usPortId, pRpmPortIdDbl->dDouble);
        return (&uRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    Rpm1000RetDouble *rpm1000cfggetporttestpntcomp_1_svc (Rpm1000Port * pRpmPortId, struct svc_req *s)
    {
        static Rpm1000RetDouble uRet;
        //uRet.ulReturn = pRpmMgr->GetPortTestPntComp (pRpmPortId->usRpmId, pRpmPortId->usPortId, &uRet.dDouble);
        return (&uRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *rpm1000cfgputportmaxinputpower_1_svc (Rpm1000PortDouble * pRpmPortIdDbl, struct svc_req *s)
    {
        static unsigned long uRet;
        //uRet = pRpmMgr->PutPortMaxInputPower (pRpmPortIdDbl->usRpmId, pRpmPortIdDbl->usPortId, pRpmPortIdDbl->dDouble);
        return (&uRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    Rpm1000RetDouble *rpm1000cfggetportmaxinputpower_1_svc (Rpm1000Port * pRpmPortId, struct svc_req *s)
    {
        static Rpm1000RetDouble uRet;
        //uRet.ulReturn = pRpmMgr->GetPortMaxInputPower (pRpmPortId->usRpmId, pRpmPortId->usPortId, &uRet.dDouble);
        return (&uRet);
    }
/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *rpm1000cfgputportuid_1_svc (Rpm1000PortULong * pRpmPortIdUl, struct svc_req *s)
    {
        static unsigned long uRet;
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000cfgputportuid");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(pRpmPortIdUl->usRpmId));                
        rb_hash_aset(methodin, CSTR2SYM("port"), INT2FIX(pRpmPortIdUl->usPortId));                        
        rb_hash_aset(methodin, CSTR2SYM("uid"), INT2FIX(pRpmPortIdUl->ulLong));                        
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        uRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        return (&uRet);
        
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    Rpm1000RetULong *rpm1000cfggetportuid_1_svc (Rpm1000Port * pRpmPortId, struct svc_req *s)
    {
        static Rpm1000RetULong uRet;
        ////uRet.ulReturn = pRpmMgr->GetState (*pusRpmId, &uRet.sShort);
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000cfggetportuid");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(pRpmPortId->usRpmId));                
        rb_hash_aset(methodin, CSTR2SYM("port"), INT2FIX(pRpmPortId->usPortId));                        
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        //Translate return to C
        uRet.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        uRet.ulLong = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("uid")));        
        
        return (&uRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
     unsigned long *rpmdeploylicensekey_1_svc (RpmLicenseKey * pLicenseKey, struct svc_req *s)
    {
        static unsigned long uRet;
        ////uRet.ulReturn = pRpmMgr->GetState (*pusRpmId, &uRet.sShort);
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpmdeploylicensekey");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(pLicenseKey->usRpmId));                
        rb_hash_aset(methodin, CSTR2SYM("key"), rb_str_new2(pLicenseKey->key));
        VALUE rreturn = rb_hash_new();
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        //Translate return to C
        uRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        return (&uRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
     RpmFeatures *rpmgetfeatures_1_svc (unsigned short *pRpmId, struct svc_req *s)
    {
        static RpmFeatures featureReturn;
        static RpmFeature featureArray[3];
        ////uRet.ulReturn = pRpmMgr->GetState (*pusRpmId, &uRet.sShort);
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpmgetfeatures");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(*pRpmId));                
        VALUE rreturn = rb_hash_new();
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);
        VALUE features = rb_hash_aref(rreturn, CSTR2SYM("features"));
        unsigned long uRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        VALUE feature;
        unsigned int numberFeatures = 0;
        while ((feature = rb_ary_shift(features)) != Qnil)
        {
            featureArray[numberFeatures].featureName = STR2CSTR(rb_hash_aref(feature, CSTR2SYM("name")));
            featureArray[numberFeatures].state = STR2CSTR(rb_hash_aref(feature, CSTR2SYM("state")));
            featureArray[numberFeatures].daysRemaining = NUM2UINT(rb_hash_aref(feature, CSTR2SYM("days_remaining")));
            numberFeatures++;            
        }
        featureReturn.features.features_len = numberFeatures;
        featureReturn.features.features_val = featureArray;        
        featureReturn.ulReturn = uRet;
        return (&featureReturn);
    }

    unsigned long *rpm1000cfgsyncconfig_1_svc (unsigned short* pRpmId, struct svc_req *s )
    {
        static unsigned long ulRet = 0;
        return (&ulRet);
    }

    unsigned long *rpm1000cfgsyncportconfig_1_svc (Rpm1000Port* pRpmPortId , struct svc_req *s )
    {
        static unsigned long ulRet = 0;
        return (&ulRet);
    }




/***************************************************************/
/*     E V E N T    I N T E R F A C E   F U N C T I O N S      */
/***************************************************************/
/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *rpm1000evtconnect_1_svc (void *v, struct svc_req *s)
    {
        static unsigned long ulRet = 0;
        //ulRet = pRpmMgr->EvtConnect ();
        return (&ulRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *rpm1000evtdisconnect_1_svc (void *v, struct svc_req *s)
    {
        static unsigned long ulRet = 0;
        //ulRet = pRpmMgr->EvtDisconnect ();
        return (&ulRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *rpm1000evtreqlimitstatus_1_svc (void *v, struct svc_req *s)
    {
        static unsigned long ulRet = 0;
        //ulRet = pRpmMgr->EvtReqLimitStatus ();
        return (&ulRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *rpm1000evtenablelimitevents_1_svc (void *v, struct svc_req *s)
    {
        static unsigned long ulRet = 0;
        //ulRet = pRpmMgr->EvtEnableLimitEvents ();
        return (&ulRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *rpm1000evtdisablelimitevents_1_svc (void *v, struct svc_req *s)
    {
        static unsigned long ulRet = 0;
        //ulRet = pRpmMgr->EvtDisableLimitEvents ();
        return (&ulRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *rpm1000evtacklimit_1_svc (Rpm1000Port * pRpmPortId, struct svc_req *s)
    {
        static unsigned long ulRet = 0;
        //ulRet = pRpmMgr->EvtAckLimits (pRpmPortId->usRpmId, pRpmPortId->usPortId);
        return (&ulRet);
    }

/*****************************************************************************/
/* PT2.2 Changes                                                             */
/*****************************************************************************/
    unsigned long *rpm1000evtacklimitex_1_svc (Rpm1000Port2 * pRpmPortId, struct svc_req *s)
    {
        static unsigned long ulRet = 0;
        //ulRet = pRpmMgr->EvtAckLimits (pRpmPortId->usRpmId, pRpmPortId->usPortId, pRpmPortId->Threshold);
        return (&ulRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *rpm1000evtackalllimits_1_svc (void *v, struct svc_req *s)
    {
        static unsigned long ulRet = 0;
        
        //ulRet = pRpmMgr->EvtAckAllLimits ();
        return (&ulRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    Rpm1000RetThreshStat *rpm1000evtreqthresholdstats_1_svc (Rpm1000PortThresh * pIn, struct svc_req *s)
    {
        static Rpm1000RetThreshStat ret;
        static TSThresh thresh;
        static TSPoint points[250];
        ret.Stats.ThreshStats.ThreshStats_val = &thresh;
        ret.Stats.ThreshStats.ThreshStats_len = 1;        
        thresh.Points.Points_val = points;
        thresh.Points.Points_len = 241;                

        //VALUE methodin = rb_hash_new();
        //SETHASHMETHOD(methodin, "evtreqthresholdstats");
        //rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(pIn->usRpmId));
        //rb_hash_aset(methodin, CSTR2SYM("port"), INT2FIX(pIn->usPortId));
        char* bReqThresh = pIn->bReqThresh;
        int threshold = 0;
        int i = 0;
        for (i = 0; i < 4; ++i)
        {
			if (bReqThresh[i])
			{
				threshold = i;
				break;
			}
		}
        //rb_hash_aset(methodin, CSTR2SYM("threshold"), INT2FIX(threshold));
        //VALUE rreturn = rb_hash_new();
        //Call the Ruby function to direct this to the right place
        //rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);
        unsigned long uRet = 0;//NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")))                
        ret.ulReturn = uRet;
        unsigned short majorVersion = 1;//rb_hash_aref(rreturn, CSTR2SYM("major_version"));
        ret.Stats.usPlanMajorRev = majorVersion;
        unsigned short minorVersion = 0;//rb_hash_aref(rreturn, CSTR2SYM("minor_version"));
        ret.Stats.usPlanMinorRev = minorVersion;
        
        thresh.ulStartTime = (unsigned long)time(NULL);
        thresh.ulStopTime = thresh.ulStartTime  +  1;
        thresh.usIndex = threshold;
        thresh.usNumScans = threshold;
        
        int frequency = 1000;
        for(i = 0; i < 241; ++i) 
        {
			points[i].usFrequency = frequency;
			frequency += 50;
			points[i].sLevel = -4700;
			points[i].bIsUpward = 1;
			points[i].bIsViolated = 1;
			points[i].sMinimum = -500;
			points[i].sMaximum = -400;
			points[i].sAverage = -490;
			points[i].usNumViolations = 2;
		}
        return (&ret);
    }

/***************************************************************/
/*   M O N I T O R   P L A N   I N T E R F A C E   F U N C S   */
/***************************************************************/

/***************************************************************/
/*   M O N I T O R   P L A N   I N T E R F A C E   F U N C S   */
/***************************************************************/

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *rpm1000monconnect_1_svc (Rpm1000Port * pRpmPortId, struct svc_req *s)
    {
        static unsigned long uRet;
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000monconnect");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(pRpmPortId->usRpmId));                
        rb_hash_aset(methodin, CSTR2SYM("port"), INT2FIX(pRpmPortId->usPortId));                        
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        uRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        return (&uRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *rpm1000mondisconnect_1_svc (Rpm1000Port * pRpmPortId, struct svc_req *s)
    {
        static unsigned long uRet;
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000mondisconnect");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(pRpmPortId->usRpmId));                
        rb_hash_aset(methodin, CSTR2SYM("port"), INT2FIX(pRpmPortId->usPortId));                        
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        uRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        return (&uRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *rpm1000monputmonstate_1_svc (Rpm1000PortShort * pRpmPortIdSh, struct svc_req *s)
    {
        static unsigned long uRet;
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000mondisconnect");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(pRpmPortIdSh->usRpmId));                
        rb_hash_aset(methodin, CSTR2SYM("port"), INT2FIX(pRpmPortIdSh->usPortId));                        
        rb_hash_aset(methodin, CSTR2SYM("state"), INT2FIX(pRpmPortIdSh->sShort));                                
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        uRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        return (&uRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    Rpm1000RetShort *rpm1000mongetmonstate_1_svc (Rpm1000Port * pRpmPortId, struct svc_req *s)
    {
        static Rpm1000RetShort rpmPortShort;
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000mongetmonstate");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(pRpmPortId->usRpmId));                
        rb_hash_aset(methodin, CSTR2SYM("port"), INT2FIX(pRpmPortId->usPortId));                        
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        //Translate return to C
        rpmPortShort.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        rpmPortShort.sShort = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("state")));        
        
        return (&rpmPortShort);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *rpm1000monputlimitstate_1_svc (Rpm1000PortShort * pRpmPortIdSh, struct svc_req *s)
    {
        static unsigned long ulRet = 0;
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000monputlimitstate");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(pRpmPortIdSh->usRpmId));                
        rb_hash_aset(methodin, CSTR2SYM("port"), INT2FIX(pRpmPortIdSh->usPortId));                        
        rb_hash_aset(methodin, CSTR2SYM("state"), INT2FIX(pRpmPortIdSh->sShort));                                
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        //Translate return to C
        ulRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        
        //if (logMask & LOG_RPC)
        //{
        //    logMsg ("rpm1000monputlimitstate, rpm %d, port %d, state %d\n",
        //            pRpmPortIdSh->usRpmId, pRpmPortIdSh->usPortId, pRpmPortIdSh->sShort, 0, 0, 0);
        //}

        //ulRet = pRpmMgr->MonPlanPutLimitState (pRpmPortIdSh->usRpmId, pRpmPortIdSh->usPortId, pRpmPortIdSh->sShort);
        return (&ulRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    Rpm1000RetShort *rpm1000mongetlimitstate_1_svc (Rpm1000Port * pRpmPortId, struct svc_req *s)
    {
        static Rpm1000RetShort rpmPortShort;
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000mongetlimitstate");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(pRpmPortId->usRpmId));                
        rb_hash_aset(methodin, CSTR2SYM("port"), INT2FIX(pRpmPortId->usPortId));                        
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        //Translate return to C
        rpmPortShort.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        rpmPortShort.sShort = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("state")));        
        
        return (&rpmPortShort);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *rpm1000monputlimittype_1_svc (Rpm1000PortLimitType * pRpmPortIdLimTyp, struct svc_req *s)
    {
        static unsigned long ulRet = 0;
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000monputlimittype");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(pRpmPortIdLimTyp->usRpmId));                
        rb_hash_aset(methodin, CSTR2SYM("port"), INT2FIX(pRpmPortIdLimTyp->usPortId));                        
        rb_hash_aset(methodin, CSTR2SYM("limit_id"), INT2FIX(pRpmPortIdLimTyp->usLimitId));                                
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        //Translate return to C
        ulRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        
        //if (logMask & LOG_RPC)
        //{
        //    logMsg ("rpm1000monputlimittype, rpm %d, port %d, limit %d type %d\n",
        //            pRpmPortIdLimTyp->usRpmId, pRpmPortIdLimTyp->usPortId, pRpmPortIdLimTyp->usLimitId, pRpmPortIdLimTyp->sType, 0, 0);
        //}

        //ulRet = pRpmMgr->MonPlanPutLimitType (pRpmPortIdLimTyp->usRpmId, pRpmPortIdLimTyp->usPortId,
        //                                      pRpmPortIdLimTyp->usLimitId, pRpmPortIdLimTyp->sType);
        return (&ulRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    Rpm1000RetShort *rpm1000mongetlimittype_1_svc (Rpm1000PortUShort * pRpmPortUShort, struct svc_req *s)
    {
        static Rpm1000RetShort rpmPortShort;
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000mongetlimittype");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(pRpmPortUShort->usRpmId));                
        rb_hash_aset(methodin, CSTR2SYM("port"), INT2FIX(pRpmPortUShort->usPortId));                        
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        //Translate return to C
        rpmPortShort.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        rpmPortShort.sShort = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("limit_id")));        
        
        //rpmPortShort.ulReturn = pRpmMgr->MonPlanGetLimitType (pRpmPortUShort->usRpmId, pRpmPortUShort->usPortId,
        //                                                      pRpmPortUShort->usShort, &rpmPortShort.sShort);
        rpmPortShort.ulReturn = 0;
        rpmPortShort.sShort = 0;        
        
        return (&rpmPortShort);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *rpm1000moninitmeasconfig_1_svc (Rpm1000Port * pRpmPortId, struct svc_req *s)
    {
        static unsigned long ulRet;
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000moninitmeasconfig");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(pRpmPortId->usRpmId));                
        rb_hash_aset(methodin, CSTR2SYM("port"), INT2FIX(pRpmPortId->usPortId));                        
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ulRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        return (&ulRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *rpm1000monputmeasconfig_1_svc (Rpm1000Meas * pRpmPortIdMeas, struct svc_req *s)
    {
        static unsigned long uRet;
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000monputmeasconfig");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(pRpmPortIdMeas->usRpmId));                
        rb_hash_aset(methodin, CSTR2SYM("port"), INT2FIX(pRpmPortIdMeas->usPortId));                        
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        uRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        return (&uRet);
      
        //if (logMask & LOG_RPC)
        //{
        //    logMsg ("rpm1000monputmeasconfig, rpm %d, port %d \n",
        //            pRpmPortIdMeas->usRpmId, pRpmPortIdMeas->usPortId, 0, 0, 0, 0);
        //}
        
        //ulRet = pRpmMgr->MonPlanPutMeasConfig (pRpmPortIdMeas->usRpmId, pRpmPortIdMeas->usPortId,
        //                                       pRpmPortIdMeas->planRev, pRpmPortIdMeas->measArray.measArray_len,
        //                                       pRpmPortIdMeas->measArray.measArray_val);
    }

///////////////////////////////////////////////////////////////////////////////
// Function: rpm1000mongetmeasconfig
//
// Description:
//  This function will return the monitoring plan currently used by the port
//  in the passed parameter (Rpm #/Port #).
//
// Parameters:
//  Rpm1000Port* pRpmPortId - the Rpm and Port associated with the monitoring plan.
//
// Returns:
//  Rpm1000RetMeas* - Returns the monitoring plan, the monitoring plan rev,
//  and the status of the function.
//
// Effects:
//  None
//
//////////////////////////////////////////////////////////////////////////////
    Rpm1000RetMeas *rpm1000mongetmeasconfig_1_svc (Rpm1000Port * pRpmPortId, struct svc_req *s)
    {
        static Rpm1000RetMeas ret;
        ret.ulReturn = 0;
        static unsigned long ulRet;
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000monputmeasconfig");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(pRpmPortId->usRpmId));                
        rb_hash_aset(methodin, CSTR2SYM("port"), INT2FIX(pRpmPortId->usPortId));                        
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ret.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        return (&ret);

        // Get the monitoring plan
       // ret.ulReturn = pRpmMgr->MonPlanGetMeasConfig (pRpmPortId->usRpmId,
       //                                               pRpmPortId->usPortId,
       //                                               &(ret.planRev),
       //                                               &(ret.measArray.measArray_len), &(ret.measArray.measArray_val));
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *rpm1000svcconnect_1_svc (unsigned short *pusRpmId, struct svc_req *s)
    {
        static unsigned long uRet = 0;
        //uRet = pRpmMgr->SvcConnect (pusRpmId);
        return (&uRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *rpm1000svcdisconnect_1_svc (unsigned short *pusRpmId, struct svc_req *s)
    {
        static unsigned long uRet = 0;
        //uRet = pRpmMgr->SvcDisconnect (pusRpmId);
        return (&uRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    Rpm1000RetString10 *rpm1000svcgetfirmwarerev_1_svc (unsigned short *pusRpmId, struct svc_req *s)
    {
        static Rpm1000RetString10 retString;
        retString.str[9] = 0;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000cfggetfirmwarerev");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(*pusRpmId));                
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        retString.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* fwVersion = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("str")));
        strncpy(retString.str, fwVersion, sizeof(retString.str) - 1);        
        return (&retString);
    }

/*****************************************************************************/
/* Added for RPM 3000 firmware requests                                      */
/*****************************************************************************/
    Rpm1000RetString10 *rpm3000svcgetfirmwarerev_1_svc (Rpm3000Short *pFwReq, struct svc_req *s)
    {
        static Rpm1000RetString10 retString;
        retString.str[9] = 0;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000cfggetfirmwarerev");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(pFwReq->usRpmId));                
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        retString.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* fwVersion = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("str")));
        strncpy(retString.str, fwVersion, sizeof(retString.str) - 1);        
        return (&retString);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    Rpm1000RetString10 *rpm1000svcgetbootfirmwarerev_1_svc (unsigned short *pusRpmId,struct svc_req *s)
    {
        static Rpm1000RetString10 retString;
        retString.str[9] = 0;
        
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000cfggetfirmwarerev");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(*pusRpmId));                
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        retString.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* fwVersion = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("str")));
        strncpy(retString.str, fwVersion, sizeof(retString.str) - 1);        
        return (&retString);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    RPM1000RetFileInfo *rpm1000svcfindfwfilefirst_1_svc (unsigned short *pusRpmId,struct svc_req *s )
    {
        static RPM1000RetFileInfo FileInfo;
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000svcfindfwfilefirst");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(*pusRpmId));                        
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        FileInfo.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* fn = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("filename")));
        strncpy(FileInfo.filename, fn, sizeof(FileInfo.filename) - 1);        
        FileInfo.usMajorRev = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("usMajorRev")));        
        FileInfo.usMinorRev = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("usMinorRev")));        
        FileInfo.ulTime = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulTime")));        
        FileInfo.ulSize = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulSize")));                                
        
        return (&FileInfo);
        
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    RPM1000RetFileInfo *rpm1000svcfindfwfilenext_1_svc (unsigned short *pusRpmId,struct svc_req *s )
    {
        static RPM1000RetFileInfo FileInfo;
        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000svcfindfwfilenext");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(*pusRpmId));                        
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        FileInfo.ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        char* fn = STR2CSTR(rb_hash_aref(rreturn, CSTR2SYM("filename")));
        strncpy(FileInfo.filename, fn, sizeof(FileInfo.filename) - 1);        
        FileInfo.usMajorRev = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("usMajorRev")));        
        FileInfo.usMinorRev = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("usMinorRev")));        
        FileInfo.ulTime = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulTime")));        
        FileInfo.ulSize = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulSize")));                                
        return (&FileInfo);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    unsigned long *rpm1000svcinstallfw_1_svc (RPM1000FileInfo * FileInfo, struct svc_req *s)
    {
        static unsigned long ulReturn = 0;

        //Stick the method call and args in a hash
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000svcinstallfw");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(FileInfo->usRpmId));                        
        rb_hash_aset(methodin, CSTR2SYM("filename"), rb_str_new2(FileInfo->filename));        
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ulReturn = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        
        return (&ulReturn);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    Rpm1000RetString * rpm1000cfggetportcustomlabelvalue_2_svc (Rpm1000PortCustomLabelKey * pRpmPortCustomLabelKey, struct svc_req *s)
    {
        static Rpm1000RetString uRet = {0, 0};
        char* c = "";
        uRet.str = c;
        return (&uRet);
    }

    /*****************************************************************************/
    /*                                                                           */
    /*****************************************************************************/
    u_long * rpm1000cfgsetportcustomlabelvalue_2_svc(Rpm1000PortCustomLabelKeyValue *pPortCustomLabelKeyValue, struct svc_req *s)
    {
        static unsigned long ulReturn = 0;
        return &ulReturn;
    }

    /*****************************************************************************/
    /*                                                                           */
    /*****************************************************************************/
    u_long * rpm1000cfgremoveportcustomlabel_2_svc(Rpm1000PortCustomLabelKey * pRpmPortCustomLabelKey, struct svc_req *s)
    {
        static unsigned long ulReturn = 0;
        return &ulReturn;
    }

    /*****************************************************************************/
    /*                                                                           */
    /*****************************************************************************/
    Rpm1000RetUShort * rpm1000cfggetportcustomlabelnumkeys_2_svc(Rpm1000Port * pRpmPortId, struct svc_req *s)
    {
        static Rpm1000RetUShort rv = {0, 0};
        return (&rv);
    }

    /*****************************************************************************/
    /*                                                                           */
    /*****************************************************************************/
    Rpm1000RetString * rpm1000cfggetportcustomlabelkey_2_svc(Rpm1000PortCustomLabelKeyIndex * pRpmPortCustomLabelKeyIndex, struct svc_req *s)
    {
        static Rpm1000RetString uRet = {0, 0};
        char* c = "";
        uRet.str = c;
        return (&uRet);
    }

