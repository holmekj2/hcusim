require 'Rpc/rpc_utils'
#require 'Rpc/rpc_server'
#require 'Rpc/HcuRpc/Rpc'
require 'apputils/hcu_file_utils'
require 'apputils/hcu_network_utils'
require 'Rpc/Intercepter/rpc_override'
require 'log'

class HcuRpcHandler
  include RpcUtils
  HCU_FIRMWARE_DIRECTORY = '/DOS1/Firmware/HCU/'
  #Pass in an instance of a Hcu class that handles the RPC requests.
  def initialize(hcu, event_sender)
    @hcu = hcu
    @event_sender = event_sender
    #Create a chain of RPC handlers.
    #rpc_handlers = [secondary_rpc_handlers, self]
    #Flatten the array in case the input (secondary_rpc_handlers) was an array 
    #rpc_handlers.flatten!
    #@rpc_server = RpcServer.new(HcuRpc, rpc_handlers)
    #@hcu_rpc = HcuRpc.new(self)
  end
  def hcugetmodelno(args)
    chassis_model = @hcu.chassis_model
    chassis_model = validate_string(chassis_model)
    ret = {:ulReturn => 0, :str => chassis_model}
  end
  def hcugetsbcmodelno(args)
    sbc_model = @hcu.sbc_model
    sbc_model = validate_string(sbc_model)
    ret = {:ulReturn => 0, :str => sbc_model}
  end
  def hcugetserialno(args)
    serial_number = @hcu.serial_number
    serial_number = validate_string(serial_number)
    ret = {:ulReturn => 0, :str => serial_number}
  end
  def hcuputserialno(args)
    @hcu.serial_number = args[:str]
    @hcu.save
    ret = {:ulReturn => 0}
  end
  def hcugetsbcflashsize(args)
    flash_size = @hcu.flash_size
    flash_size = validate_float(flash_size)    
    ret = {:ulReturn => 0, :str => flash_size.to_i.to_s + " Meg"}
  end
  def hcugetsbcdramsize(args)
    ram_size = @hcu.ram_size
    ram_size = validate_float(ram_size)    
    ret = {:ulReturn => 0, :str => ram_size.to_i.to_s + " Meg"}
  end
  def hcugetsbcfirmwarerev(args)
    sbc_fw_version = @hcu.sbc_fw_version
    sbc_fw_version = validate_string(sbc_fw_version)   
    ret = {:ulReturn => 0, :str => sbc_fw_version}
  end
  def hcugetswrev(args)
    fw_version = @hcu.fw_version
    fw_version = validate_string(fw_version)    
    ret = {:ulReturn => 0, :str => fw_version}
  end
  def hcugetharddiskinfo(args)    
    hard_drive_capacity = @hcu.hard_drive_capacity
    ret = {:ulReturn => 0, :capacity => hard_drive_capacity, :usedSpace => hard_drive_capacity/2, :freeSpace => hard_drive_capacity/2}
  end
  def hcugetlabel(args)
    label = @hcu.label
    label = validate_string(label)
    ret = {:ulReturn => 0, :str => label}
  end
  def hcuputlabel(args)
    @hcu.label = args[:str]
    @hcu.save
    ret = {:ulReturn => 0}
  end
  def hcugetlocation(args)
    location = @hcu.location
    location = validate_string(location)
    ret = {:ulReturn => 0, :str => location}
  end
  def hcuputlocation(args)
    @hcu.location = args[:location]
    @hcu.save
    ret = {:ulReturn => 0}
  end
  def hcugetnotes(args)
    notes = @hcu.notes
    notes = validate_string(notes)
    ret = {:ulReturn => 0, :str => notes}
  end
  def hcuputnotes(args)
    @hcu.notes = args[:str]
    @hcu.save
    ret = {:ulReturn => 0}
  end
  def hcugetuid(args)
    uid = @hcu.uid
    uid = validate_int(uid)
    ret = {:ulReturn => 0, :ulNum => uid}
  end
  def hcuputuid(args)
    @hcu.uid = args[:uid]
    @hcu.save
    ret = {:ulReturn => 0}
  end
  def hcugetnumslots(args)
    model = @hcu.chassis_model
    if model == "HCU400"
      slots = 5
    else
      slots = 16
    end
    ret = {:ulReturn => 0, :usNum => slots}
  end  
  def hcuputslotinfo(args)
    ret = {}
    begin
      Log::GeneralLogger.error "hcuputslotinfo #{args[:slot]} called"
      rpm = @hcu.rpm_delegator.get_rpm(args[:slot])
      rpm.assigned_device_id = args[:assigned_id]
      rpm.save
      ret = {:ulReturn => 0}     
    rescue
      Log::GeneralLogger.error "hcuputslotinfo for RPM#{args[:slot]} failed"    
      ret = {:ulReturn => 0x80041001} 
    end
    return ret
  end

  def hcugetslotinfo(args)
    ret = {}
    if args[:slot] == 0
      #HCU cfg
      ret = get_hcu_slot_cfg
    else
      #HCU cfg
      ret = get_rpm_slot_cfg(args[:slot])
    end
    return ret
  end
  
  def hcugetscnetworkaddr(args)
    server_ip = @hcu.server_ip
    server_ip = validate_string(server_ip)
    ret = {:ulReturn => 0, :str => server_ip}
  end
  def hcugetscgatewayaddr(args)
    server_ip = @hcu.server_ip
    server_ip = validate_string(server_ip)
    ret = {:ulReturn => 0, :str => server_ip}
  end
  
  def hcuputscnetworkaddr(args)
    @hcu.server_ip = args[:addr]
    #Also set the HCU IP address in the database here to the current IP address of this machine
    #@hcu.hcu_ip = HcuNetworkUtils.local_ip
    @hcu.save
    ret = {:ulReturn => 0}
  end 
  def hcusvcfindswfilefirst(args)    
    #Update the directory list    
    @fw_file_list = Dir.entries(HCU_FIRMWARE_DIRECTORY)
    file = HcuFileUtils.get_next_file(HCU_FIRMWARE_DIRECTORY, @fw_file_list)
    if file != nil
      ret = {:ulReturn => 0, :filename => file, :usMajorRev => 0,
        :usMinorRev => 0, :ulTime => 0, :ulSize => 0}
    else
      ret = {:ulReturn => 0x8004100e, :filename => "", :usMajorRev => 0,
        :usMinorRev => 0, :ulTime => 0, :ulSize => 0}
    end
    return ret    
  end
  def hcusvcfindswfilenext(args)
    #Update the directory list    
    file = HcuFileUtils.get_next_file(HCU_FIRMWARE_DIRECTORY, @fw_file_list)
    if file != nil
      ret = {:ulReturn => 0, :filename => file, :usMajorRev => 0,
        :usMinorRev => 0, :ulTime => 0, :ulSize => 0}
    else
      ret = {:ulReturn => 0x8004100e, :filename => "", :usMajorRev => 0,
        :usMinorRev => 0, :ulTime => 0, :ulSize => 0}
    end
    return ret    
  end
  def hcusvcinstallsw(args)
    #if @hcu.install_software
    #  ret = {:ulReturn => 0}
    #else
    #  ret = {:ulReturn => 0x80041001}
    #end
	ret = {:ulReturn => 0x8004200E}
    return ret
  end
  
  def hcuevtconnect(args)
    ret = {}
    begin
      @event_sender.connect(args[:ipAddr], args[:port])
      ret = {:ulReturn => 0}     
    rescue
      Log::GeneralLogger.error "hcuevtconnect failed"    
      ret = {:ulReturn => 0x80041002} 
    end
    return ret
  end
  def hcuevtdisconnect(args)
      @event_sender.disconnect
      ret = {:ulReturn => 0}     
  end
  def hcuevtenableeventtransfer(args)     
      @event_sender.enable_transfer
      ret = {:ulReturn => 0}     
  end
  def hcuevtisconnected(args)     
      ret = {:ulReturn => @event_sender.connected? ? 1 : 0}     
  end
  
  def hcuevtdisableeventtransfer(args)
      @event_sender.disable_transfer
      ret = {:ulReturn => 0}     
  end
  def hcugetwatchdoginterval(args)
    watchdog_interval = @hcu.watchdog_interval
    watchdog_interval = validate_int(watchdog_interval)
    ret = {:ulReturn => 0, :ulNum => watchdog_interval}
  end
  def hcuputwatchdoginterval(args)
    @hcu.update_watchdog_interval(args[:interval])
    #@hcu.watchdog_interval = args[:interval]
    #@hcu.save
    ret = {:ulReturn => 0}
  end
  
  
  #Called if the RPC handler does not implement a method
  def method_missing(method, args=nil)
    return nil
    #Log::GeneralLogger.fatal "HCU RPC does not handle: #{method}"
  end
  
private
  def get_hcu_slot_cfg
    #Assign the enumeracted types for SBC. For the SBC the assigned type always equals detected
    #(i.e. the server does not set the hcu assigned type)
    if @hcu.sbc_model == "MVME2700"
      detected_type = 1
    elsif @hcu.sbc_model == "MVME5500"      
      detected_type = 7    
    else
      detected_type = 0
    end    
    ret = {:ulReturn => 0, :slotNo => 0, :usDefaultVMECfg => 0, 
      :usVMEAddrMode => 0, :ulVMEAddr => 0, 
      :usVMEIRQ => 0, :usAutoDetect => 0, 
      :usStatus => 0, :usState => 1, :usDevId => 0, 
      :usDevTypeDetected => detected_type, 
      :usDevTypeAssigned => detected_type}
  end  

  def get_rpm_slot_cfg(slot)
    ret = {}
    begin
      rpm = @hcu.rpm_delegator.get_rpm(slot)      
      #Assign the enumeracted types for SBC. For the SBC the assigned type always equals detected
      #(i.e. the server does not set the hcu assigned type)
      if rpm.model == "RPM1000"
        detected_type = 2
      elsif rpm.model == "RPM2000"
        detected_type = 6
      elsif rpm.model == "RPM3000"
        detected_type = 8
      else
        detected_type = 0
      end    
      
      ret = {:ulReturn => 0, :slotNo => slot, :usDefaultVMECfg => 1, 
        :usVMEAddrMode => 0, :ulVMEAddr => 0, 
        :usVMEIRQ => 0, :usAutoDetect => 1, 
        :usStatus => 0, 
        :usState => (rpm.device_enabled == true) ? 1 : 0, 
        :usDevId => slot, 
        :usDevTypeDetected => detected_type, 
        :usDevTypeAssigned => rpm.assigned_device_id}
    #If the RPM is not detected, then just return an empty slot
    rescue
      ret = {:ulReturn => 0, :slotNo => slot, :usDefaultVMECfg => 0, 
        :usVMEAddrMode => 0, :ulVMEAddr => 0, 
        :usVMEIRQ => 0, :usAutoDetect => 0, 
        :usStatus => 0, :usState => 0, :usDevId => 0, 
        :usDevTypeDetected => 0, 
        :usDevTypeAssigned => 0}
    end
    return ret
  end  
end






