#include <rpc/rpc.h>
#include "hcurpc.h"
#include <time.h>
#include <stdlib.h>
#include <iostream>
using namespace std;

//hol48987@hol48987-desktop:~/workspace/ruby/HcuSim/HcuRpc/HcuRpcClientTest$ gcc hcu_rpc_client_test.c hcurpc_clnt.c hcurpc_xdr.c 
//arguement 1 is server IP
unsigned int getRandomInt(unsigned int range);
void GetRandomString(char* st, unsigned int length);

int main(int argc, char **argv)
{
    CLIENT *hcuClient;
    static long index = 0;
    unsigned long* ret;
    char protocol[] = "tcp"; 
    static struct hcuRetULong_s *uid = NULL;      
      
    
    if (argc < 2)
    {
       printf("Usage: %s serverIp\n", argv[0]);
       return -1;
    }



    uid = hcugetuid_1(0, hcuClient);
    if (uid == NULL)
    {
        printf("RPC call failed\n");
    }
    else
    {
        printf("ret = %d, uid = %d\n", uid->ulReturn, uid->ulNum);
    }
    unsigned long newuid = 11;
    ret = hcuputuid_1(&newuid, hcuClient);
    if (ret == NULL)
    {
        printf("RPC call failed\n");
    }
    else
    {
        printf("put uid ret = %d\n", *ret);
    }

    uid = hcugetuid_1(0, hcuClient);
    if (uid == NULL)
    {
        printf("RPC call failed\n");
    }
    else
    {
        printf("ret = %d, uid = %d\n", uid->ulReturn, uid->ulNum);
    }

    
}
void HcuRpcTest::setUp()
{
    m_hcuClient = clnt_create(m_serverIp, HCUCFGSVR, HCUCFGSVRVERS, "tcp");
    CPPUNIT_ASSERT(m_hcuClient != NULL);
}

void HcuRpcTest::tearDown()
{
    clnt_destroy(m_hcuClient);  
}

void HcuRpcTest::testModelNumber()
{
    struct hcuRetString40_s *model = 0;
    model = hcugetmodelno_1(0, m_hcuClient);
    CPPUNIT_ASSERT(model != 0);    
    CPPUNIT_ASSERT(model->ulReturn == 0);
    CPPUNIT_ASSERT(model->str == "1500");    
}

void HcuRpcTest::testSerialNumber()
{
    struct hcuRetString15_s *serial = 0;    
    unsigned int random = getRandomInt(10000);
    char serialno[10];
    sprintf(serialno, "%d", random);
    cout << "serial no:" << serialno << endl;
    char* pserialno = serialno;
    ret = hcuputserialno_1(&pserialno, hcuClient);
    CPPUNIT_ASSERT(ret != 0);    
    CPPUNIT_ASSERT(*ret == 0);

    serial = hcugetserialno_1(0, hcuClient);
    CPPUNIT_ASSERT(serial != 0);    
    CPPUNIT_ASSERT(serial->ulReturn == 0);
    CPPUNIT_ASSERT(serial->str == serialNo);    
}

void HcuRpcTest::testUid()
{
    unsigned long newuid = getRandomInt(10000);
    ret = hcuputuid_1(&newuid, hcuClient);
    CPPUNIT_ASSERT(ret != 0);    
    CPPUNIT_ASSERT(*ret == 0);

    struct hcuRetULong_s *uid = 0;      
    uid = hcugetuid_1(0, hcuClient);
    CPPUNIT_ASSERT(uid != 0);    
    CPPUNIT_ASSERT(uid->ulReturn == 0);
    CPPUNIT_ASSERT(uid->ulNum == newuid);    
}

unsigned int getRandomInt(unsigned int range)
{
    srand(time(NULL));
    return rand() % range;
}


void GetRandomString(char* st, unsigned int length)
{
    st[length - 1] = 0;
    for (unsigned int i = 0; i < length - 1; ++i)
    {
        srand(time(NULL));
        st[i] = rand() % 44 + 48;
    }
    cout << "Random string" << st << endl;
}
