#include "hcu_rpc_client_connect.h"

CLIENT* connect_to_server(char* ip)
{
    CLIENT *rpcClient;  
    //Create the RPC connection
    if ((rpcClient = clnt_create(ip, HCUCFGSVR, HCUCFGSVRVERS, "tcp")) == NULL) 
    {
        printf ("clnt_create failed %s\n", ip); 
    }
    return rpcClient;
}
//This is a complete hack. I don't know how to get Ruby to translate to a char** pointer like some RPC calls want. Also I don't know how to create 
//a char** type in ruby. I think you have to use typemap
//This is not reentrant so use at your own risk.
char** convert_rstring_ppchar(char* rstring)
{
    static char* p = 0;
    p = rstring; 
    return &p;
}

