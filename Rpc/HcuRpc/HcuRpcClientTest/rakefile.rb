require 'rake/clean'

CLEAN.include('*.o')
CLOBBER.include('HcuRpcTest')

task :default => ["HcuRpcTest"]

SRC = FileList['*.c'] + FileList['*.cpp']
OBJ = SRC.ext('o')
 
rule '.o' => '.c' do |t|
  sh "g++ -c -o #{t.name} #{t.source}"
end

rule '.o' => '.cpp' do |t|
  sh "g++ -c -o #{t.name} #{t.source}"
end
 
file "HcuRpcTest" => OBJ do
  sh "g++ -lcppunit -o HcuRpcTest #{OBJ}"
end

file 'HcuRpcTest.o' => ['HcuRpcTest.h', 'HcuRpcTest.cpp']
