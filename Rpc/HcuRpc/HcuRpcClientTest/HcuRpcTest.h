#ifndef HCURPCTEST
#define HCURPCTEST

#include <rpc/rpc.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <string>

class HcuRpcTest : public CppUnit::TestFixture
{
public:
    HcuRpcTest();
    virtual ~HcuRpcTest();    
    
    //Initializations before tests are run
    void setUp();
    //Tear down after all tests are done
    void tearDown();
    
    //My tests
    void hcugetmodelno();
    void hcugetsbcmodelno();    
    void hcuputgetserialno();
    void hcugetsbcflashsize();
    void hcugetsbcdramsize();
    void hcugetsbcfirmwarerev();
    void hcugetswrev();       
    void hcugetharddiskinfo();     
    void hcuputgetlabel();
    void hcuputgetlocation();
    void hcuputgetnotes();        
    void hcuputgetuid();       
    void hcugetnumslots(); 
    void hcuputgetslotinfo();        
    void hcuputgetscnetworkaddr();        
    void hcusvcfindswfile();   
    void hcuputgetwatchdoginterval();   
    void hcu_test_override1();                 
    void hcu_test_override2();                     
    
    CPPUNIT_TEST_SUITE(HcuRpcTest);
    //CPPUNIT_TEST(hcugetmodelno);
    //CPPUNIT_TEST(hcugetsbcmodelno);            
    //CPPUNIT_TEST(hcuputgetserialno);    
    //CPPUNIT_TEST(hcugetsbcflashsize);    
    //CPPUNIT_TEST(hcugetsbcdramsize);        
    //CPPUNIT_TEST(hcugetsbcfirmwarerev);            
    //CPPUNIT_TEST(hcugetswrev);                
    //CPPUNIT_TEST(hcugetharddiskinfo);                    
    //CPPUNIT_TEST(hcuputgetlabel);                        
    //CPPUNIT_TEST(hcuputgetlocation);                        
    //CPPUNIT_TEST(hcuputgetnotes);                                
    //CPPUNIT_TEST(hcuputgetuid);          
    //CPPUNIT_TEST(hcugetnumslots);              
    //CPPUNIT_TEST(hcuputgetslotinfo);                  
    //CPPUNIT_TEST(hcuputgetscnetworkaddr);                  
    //CPPUNIT_TEST(hcusvcfindswfile);                      
    //CPPUNIT_TEST(hcuputgetwatchdoginterval);                      
    CPPUNIT_TEST(hcu_test_override2);
    CPPUNIT_TEST_SUITE_END();  

private:
    std::string m_serverIp;
    CLIENT* m_hcuClient;
};

#endif //HCURPCTEST
