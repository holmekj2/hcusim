#include "HcuRpcTest.h"
#include "hcurpc.h"
#include <sys/time.h>
#include <stdlib.h>
#include <iostream>
using namespace std;

CPPUNIT_TEST_SUITE_REGISTRATION(HcuRpcTest);

//g++ HcuRpcTest.cpp hcurpc_clnt.c hcurpc_xdr.c -lcppunit

unsigned int getRandomInt(unsigned int range);
void GetRandomString(char* st, unsigned int length);

int main() {
  CppUnit::TextUi::TestRunner runner;
  runner.addTest( CppUnit::TestFactoryRegistry::getRegistry().makeTest() );
  bool success = runner.run("", false);

  return success;
}

HcuRpcTest::HcuRpcTest() : m_serverIp("localhost")
{
}

HcuRpcTest::~HcuRpcTest()
{
}

void HcuRpcTest::setUp()
{
    m_hcuClient = clnt_create(m_serverIp.c_str(), HCUCFGSVR, HCUCFGSVRVERS, "tcp");
    CPPUNIT_ASSERT(m_hcuClient != NULL);
}

void HcuRpcTest::tearDown()
{
    clnt_destroy(m_hcuClient);  
}

void HcuRpcTest::hcu_test_override1()
{
    struct hcuRetString40_s *model = 0;
    //Setup the sim to override get model number once
    model = hcugetmodelno_1(0, m_hcuClient);
    CPPUNIT_ASSERT(model == 0);    
    model = hcugetmodelno_1(0, m_hcuClient);
    CPPUNIT_ASSERT(model != 0);    
}

void HcuRpcTest::hcu_test_override2()
{
    struct hcuRetSlotInfo_s* gSlotInfo = 0;
    //Get HCU stuff 
    unsigned short slot = 0;
    gSlotInfo = hcugetslotinfo_1(&slot, m_hcuClient);
    CPPUNIT_ASSERT(gSlotInfo != 0);    
    gSlotInfo = hcugetslotinfo_1(&slot, m_hcuClient);
    CPPUNIT_ASSERT(gSlotInfo != 0);    
}


void HcuRpcTest::hcugetmodelno()
{
    struct hcuRetString40_s *model = 0;
    model = hcugetmodelno_1(0, m_hcuClient);
    CPPUNIT_ASSERT(model != 0);    
    CPPUNIT_ASSERT(model->ulReturn == 0);
    CPPUNIT_ASSERT(strcmp(model->str, "HCU1500") == 0);    
}

void HcuRpcTest::hcugetsbcmodelno()
{
    struct hcuRetString40_s *model = 0;
    model = hcugetsbcmodelno_1(0, m_hcuClient);
    CPPUNIT_ASSERT(model != 0);    
    CPPUNIT_ASSERT(model->ulReturn == 0);
    CPPUNIT_ASSERT(strcmp(model->str, "MVME2700") == 0);    
}

void HcuRpcTest::hcuputgetserialno()
{
    struct hcuRetString15_s *serial = 0;    
    unsigned int random = getRandomInt(10000);
    char serialno[10];
    sprintf(serialno, "%d", random);
    char* pserialno = serialno;
    unsigned long* ret;
    ret = hcuputserialno_1(&pserialno, m_hcuClient);
    CPPUNIT_ASSERT(ret != 0);    
    CPPUNIT_ASSERT(*ret == 0);

    serial = hcugetserialno_1(0, m_hcuClient);
    CPPUNIT_ASSERT(serial != 0);    
    CPPUNIT_ASSERT(serial->ulReturn == 0);
    CPPUNIT_ASSERT(strcmp(serial->str, serialno) == 0);    
}

void HcuRpcTest::hcugetsbcflashsize()
{
    struct hcuRetString10_s *flashSize = 0;
    flashSize = hcugetsbcflashsize_1(0, m_hcuClient);
    CPPUNIT_ASSERT(flashSize != 0);    
    CPPUNIT_ASSERT(flashSize->ulReturn == 0);
    CPPUNIT_ASSERT(strcmp(flashSize->str, "10MB") == 0);    
}

void HcuRpcTest::hcugetsbcdramsize()
{
    struct hcuRetString10_s *ramSize = 0;
    ramSize = hcugetsbcdramsize_1(0, m_hcuClient);
    CPPUNIT_ASSERT(ramSize != 0);    
    CPPUNIT_ASSERT(ramSize->ulReturn == 0);
    CPPUNIT_ASSERT(strcmp(ramSize->str, "128MB") == 0);    
}

void HcuRpcTest::hcugetsbcfirmwarerev()
{
    struct hcuRetString10_s *sbcfwVersion = 0;
    sbcfwVersion = hcugetsbcfirmwarerev_1(0, m_hcuClient);
    CPPUNIT_ASSERT(sbcfwVersion != 0);    
    CPPUNIT_ASSERT(sbcfwVersion->ulReturn == 0);
    CPPUNIT_ASSERT(strcmp(sbcfwVersion->str, "1.00") == 0);    
}

void HcuRpcTest::hcugetswrev()
{
    struct hcuRetString10_s *fwVersion = 0;
    fwVersion = hcugetswrev_1(0, m_hcuClient);
    CPPUNIT_ASSERT(fwVersion != 0);    
    CPPUNIT_ASSERT(fwVersion->ulReturn == 0);
    CPPUNIT_ASSERT(strcmp(fwVersion->str, "2.40") == 0);    
}

void HcuRpcTest::hcugetharddiskinfo()
{
    struct hcuRetDiskInfo_s *info = 0;
    unsigned short num = 1;
    info = hcugetharddiskinfo_1(&num, m_hcuClient);
    CPPUNIT_ASSERT(info != 0);    
    CPPUNIT_ASSERT(info->ulReturn == 0);
    CPPUNIT_ASSERT(info->capacity == 100000);
    CPPUNIT_ASSERT(info->usedSpace == 50000);
    CPPUNIT_ASSERT(info->freeSpace == 50000);            
}

void HcuRpcTest::hcuputgetlabel()
{
    struct hcuRetString40_s *label = 0;    
    char llabel[40]; 
    GetRandomString(llabel, 10);
    char* plabel = llabel;
    unsigned long* ret;
    ret = hcuputlabel_1(&plabel, m_hcuClient);
    CPPUNIT_ASSERT(ret != 0);    
    CPPUNIT_ASSERT(*ret == 0);

    label = hcugetlabel_1(0, m_hcuClient);
    CPPUNIT_ASSERT(label != 0);    
    CPPUNIT_ASSERT(label->ulReturn == 0);
    CPPUNIT_ASSERT(strcmp(label->str, llabel) == 0);    
}
void HcuRpcTest::hcuputgetlocation()
{
    struct hcuRetString255_s *location = 0;    
    char llocation[255]; 
    GetRandomString(llocation, 10);
    char* plocation = llocation;
    unsigned long* ret;
    ret = hcuputlocation_1(&plocation, m_hcuClient);
    CPPUNIT_ASSERT(ret != 0);    
    CPPUNIT_ASSERT(*ret == 0);

    location = hcugetlocation_1(0, m_hcuClient);
    CPPUNIT_ASSERT(location != 0);    
    CPPUNIT_ASSERT(location->ulReturn == 0);
    CPPUNIT_ASSERT(strcmp(location->str, llocation) == 0);    
}
void HcuRpcTest::hcuputgetnotes()
{
    struct hcuRetString1024_s *notes = 0;    
    char lnotes[1024]; 
    GetRandomString(lnotes, 10);
    char* pnotes = lnotes;
    unsigned long* ret;
    ret = hcuputnotes_1(&pnotes, m_hcuClient);
    CPPUNIT_ASSERT(ret != 0);    
    CPPUNIT_ASSERT(*ret == 0);

    notes = hcugetnotes_1(0, m_hcuClient);
    CPPUNIT_ASSERT(notes != 0);    
    CPPUNIT_ASSERT(notes->ulReturn == 0);
    CPPUNIT_ASSERT(strcmp(notes->str, lnotes) == 0);    
}

void HcuRpcTest::hcuputgetuid()
{
    unsigned long newuid = getRandomInt(10000);
    unsigned long* ret;    
    ret = hcuputuid_1(&newuid, m_hcuClient);
    CPPUNIT_ASSERT(ret != 0);    
    CPPUNIT_ASSERT(*ret == 0);

    struct hcuRetULong_s *uid = 0;      
    uid = hcugetuid_1(0, m_hcuClient);
    CPPUNIT_ASSERT(uid != 0);    
    CPPUNIT_ASSERT(uid->ulReturn == 0);
    CPPUNIT_ASSERT(uid->ulNum == newuid);    
}

void HcuRpcTest::hcugetnumslots()
{
    struct hcuRetUShort_s *slots = 0;      
    slots = hcugetnumslots_1(0, m_hcuClient);
    CPPUNIT_ASSERT(slots != 0);    
    CPPUNIT_ASSERT(slots->ulReturn == 0);
    CPPUNIT_ASSERT(slots->usNum == 16);    
}

void HcuRpcTest::hcuputgetslotinfo()
{
    struct hcuRetSlotInfo_s* gSlotInfo = 0;
    //Get HCU stuff 
    unsigned short slot = 0;
    gSlotInfo = hcugetslotinfo_1(&slot, m_hcuClient);
    CPPUNIT_ASSERT(gSlotInfo != 0);    
        
    CPPUNIT_ASSERT(gSlotInfo->ulReturn == 0);    
    CPPUNIT_ASSERT(gSlotInfo->slotNo == 0);
    CPPUNIT_ASSERT(gSlotInfo->usState == 1);    
    CPPUNIT_ASSERT(gSlotInfo->usStatus == 0);        
    CPPUNIT_ASSERT(gSlotInfo->usDevTypeDetected == 1);    
    CPPUNIT_ASSERT(gSlotInfo->usDevTypeAssigned == 1);        
    CPPUNIT_ASSERT(gSlotInfo->usDevId == 0);    
    
    //Get RPM stuff 
    slot = 1;
    gSlotInfo = hcugetslotinfo_1(&slot, m_hcuClient);
    CPPUNIT_ASSERT(gSlotInfo != 0);    
        
    CPPUNIT_ASSERT(gSlotInfo->ulReturn == 0);    
    CPPUNIT_ASSERT(gSlotInfo->slotNo == 1);
    CPPUNIT_ASSERT(gSlotInfo->usStatus == 0);        
    CPPUNIT_ASSERT(gSlotInfo->usDevTypeDetected == 2);    
    CPPUNIT_ASSERT(gSlotInfo->usDevId == 1);   

    //Set the device assignment    
    struct hcuSlotInfo_s slotInfo;
    slotInfo.slotNo = slot;
    slotInfo.usDevTypeAssigned = gSlotInfo->usDevTypeDetected;  
    unsigned long* ret;    
    ret = hcuputslotinfo_1(&slotInfo, m_hcuClient);
    CPPUNIT_ASSERT(ret != 0);    
    CPPUNIT_ASSERT(*ret == 0);

    //Verify the assignment
    gSlotInfo = hcugetslotinfo_1(&slot, m_hcuClient);
    CPPUNIT_ASSERT(gSlotInfo != 0);    
    CPPUNIT_ASSERT(gSlotInfo->ulReturn == 0);    
    CPPUNIT_ASSERT(gSlotInfo->slotNo == 1);
    CPPUNIT_ASSERT(gSlotInfo->usStatus == 0);        
    CPPUNIT_ASSERT(gSlotInfo->usDevTypeDetected == 2);    
    CPPUNIT_ASSERT(gSlotInfo->usDevTypeAssigned == 2);        
    CPPUNIT_ASSERT(gSlotInfo->usDevId == 1);   
    
    //Verify 3000
    slot = 2;
    gSlotInfo = hcugetslotinfo_1(&slot, m_hcuClient);
    CPPUNIT_ASSERT(gSlotInfo != 0);    
    CPPUNIT_ASSERT(gSlotInfo->ulReturn == 0);    
    CPPUNIT_ASSERT(gSlotInfo->usDevTypeDetected == 8);    

    //Verify 2000
    slot = 3;
    gSlotInfo = hcugetslotinfo_1(&slot, m_hcuClient);
    CPPUNIT_ASSERT(gSlotInfo != 0);    
    CPPUNIT_ASSERT(gSlotInfo->ulReturn == 0);    
    CPPUNIT_ASSERT(gSlotInfo->usDevTypeDetected == 6);    
    
    //Verify empty slot
    slot = 15;
    gSlotInfo = hcugetslotinfo_1(&slot, m_hcuClient);
    CPPUNIT_ASSERT(gSlotInfo != 0);    
    CPPUNIT_ASSERT(gSlotInfo->ulReturn == 0);    
    CPPUNIT_ASSERT(gSlotInfo->usDevTypeDetected == 0);        
}


void HcuRpcTest::hcuputgetscnetworkaddr()
{
    struct hcuRetString20_s *ip = 0;    
    char lip[20]; 
    GetRandomString(lip, 10);
    char* pip = lip;
    unsigned long* ret;
    ret = hcuputscnetworkaddr_1(&pip, m_hcuClient);
    CPPUNIT_ASSERT(ret != 0);    
    CPPUNIT_ASSERT(*ret == 0);

    ip = hcugetscnetworkaddr_1(0, m_hcuClient);
    CPPUNIT_ASSERT(ip != 0);    
    CPPUNIT_ASSERT(ip->ulReturn == 0);
    CPPUNIT_ASSERT(strcmp(ip->str, lip) == 0);    
}

void HcuRpcTest::hcusvcfindswfile()
{
    //Need to fix this. This uses a different RPC interface than the configuration. 
    clnt_destroy(m_hcuClient); 
    m_hcuClient = clnt_create(m_serverIp.c_str(), HCUSERVICESVR, HCUSERVICESVRVERS, "tcp");
    unsigned long* ret;
    ret = hcusvcconnect_1(0, m_hcuClient);
    CPPUNIT_ASSERT(ret != 0);        
  
    struct hcuRetFileInfo_s *file = 0;    

    file = hcusvcfindswfilefirst_1(0, m_hcuClient);
    CPPUNIT_ASSERT(file != 0);    
    CPPUNIT_ASSERT(file->ulReturn == 0);
    CPPUNIT_ASSERT(strcmp(file->filename, "HCUApp_New.out") == 0);    

    file = hcusvcfindswfilenext_1(0, m_hcuClient);
    CPPUNIT_ASSERT(file != 0);    
    CPPUNIT_ASSERT(file->ulReturn == 0);
    CPPUNIT_ASSERT(strcmp(file->filename, "SBC2700_2.41.st") == 0);    
    
    file = hcusvcfindswfilenext_1(0, m_hcuClient);
    CPPUNIT_ASSERT(file != 0);    
    CPPUNIT_ASSERT(file->ulReturn == 0x8004100e);
    CPPUNIT_ASSERT(strcmp(file->filename, "") == 0);    

    file = hcusvcfindswfilefirst_1(0, m_hcuClient);
    CPPUNIT_ASSERT(file != 0);    
    CPPUNIT_ASSERT(file->ulReturn == 0);
    CPPUNIT_ASSERT(strcmp(file->filename, "HCUApp_New.out") == 0);    

    file = hcusvcfindswfilenext_1(0, m_hcuClient);
    CPPUNIT_ASSERT(file != 0);    
    CPPUNIT_ASSERT(file->ulReturn == 0);
    CPPUNIT_ASSERT(strcmp(file->filename, "SBC2700_2.41.st") == 0);    
    
    file = hcusvcfindswfilenext_1(0, m_hcuClient);
    CPPUNIT_ASSERT(file != 0);    
    CPPUNIT_ASSERT(file->ulReturn == 0x8004100e);
    CPPUNIT_ASSERT(strcmp(file->filename, "") == 0);    
}

void HcuRpcTest::hcuputgetwatchdoginterval()
{
    unsigned long newWatchdogInterval = 300;
    unsigned long* ret;    
    struct hcuRetULong_s *watchdogInterval = 0;      
    
    watchdogInterval = hcugetwatchdoginterval_1(0, m_hcuClient);
    CPPUNIT_ASSERT(watchdogInterval != 0);    
    CPPUNIT_ASSERT(watchdogInterval->ulReturn == 0);
    CPPUNIT_ASSERT(watchdogInterval->ulNum == 0);    
    
    ret = hcuputwatchdoginterval_1(&newWatchdogInterval, m_hcuClient);
    CPPUNIT_ASSERT(ret != 0);    
    CPPUNIT_ASSERT(*ret == 0);

    watchdogInterval = hcugetwatchdoginterval_1(0, m_hcuClient);
    CPPUNIT_ASSERT(watchdogInterval != 0);    
    CPPUNIT_ASSERT(watchdogInterval->ulReturn == 0);
    CPPUNIT_ASSERT(watchdogInterval->ulNum == 300);    
}


unsigned int getRandomInt(unsigned int range)
{
    //int seed = time(NULL);
    struct timeval t;
    gettimeofday(&t, 0);
    int seed = t.tv_usec;
    srand(seed);
    return rand() % range;
}


void GetRandomString(char* st, unsigned int length)
{
    st[length - 1] = 0;
    int rand;
    for (unsigned int i = 0; i < length - 1; ++i)
    {
        
        //srand(time(NULL));
        rand = getRandomInt(26);
        //st[i] = rand() % 44 + 48;
        //Get the random upto alpha chars
        st[i] = rand + 65;
    }
}
