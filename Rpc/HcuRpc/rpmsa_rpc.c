#include <rpc/rpc.h>
#include "rpm1000sarpc.h"
#include "ruby.h"
#include "../cruby_utils.h"
#include <stdio.h>

//This is the RPC server main calls
extern int rpmsaRpcSvrMain ();

//Class singleton (i.e. a ruby class object)
VALUE rpmsaRpc;
//Object singleton. Using this implies that this class is a singleton class since all objects will use this global.
//This is necessary if something other than Ruby is calling c methods (e.g. RPC server). 
VALUE rpcInstance;

VALUE initRpmsa(VALUE self, VALUE robject)
{
  //Assign the Ruby object the implementing Ruby object
  rpcInstance = robject;
  rb_iv_set(self, "@robject", robject);
  return self;
}

void startRpmsaRpcService(unsigned long registerProtocol)
{
  //Start the RPC service
  rpmsaRpcSvrMain(registerProtocol);
}

//void Init_RpmsaRpc() {
  //Create mapping from ruby to c. Ruby object will be named XdrCommon
//  rpmsaRpc = rb_define_class("RpmsaRpc", rb_cObject);

// rb_define_method(rpmsaRpc, "initialize", init, 1);
//  rb_define_method(rpmsaRpc, "start_rpc_service", startRpcService, 0);  
//}    

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    u_long *rpm1000saconnect_1_svc (Rpm1000SACnctInfo * pcnctinfo, struct svc_req *s)
    {
        static u_long ulRet;
        VALUE methodin = rb_hash_new();
        printf ("rpm1000saconnect_1_svc called\n");
        SETHASHMETHOD(methodin, "rpm1000saconnect");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(pcnctinfo->usRpmId));                
        rb_hash_aset(methodin, CSTR2SYM("port"), INT2FIX(pcnctinfo->usPortId));      
        rb_hash_aset(methodin, CSTR2SYM("server_ip"), rb_str_new2(pcnctinfo->strSCIpAddr));
        rb_hash_aset(methodin, CSTR2SYM("server_port"), INT2FIX(pcnctinfo->usSocketPort));                                        
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ulRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        return (&ulRet);
        //ulRet = pRpmMgr->SAConnect (pcnctinfo->usRpmId, pcnctinfo->usPortId,
        //                              pcnctinfo->strSCIpAddr, pcnctinfo->usSocketPort);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    u_long *rpm1000sadisconnect_1_svc (Rpm1000SAGenInfo * pgeninfo, struct svc_req *s)
    {
        static u_long ulRet;
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000sadisconnect");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(pgeninfo->usRpmId));                
        rb_hash_aset(methodin, CSTR2SYM("port"), INT2FIX(pgeninfo->usPortId));      
        rb_hash_aset(methodin, CSTR2SYM("server_port"), INT2FIX(pgeninfo->usSocketPort));                                        
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ulRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        //ulRet = pRpmMgr->SADisconnect (pgeninfo->usRpmId, pgeninfo->usPortId, pgeninfo->usSocketPort);
        return (&ulRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    u_long *rpm1000saabortsngltrace_1_svc (Rpm1000SAGenInfo * pgeninfo, struct svc_req *s)
    {
        static u_long ulRet;
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000saabortsngltrace");
        rb_hash_aset(methodin, CSTR2SYM("rpm"), INT2FIX(pgeninfo->usRpmId));                
        rb_hash_aset(methodin, CSTR2SYM("port"), INT2FIX(pgeninfo->usPortId));      
        rb_hash_aset(methodin, CSTR2SYM("server_port"), INT2FIX(pgeninfo->usSocketPort));                                        
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ulRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        return (&ulRet);

        //ulRet = pRpmMgr->SAAbortSnglTrace (pgeninfo->usRpmId, pgeninfo->usPortId, pgeninfo->usSocketPort);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    u_long *rpm1000sareqsngltrace_1_svc (Rpm1000SATrcReq * ptrcreq, struct svc_req *s)
    {
        static u_long ulRet;
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000sareqsngltrace");
        rb_hash_aset(methodin, CSTR2SYM("usRpmId"), INT2FIX(ptrcreq->usRpmId));                
        rb_hash_aset(methodin, CSTR2SYM("usPortId"), INT2FIX(ptrcreq->usPortId));      
        rb_hash_aset(methodin, CSTR2SYM("usSocketPort"), INT2FIX(ptrcreq->usSocketPort));                                                
        rb_hash_aset(methodin, CSTR2SYM("ulRequestId"), INT2FIX(ptrcreq->ulRequestId));                                        
        rb_hash_aset(methodin, CSTR2SYM("ulConfigVersId"), INT2FIX(ptrcreq->ulConfigVersId));                                                
        rb_hash_aset(methodin, CSTR2SYM("nTraceMode"), INT2FIX(ptrcreq->nTraceMode));                                                
        rb_hash_aset(methodin, CSTR2SYM("dwCenterFreq"), rb_float_new(ptrcreq->dwCenterFreq));                                                                
        rb_hash_aset(methodin, CSTR2SYM("dwFreqSpan"), rb_float_new(ptrcreq->dwFreqSpan));                                                                        
        rb_hash_aset(methodin, CSTR2SYM("dwStartFreq"), rb_float_new(ptrcreq->dwStartFreq));                                                        
        rb_hash_aset(methodin, CSTR2SYM("dwStopFreq"), rb_float_new(ptrcreq->dwStopFreq));                                                                
        rb_hash_aset(methodin, CSTR2SYM("dwDesiredReflevel"), rb_float_new(ptrcreq->dwDesiredReflevel));                                                        
        rb_hash_aset(methodin, CSTR2SYM("dwActualRefLevel"), rb_float_new(ptrcreq->dwActualRefLevel));                                                                
        rb_hash_aset(methodin, CSTR2SYM("nRBW"), INT2FIX(ptrcreq->nRBW));                                                
        rb_hash_aset(methodin, CSTR2SYM("nVBW"), INT2FIX(ptrcreq->nVBW));                                                
        rb_hash_aset(methodin, CSTR2SYM("nDetMode"), INT2FIX(ptrcreq->nDetMode));                                                
        rb_hash_aset(methodin, CSTR2SYM("ulDwellTime"), INT2FIX(ptrcreq->ulDwellTime));                                                
        rb_hash_aset(methodin, CSTR2SYM("nTrigMode"), INT2FIX(ptrcreq->nTrigMode));                                                
        rb_hash_aset(methodin, CSTR2SYM("dwTrigLevel"), rb_float_new(ptrcreq->dwTrigLevel));                                                                        
        rb_hash_aset(methodin, CSTR2SYM("ulTrigDelay"), INT2FIX(ptrcreq->ulTrigDelay));                                                
        rb_hash_aset(methodin, CSTR2SYM("ulTimeSpan"), INT2FIX(ptrcreq->ulTimeSpan));                                                
        rb_hash_aset(methodin, CSTR2SYM("ulInterval"), INT2FIX(ptrcreq->ulInterval));                                                
        
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ulRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        return (&ulRet);
        
        //ulRet = pRpmMgr->SAReqSnglTrace (ptrcreq);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    u_long *rpm1000sareqmultitrace_1_svc (Rpm1000SATrcReq * ptrcreq, struct svc_req *s)
    {
        static u_long ulRet;
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000sareqmultitrace");
        rb_hash_aset(methodin, CSTR2SYM("usRpmId"), INT2FIX(ptrcreq->usRpmId));                
        rb_hash_aset(methodin, CSTR2SYM("usPortId"), INT2FIX(ptrcreq->usPortId));      
        rb_hash_aset(methodin, CSTR2SYM("usSocketPort"), INT2FIX(ptrcreq->usSocketPort));                                                
        rb_hash_aset(methodin, CSTR2SYM("ulRequestId"), INT2FIX(ptrcreq->ulRequestId));                                        
        rb_hash_aset(methodin, CSTR2SYM("ulConfigVersId"), INT2FIX(ptrcreq->ulConfigVersId));                                                
        rb_hash_aset(methodin, CSTR2SYM("nTraceMode"), INT2FIX(ptrcreq->nTraceMode));                                                
        rb_hash_aset(methodin, CSTR2SYM("dwCenterFreq"), rb_float_new(ptrcreq->dwCenterFreq));                                                                
        rb_hash_aset(methodin, CSTR2SYM("dwFreqSpan"), rb_float_new(ptrcreq->dwFreqSpan));                                                                        
        rb_hash_aset(methodin, CSTR2SYM("dwStartFreq"), rb_float_new(ptrcreq->dwStartFreq));                                                        
        rb_hash_aset(methodin, CSTR2SYM("dwStopFreq"), rb_float_new(ptrcreq->dwStopFreq));                                                                
        rb_hash_aset(methodin, CSTR2SYM("dwDesiredReflevel"), rb_float_new(ptrcreq->dwDesiredReflevel));                                                        
        rb_hash_aset(methodin, CSTR2SYM("dwActualRefLevel"), rb_float_new(ptrcreq->dwActualRefLevel));                                                                
        rb_hash_aset(methodin, CSTR2SYM("nRBW"), INT2FIX(ptrcreq->nRBW));                                                
        rb_hash_aset(methodin, CSTR2SYM("nVBW"), INT2FIX(ptrcreq->nVBW));                                                
        rb_hash_aset(methodin, CSTR2SYM("nDetMode"), INT2FIX(ptrcreq->nDetMode));                                                
        rb_hash_aset(methodin, CSTR2SYM("ulDwellTime"), INT2FIX(ptrcreq->ulDwellTime));                                                
        rb_hash_aset(methodin, CSTR2SYM("nTrigMode"), INT2FIX(ptrcreq->nTrigMode));                                                
        rb_hash_aset(methodin, CSTR2SYM("dwTrigLevel"), rb_float_new(ptrcreq->dwTrigLevel));                                                                        
        rb_hash_aset(methodin, CSTR2SYM("ulTrigDelay"), INT2FIX(ptrcreq->ulTrigDelay));                                                
        rb_hash_aset(methodin, CSTR2SYM("ulTimeSpan"), INT2FIX(ptrcreq->ulTimeSpan));                                                
        rb_hash_aset(methodin, CSTR2SYM("ulInterval"), INT2FIX(ptrcreq->ulInterval));                                                
        
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ulRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        return (&ulRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    u_long *rpm1000sahaltmultitrace_1_svc (Rpm1000SAGenInfo * pgeninfo, struct svc_req *s)
    {
        static u_long ulRet;
        VALUE methodin = rb_hash_new();
        SETHASHMETHOD(methodin, "rpm1000sahaltmultitrace");
        rb_hash_aset(methodin, CSTR2SYM("usRpmId"), INT2FIX(pgeninfo->usRpmId));                
        rb_hash_aset(methodin, CSTR2SYM("usPortId"), INT2FIX(pgeninfo->usPortId));      
        rb_hash_aset(methodin, CSTR2SYM("usSocketPort"), INT2FIX(pgeninfo->usSocketPort));                                        
        VALUE rreturn = rb_hash_new();
        
        //Call the Ruby function to direct this to the right place
        rreturn = rb_funcall(rpcInstance, rb_intern("rpc_call"), 1, methodin);                    
        
        //Translate return to C
        ulRet = NUM2UINT(rb_hash_aref(rreturn, CSTR2SYM("ulReturn")));
        return (&ulRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    u_long *rpm3000demodconnect_1_svc (Rpm1000SACnctInfo * pcnctinfo, struct svc_req *s)
    {
        static u_long ulRet = 0;
        //ulRet = pRpmMgr->DemodConnect (pcnctinfo->usRpmId, pcnctinfo->usPortId,
        //                              pcnctinfo->strSCIpAddr, pcnctinfo->usSocketPort);
        return (&ulRet);
    }
    
    struct DetectedChannels*  rpm3000demodconnect_2_svc ( Rpm1000SACnctInfo* arg, struct svc_req * s)   
    {
		static DetectedChannels detectedChannels;
		detectedChannels.channels.channels_len = 0;
		detectedChannels.channels.channels_val = 0;
		return &detectedChannels;
	} 

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    u_long *rpm3000demoddisconnect_1_svc (Rpm1000SAGenInfo * pgeninfo, struct svc_req *s)
    {
        static u_long ulRet = 0;
        //ulRet = pRpmMgr->DemodDisconnect (pgeninfo->usRpmId, pgeninfo->usPortId, pgeninfo->usSocketPort);
        return (&ulRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    u_long *rpm3000demodabortsngltrace_1_svc (Rpm1000SAGenInfo * pgeninfo, struct svc_req *s)
    {
        static u_long ulRet = 0;
        //ulRet = pRpmMgr->DemodAbortSnglTrace (pgeninfo->usRpmId, pgeninfo->usPortId, pgeninfo->usSocketPort);
        return (&ulRet);
    }

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    u_long *rpm3000demodreqsngltrace_1_svc (Rpm3000DemodReq * pdemodreq, struct svc_req *s)
    {
        static u_long ulRet = 0;
        //ulRet = pRpmMgr->DemodReqSnglTrace (pdemodreq);
        return (&ulRet);
    }

    unsigned long* startucdportscan_2_svc( struct RpmPortWithClear *arg, struct svc_req * s)
    {
        // DetectedChannels* rv = common_detectedChannels(arg->rpm, arg->port);
        static unsigned long rv = 0;
        return &rv;
    }
    
    struct DetectedChannels* getdetectedchannels_2_svc( struct RpmPort* arg, struct svc_req * s)    
    {
		static DetectedChannels detectedChannels;
		detectedChannels.channels.channels_len = 0;
		detectedChannels.channels.channels_val = 0;
		return &detectedChannels;
	}
	
    u_long * rpm3000qamfieldviewconnect_3_svc(struct Rpm1000SACnctInfo *pcnctinfo, struct svc_req *s)
    {
        static u_long ulRet;
        return (&ulRet);
    }

    u_long * rpm3000qamfieldviewreqmultitrace_3_svc(struct Rpm3000QamFieldviewReq *pdemodreq, struct svc_req *s)
    {
        static u_long ulRet;
        return (&ulRet);
    }

    u_long * rpm3000qamfieldviewdisconnect_3_svc(struct Rpm1000SAGenInfo *pgeninfo, struct svc_req *s)
    {
        static u_long ulRet;
        return (&ulRet);
    }
	
/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    u_long *rpm3000demodreqmultitrace_1_svc (Rpm3000DemodReq * pdemodreq, struct svc_req *s)
    {
        static u_long ulRet = 0;
        //ulRet = pRpmMgr->DemodReqMultiTrace (pdemodreq);
        return (&ulRet);
    }

    u_long *rpm3000demodreqmultitrace_2_svc (Rpm3000DemodReq * pdemodreq, struct svc_req *s)
    {
        static u_long ulRet = 0;
        //ulRet = pRpmMgr->DemodReqMultiTrace (pdemodreq);
        return (&ulRet);
    }

    u_long* rpm3000demodreqmultitracechannel_2_svc ( struct Rpm3000DemodReq_Channel *arg, struct svc_req * svc)
	{
        static u_long ulRet = 0;
        return (&ulRet);
	}
/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
    u_long *rpm3000demodhaltmultitrace_1_svc (Rpm1000SAGenInfo * pgeninfo, struct svc_req *s)
    {
        static u_long ulRet = 0;
        //ulRet = pRpmMgr->DemodHaltMultiTrace (pgeninfo->usRpmId, pgeninfo->usPortId, pgeninfo->usSocketPort);
        return (&ulRet);
    }
    
    struct DetectedChannelsWithPortInfoArray * getalldetectedchannels_3_svc(void *v, struct svc_req *s)    
    {
        static DetectedChannelsWithPortInfoArray rv;
        rv.channels.channels_len = 0;
        rv.channels.channels_val = 0;
        return &rv;
    }
