#include <rpc/rpc.h>
#include "ruby.h"
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <arpa/inet.h>

//Class singleton (i.e. a ruby class object)
VALUE rpc;
//Object singleton. Using this implies that this class is a singleton class since all objects will use this global.
//This is necessary if something other than Ruby is calling c methods (e.g. RPC server). 
VALUE rpcInstance;

extern VALUE initHcu(VALUE self, VALUE robject);
//extern VALUE startHcuRpcService(VALUE self);
extern void startHcuRpcService(unsigned long registerProtocol);
extern VALUE hcuRpc;

extern VALUE initRpmgen(VALUE self, VALUE robject);
//extern VALUE startRpmgenRpcService(VALUE self);
extern void startRpmgenRpcService(unsigned long registerProtocol);
extern VALUE rpmgenRpc;

extern VALUE initRpmsa(VALUE self, VALUE robject);
//extern VALUE startRpmgenRpcService(VALUE self);
extern void startRpmsaRpcService(unsigned long registerProtocol);
extern VALUE rpmsaRpc;

extern VALUE initRpcArchive(VALUE self, VALUE robject);
//extern VALUE startRpmgenRpcService(VALUE self);
extern void startArchiveRpcService(unsigned long registerProtocol);
extern VALUE rpmArchiveRpc;

extern void initXdr();
extern void intHcuBootRpc();
void initXdrEvent();

extern VALUE initHsm(VALUE self, VALUE robject);
extern void startHsmRpcService(unsigned long registerProtocol);
extern VALUE hsmRpc;


//Global value for the RPC transport
SVCXPRT* transp;

VALUE initRpc(VALUE self, VALUE robject)
{
  //Assign the Ruby object the implementing Ruby object
  rpcInstance = robject;
  rb_iv_set(self, "@robject", robject);
  return self;
}

static int createSocket(int port) 
{
	//create
    int sockfd = socket ( AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1) 
    {
		printf("Cannot create RPC socket\n");
		return -1;
	}
    struct sockaddr_in	addr;    
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = INADDR_ANY;
    addr.sin_port = htons ( port );

    //bind
    int bind_return = bind(sockfd, (struct sockaddr *) &addr, sizeof(addr));
    if (bind_return == -1) 
    {
        printf("Cannot bind RPC socket\n");		
		return -1;
	}
    
    //listen
    int MAXCONNECTIONS = 10;
    int listen_return = listen(sockfd, MAXCONNECTIONS);
    if (listen_return == -1) 
    {
		printf("Cannot listen RPC socket\n");
		return -1;
    }
    return sockfd;
}

VALUE startRpcServices(VALUE self, VALUE rport)
{
  int port = NUM2INT(rport);
  unsigned long registerProtocol = 0;
  //If the user has not specified a port, use the portmapper
  if (port == 0)
  {
	  registerProtocol = IPPROTO_TCP;
      transp = svctcp_create(RPC_ANYSOCK, 0, 0);	  
  }
  else
  {
      int sockfd = createSocket(port);	  
      if (sockfd != -1) transp = svctcp_create(sockfd, 0, 0);
  }
  if (transp != NULL)
  {	  
      //Start the RPC service      
      startHcuRpcService(registerProtocol);
      startHsmRpcService(registerProtocol);
      startRpmgenRpcService(registerProtocol);
      startRpmsaRpcService(registerProtocol);
      startArchiveRpcService(registerProtocol);
      svc_run ();
  }
  else
  {
	  printf("Cannot create RPC services\n");
  }
  return Qnil;  
}

void Init_Rpc() {
  //Create mapping from ruby to c. 
  rpc = rb_define_class("Rpc", rb_cObject);  
  rb_define_method(rpc, "initialize", initRpc, 1);  
  rb_define_method(rpc, "start_rpc_services", startRpcServices, 1);
  
  hcuRpc = rb_define_class("HcuRpc", rb_cObject);
  rb_define_method(hcuRpc, "initialize", initHcu, 1);

  hsmRpc = rb_define_class("HsmRpc", rb_cObject);
  rb_define_method(hsmRpc, "initialize", initHsm, 1);
  
  rpmgenRpc = rb_define_class("RpmgenRpc", rb_cObject);
  rb_define_method(rpmgenRpc, "initialize", initRpmgen, 1);

  rpmsaRpc = rb_define_class("RpmsaRpc", rb_cObject);
  rb_define_method(rpmsaRpc, "initialize", initRpmsa, 1);
  
  rpmArchiveRpc = rb_define_class("RpmArchiveRpc", rb_cObject);
  rb_define_method(rpmArchiveRpc, "initialize", initRpcArchive, 1);
  

  initXdrHeader();
  intHcuBootRpc();
  initXdrEvent();
}   

