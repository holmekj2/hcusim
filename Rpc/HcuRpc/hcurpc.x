/*
    .x file for the HCU config RPC Server 
*/

struct hcuRetDiskInfo_s { 
    unsigned long ulReturn;
    unsigned int usedSpace;
    unsigned int freeSpace;
    unsigned int capacity;
};
typedef struct hcuRetDiskInfo_s hcuRetDiskInfo;

struct hcuRetString10_s
{
    unsigned long ulReturn;
    char          str[10];
};
typedef struct hcuRetString10_s hcuRetString10;

struct hcuRetString15_s
{
    unsigned long ulReturn;
    char          str[15];
};
typedef struct hcuRetString15_s hcuRetString15;

struct hcuRetString20_s
{
    unsigned long ulReturn;
    char          str[20];
};
typedef struct hcuRetString20_s hcuRetString20;

struct hcuRetString40_s
{
    unsigned long ulReturn;
    char          str[40];
};
typedef struct hcuRetString40_s hcuRetString40;

struct hcuRetString255_s
{
    unsigned long ulReturn;
    char          str[255];
};
typedef struct hcuRetString255_s hcuRetString255;

struct hcuRetString1024_s
{
    unsigned long ulReturn;
    char          str[1024];
};
typedef struct hcuRetString1024_s hcuRetString1024;

struct hcuRetString_s
{
    unsigned long ulReturn;
    string        str<>;
};
typedef struct hcuRetString_s hcuRetString;

struct hcuRetShort_s
{
    unsigned long ulReturn;
    short         sNum;
};
typedef struct hcuRetShort_s hcuRetShort;

struct hcuRetUShort_s
{
    unsigned long  ulReturn;
    unsigned short usNum;
};
typedef struct hcuRetUShort_s hcuRetUShort;

struct hcuRetULong_s
{
    unsigned long  ulReturn;
    unsigned long  ulNum;
};
typedef struct hcuRetULong_s hcuRetULong;

struct hcuSlotInfo_s 
{
    unsigned int slotNo;
    unsigned short usDefaultVMECfg;
    unsigned short usVMEAddrMode;
    unsigned long  ulVMEAddr;
    unsigned short usVMEIRQ;
    unsigned short usAutoDetect;
    unsigned short usState;
    unsigned short usStatus;
    unsigned short usDevTypeDetected;
    unsigned short usDevTypeAssigned;
    unsigned short usDevId;
};
typedef struct hcuSlotInfo_s hcuSlotInfo;

struct hcuRetSlotInfo_s 
{
    unsigned long ulReturn;
    unsigned int slotNo;
    unsigned short usDefaultVMECfg;
    unsigned short usVMEAddrMode;
    unsigned long  ulVMEAddr;
    unsigned short usVMEIRQ;
    unsigned short usAutoDetect;
    unsigned short usState;
    unsigned short usStatus;
    unsigned short usDevTypeDetected;
    unsigned short usDevTypeAssigned;
    unsigned short usDevId;
};
typedef struct hcuRetSlotInfo_s hcuRetSlotInfo;

struct hcuIoPortInfo_s 
{
    unsigned int PortNo;
    unsigned short usAutoDetect;
    unsigned short usState;
    unsigned short usStatus;
    unsigned short usDevTypeDetected;
    unsigned short usDevTypeAssigned;
    unsigned short usDevId;
};
typedef struct hcuIoPortInfo_s hcuIoPortInfo;

struct hcuRetIoPortInfo_s 
{
    unsigned long ulReturn;
    unsigned int PortNo;
    unsigned short usAutoDetect;
    unsigned short usState;
    unsigned short usStatus;
    unsigned short usDevTypeDetected;
    unsigned short usDevTypeAssigned;
    unsigned short usDevId;
};
typedef struct hcuRetIoPortInfo_s hcuRetIoPortInfo;

struct hcuPhoneInfo_s {
    unsigned int phoneId;
    char phoneNo[40]; 
    char ipAddr [20];
};
typedef struct hcuPhoneInfo_s hcuPhoneInfo;

struct hcuRetPhoneInfo_s {
    unsigned long ulReturn;
    unsigned int phoneId;
    char phoneNo[40]; 
    char ipAddr [20];
};
typedef struct hcuRetPhoneInfo_s hcuRetPhoneInfo;

struct hcuEvtCnctInfo_s {
    char ipAddr [20];
    unsigned short port;
};
typedef struct hcuEvtCnctInfo_s hcuEvtCnctInfo;

struct hcuRetFileInfo_s {
    unsigned long  ulReturn;
    char           filename[255];
    unsigned short usMajorRev;
    unsigned short usMinorRev;
    unsigned long  ulTime;
    unsigned long  ulSize;
};
typedef struct hcuRetFileInfo_s hcuRetFileInfo;

struct hcuFileInfo_s {
    char filename[255];
};
typedef struct hcuFileInfo_s hcuFileInfo;

struct hcuPathFile_s {
    char cPath[255];
	char cFilename[25];
};
typedef struct hcuPathFile_s hcuPathFile;

struct hcuPathFileAction_s {
    char cPath[255];
	char cFilename[25];
	unsigned short usFlag;
};
typedef struct hcuPathFileAction_s hcuPathFileAction;

struct hcuRetPathFileAction_s {
	unsigned long ulReturn;
	hcuPathFileAction fileInfo;
};
typedef struct hcuRetPathFileAction_s hcuRetPathFileAction;

struct hcuRetUnInt_s {
	unsigned long ulReturn;
	unsigned int unNum;
};
typedef struct hcuRetUnInt_s hcuRetUnInt;

struct hcuRetBegBackupInfo_s {
	unsigned long ulReturn;
	short sFlag;
	unsigned int unCookie;
};

typedef struct hcuRetBegBackupInfo_s hcuRetBegBackupInfo;

struct hcuSnmpState_s {
	unsigned long ulReturn;
	short snmpState;
};

typedef struct hcuSnmpState_s hcuSnmpState;

/* Contained by HcuDatabases */
struct HcuDatabase {
    /* 'lines' is the actual database format, in csv. */
    string lines<>;
    /* crc of 'lines' */
    unsigned long crc;    
};

/* Input argument to syncDatabase */
struct HcuDatabases {
    struct HcuDatabase channelConfigurationTable;
    struct HcuDatabase docsisPreambleDatabaseTable;
    struct HcuDatabase intervalUsageCodeTable;
    /* arbitrary database ID generated by server and echoed back by HCU */
    string      databaseId<>;
};

struct HcuDatabaseId {
    string        id<>;
    unsigned long status; /* One of:
                                 INTF_NO_ERROR
                                 INTF_ERR_CRC       0x80043000
                                 INTF_ERR_NO_DB     0x80043002 */
};

program HCUCFGSVR {
	version HCUCFGSVRVERS {
	 hcuRetString40    hcuGetModelNo ( void )                  = 1 ;
	 hcuRetString15    hcuGetSerialNo ( void )                 = 2 ;
	 unsigned long     hcuPutSerialNo( string )                = 3 ; 
	 hcuRetString40    hcuGetSbcModelNo ( void )               = 4 ;
	 hcuRetString10    hcuGetSbcFlashSize( void )              = 5 ;
	 hcuRetString10    hcuGetSbcDramSize ( void )              = 6 ;
	 hcuRetString10    hcuGetSbcFirmwareRev ( void )           = 7 ;
	 hcuRetString10    hcuGetSwRev ( void )                    = 8 ;
	 hcuRetUShort      hcuGetNumHardDisk ( void )              = 9 ;
	 hcuRetDiskInfo    hcuGetHardDiskInfo( unsigned short )    = 10;
	 unsigned long     hcuPutLabel ( string )                  = 11;
	 hcuRetString40    hcuGetLabel ( void )                    = 12;
	 unsigned long     hcuPutLocation ( string )               = 13;
	 hcuRetString255   hcuGetLocation ( void )                 = 14;
	 unsigned long     hcuPutNotes ( string )                  = 15;
	 hcuRetString1024  hcuGetNotes ( void )                    = 16;
	 unsigned long     hcuPutUid ( unsigned long )             = 17;
	 hcuRetULong       hcuGetUid ( void )                      = 18;
	 hcuRetUShort      hcuGetNumSlots ( void )                 = 19;
	 unsigned long     hcuPutSlotInfo ( hcuSlotInfo )          = 20;
	 hcuRetSlotInfo    hcuGetSlotInfo ( unsigned short )       = 21; 
	 unsigned long     hcuPutPhoneAddr ( hcuPhoneInfo )        = 22;
	 hcuRetPhoneInfo   hcuGetPhoneAddr ( void )                = 23;
	 unsigned long     hcuPutNetworkAddr ( string )            = 24;
	 hcuRetString20    hcuGetNetworkAddr ( void )              = 25;
	 unsigned long     hcuPutScPhoneAddr ( hcuPhoneInfo )      = 26;
	 hcuRetPhoneInfo   hcuGetScPhoneAddr ( unsigned short )    = 27;
	 unsigned long     hcuPutScNetworkAddr ( string )          = 28;
	 hcuRetString20    hcuGetScNetworkAddr ( void )            = 29;
	 unsigned long     hcuPutScGatewayAddr( string )           = 30;
	 hcuRetString20    hcuGetScGatewayAddr ( void )            = 31;
	 unsigned long     hcuPutWatchDogState ( short )           = 32;
	 hcuRetShort       hcuGetWatchDogState ( void )            = 33;
	 unsigned long     hcuPutWatchDogInterval ( unsigned long) = 34;
	 hcuRetULong       hcuGetWatchDogInterval ( void )         = 35;
	 unsigned long     hcuPutTime ( unsigned long )            = 36;
	 hcuRetULong       hcuGetTime ( void )                     = 37;
	 unsigned long     hcuCfgConnect ( void )                  = 38;
	 unsigned long     hcuCfgDisconnect ( void )               = 39;
	 unsigned long	   hcuSyncConfig( void )				   = 40;
	 unsigned long	   hcuPutIoPortInfo(hcuIoPortInfo)         = 41;
	 hcuRetUShort      hcuGetNumIoPorts( void )                = 42;
	 hcuRetIoPortInfo  hcuGetIoPortInfo(unsigned short)        = 43;
	 hcuSnmpState	   hcuGetSNMPState( void )				   = 44;
	 unsigned long     hcuPutSNMPState( short )				   = 45;
	} = 1;
    
    version HCUCFGSVRVERS_2 {
        /* This call returns the database id that was passed in
           by the syncDatabase call.  */
        struct HcuDatabaseId           hcuGetDatabaseId( void )       = 46;
	
        /* Note: Detected channels are cleared when syncDatabase is called
        /* Error Codes:
        /*    INTF_NO_ERROR      0x0
        /*   INTF_ERR_CRC        0x80043000 
        /*   INTF_ERR_VALIDATION 0x80043001 */
        unsigned long    syncDatabase( struct HcuDatabases )          = 47;

        /* if clearDetectedChannels is true, all detected channels are cleared before system scan */
        unsigned long    startUcdSystemScan( bool /* clearDetectedChannels */) = 48;
    } = 2;
    
    version HCUCFGSVRVERS_3 {
        hcuRetUShort  hcuGetRpm3000MaxNumSAs( void )                  = 49;
        unsigned long hcuSetRpm3000MaxNumSAs( unsigned short )        = 50;
    } = 3;
    
    version HCUCFGSVRVERS_4 {
        hcuRetString  hcuGetLabel ( void )                           = 12;
        hcuRetString  hcuGetLocation ( void )                        = 14;
        hcuRetString  hcuGetNotes ( void )                           = 16;
    } = 4;
} = 0x20000001;

program HCUEVTSVR {
	version HCUEVTSVRVERS {
       unsigned long hcuEvtConnect ( hcuEvtCnctInfo )          = 1;
	   unsigned long hcuEvtDisconnect ( void )                 = 2;
	   unsigned long hcuEvtEnableEventTransfer ( void )        = 3;
	   unsigned long hcuEvtDisableEventTransfer ( void )       = 4;
	   unsigned long hcuPutHighWaterMark ( unsigned long )     = 5;
	   unsigned long hcuEvtIsConnected ( void )                = 6;
	} = 1;
} = 0x20000002;

program HCUCOMMSVR {
    version HCUCOMMSVRVERS {
	unsigned long ResetInterfaceConnections ( void )           =  1;
	unsigned long ResetHcu( unsigned long )                    =  2;
    } = 1;
} = 0x20000009;

program HCUSERVICESVR {
    version HCUSERVICESVRVERS {
	unsigned long  hcuSvcConnect ( void )                      =  1;
	unsigned long  hcuSvcDisconnect ( void )                   =  2;
	hcuRetString10 hcuSvcGetSoftwareRev ( void )               =  3;
	hcuRetFileInfo hcuSvcFindSWFileFirst ( void )              =  4;
	hcuRetFileInfo hcuSvcFindSWFileNext ( void )               =  5;
	unsigned long  hcuSvcInstallSW ( hcuFileInfo )             =  6;
    unsigned long  hcuSvcClean( string )					   =  7;
    } = 1;
} = 0x2000000a;

program HCURCYSVR {
    version HCURCYSVRSERS {
	unsigned long         hcuRcyConnect ( void )               =  1;
	unsigned long         hcuRcyDisconnect ( void )            =  2;
	unsigned long         hcuRcyResetChangeLog ( void )        =  3;
	hcuRetBegBackupInfo   hcuRcyBeginBackup ( void )           =  4;
	unsigned long         hcuRcyEndBackup ( unsigned int )     =  5;
	unsigned long         hcuRcyAbortBackup ( unsigned int )   =  6;
	hcuRetUnInt           hcuRcyBeginRestore ( void )          =  7;
	unsigned long         hcuRcyEndRestore ( unsigned int )    =  8;
	unsigned long         hcuRcyAbortRestore ( unsigned int )  =  9;
	hcuRetPathFileAction  hcuRcyBackupFile ( void )            =  10;
	unsigned long         hcuRcyRestoreFile ( hcuPathFile )    =  11;
    } = 1;
} = 0x2000000f;


/*
    .x file for the HCU config RPC Server 
*/

const MAX_PAYLOAD = 5;

struct EvtMsg_s {
    u_long      ulSequenceNum;
    u_long      ulEventId;
    u_long      ulSourceUid;
    u_long      ulTime;
    u_short     usPriority;
    u_short     usPayloadLen;
    u_long      ulPayload[MAX_PAYLOAD];
};

typedef u_long EvtAck;

typedef struct EvtMsg_s EvtMsg;
