require 'Rpc/rpc_utils'
#require 'Rpc/rpc_server'
#require 'Rpc/HcuRpc/Rpc'
require 'apputils/hcu_file_utils'
require 'Rpc/Intercepter/rpc_override'
require 'log'

class RpmgenRpcHandler
  include RpcUtils
  RPM_FIRMWARE_DIRECTORY = '/DOS1/Firmware/RPM/'
  #Pass in an instance of a RPM delegate class that handles the RPC requests.
  def initialize(rpm_delegator)
    @rpm_delegator = rpm_delegator
    #Create a chain of RPC handlers.
    #rpc_handlers = [secondary_rpc_handlers, self]
    #Flatten the array in case the input (secondary_rpc_handlers) was an array 
    #rpc_handlers.flatten!
    #@rpc_server = RpcServer.new(RpmgenRpc, rpc_handlers)
  end
  def rpm1000cfggetmodelno(args)
    ret = {}
    begin
      rpm = @rpm_delegator.get_rpm(args[:rpm])
      model = rpm.model
      model = validate_string(model)
      ret = {:ulReturn => 0, :str => model}
    rescue Exception
      Log::GeneralLogger.error "rpm1000cfggetmodelno for RPM#{args[:rpm]} failed"
      #TODO Add IntfError constants to ruby
      ret = {:ulReturn => 0x80041001, :str => ""}
    end
    return ret
  end

  def rpm1000cfgputstate(args)
    ret = {}
    begin
      rpm = @rpm_delegator.get_rpm(args[:rpm])
      #Set the state to a boolean value
      device_enabled = (args[:state] == 1)
      rpm.device_enabled = device_enabled
      rpm.save
      ret = {:ulReturn => 0}
    rescue Exception
      Log::GeneralLogger.error "rpm1000cfgputstate for RPM#{args[:rpm]} failed"
      #TODO Add IntfError constants to ruby
      ret = {:ulReturn => 0x80041001}
    end
    return ret
  end

  def rpm1000cfggetstate(args)    
    ret = {}
    begin
      rpm = @rpm_delegator.get_rpm(args[:rpm])
      device_enabled = rpm.device_enabled
      #Translate to an integer to send over RPC
      state = (device_enabled == true) ? 1 : 0;      
      ret = {:ulReturn => 0, :state => state}            
    rescue Exception
      Log::GeneralLogger.error "rpm1000cfggetstate for RPM#{args[:rpm]} failed"
      #TODO Add IntfError constants to ruby
      ret = {:ulReturn => 0x80041001, :state => 0}
    end
    return ret
  end
  
  def rpm1000cfggetserialno(args)
    ret = {}
    begin
      rpm = @rpm_delegator.get_rpm(args[:rpm])
      serial_number = rpm.serial_number
      serial_number = validate_string(serial_number)
      ret = {:ulReturn => 0, :str => serial_number}
    rescue Exception
      Log::GeneralLogger.error "rpm1000cfggetserialno for RPM#{args[:rpm]} failed"
      #TODO Add IntfError constants to ruby
      ret = {:ulReturn => 0x80041001, :str => ""}
    end
    return ret
  end

  def rpm1000cfggetfirmwarerev(args)
    ret = {}    
    begin
      rpm = @rpm_delegator.get_rpm(args[:rpm])
      fw_version = rpm.fw_version
      fw_version = validate_string(fw_version)
      ret = {:ulReturn => 0, :str => fw_version}
    rescue Exception
      Log::GeneralLogger.error "rpm1000cfggetfirmwarerev for RPM#{args[:rpm]} failed"
      #TODO Add IntfError constants to ruby
      ret = {:ulReturn => 0x80041001, :str => ""}
    end
    return ret
  end
  
  def rpm1000cfggetlabel(args)
    ret = {}
    begin
      rpm = @rpm_delegator.get_rpm(args[:rpm])
      label = rpm.label
      label = validate_string(label)
      ret = {:ulReturn => 0, :str => label}
    rescue Exception
      Log::GeneralLogger.error "rpm1000cfggetlabel for RPM#{args[:rpm]} failed"
      #TODO Add IntfError constants to ruby
      ret = {:ulReturn => 0x80041001, :str => ""}
    end
    return ret
  end
  
  def rpm1000cfgputlabel(args)
    ret = {}
    begin
      rpm = @rpm_delegator.get_rpm(args[:rpm])
      rpm.label = args[:str].rstrip
      rpm.save
      ret = {:ulReturn => 0}
    rescue Exception
      Log::GeneralLogger.error "rpm1000cfgputlabel for RPM#{args[:rpm]} failed"
      #TODO Add IntfError constants to ruby
      ret = {:ulReturn => 0x80041001}
    end
    return ret
  end

  def rpm1000cfggetnotes(args)
    ret = {}
    begin
      rpm = @rpm_delegator.get_rpm(args[:rpm])
      notes = rpm.notes
      notes = validate_string(notes)
      ret = {:ulReturn => 0, :str => notes}
    rescue Exception
      Log::GeneralLogger.error "rpm1000cfggetlabel for RPM#{args[:rpm]} failed"
      #TODO Add IntfError constants to ruby
      ret = {:ulReturn => 0x80041001, :str => ""}
    end
    return ret
  end
  
  def rpm1000cfgputnotes(args)
    ret = {}
    begin
      rpm = @rpm_delegator.get_rpm(args[:rpm])
      rpm.notes = args[:str]
      rpm.save
      ret = {:ulReturn => 0}
    rescue Exception
      Log::GeneralLogger.error "rpm1000cfgputnotes for RPM#{args[:rpm]} failed"
      #TODO Add IntfError constants to ruby
      ret = {:ulReturn => 0x80041001}
    end
    return ret
  end
  
  def rpm1000cfggetfreqrange(args)
    ret = {}
    begin
      rpm = @rpm_delegator.get_rpm(args[:rpm])
      minimum_frequency = rpm.minimum_frequency
      maximum_frequency = rpm.maximum_frequency
      ret = {:ulReturn => 0, :dMinFreq => minimum_frequency, :dMaxFreq => maximum_frequency}
    rescue Exception
      Log::GeneralLogger.error "rpm1000cfggetfreqrange for RPM#{args[:rpm]} failed"
      #TODO Add IntfError constants to ruby
      ret = {:ulReturn => 0x80041001, :str => ""}
    end
    return ret
  end

  def rpm1000cfgputuid(args)
    ret = {}
    begin
      rpm = @rpm_delegator.get_rpm(args[:rpm])
      rpm.uid = args[:uid]
      rpm.save
      ret = {:ulReturn => 0}
    rescue Exception
      Log::GeneralLogger.error "rpm1000cfgputuid for RPM#{args[:rpm]} failed"
      #TODO Add IntfError constants to ruby
      ret = {:ulReturn => 0x80041001}
    end
    return ret
  end

  def rpm1000cfggetuid(args)    
    ret = {}
    begin
      rpm = @rpm_delegator.get_rpm(args[:rpm])
      ret = {:ulReturn => 0, :uid => rpm.uid}            
    rescue Exception
      Log::GeneralLogger.error "rpm1000cfggetuid for RPM#{args[:rpm]} failed"
      #TODO Add IntfError constants to ruby
      ret = {:ulReturn => 0x80041001, :state => 0}
    end
    return ret
  end
  
  def rpmdeploylicensekey(args)
      Log::GeneralLogger.info "rpmdeploylicensekey rpm=#{args[:rpm]}, key=#{args[:key]}"
      #Just return good for now. 
      ret = {:ulReturn => 0}
  end

  def rpmgetfeatures(args)    
    ret = {}
    begin
      rpm = @rpm_delegator.get_rpm(args[:rpm])
      ret = {:ulReturn => 0, :features => rpm.get_features}
    rescue Exception
      Log::GeneralLogger.error "rpmgetfeatures for RPM#{args[:rpm]} failed"
      ret = {:ulReturn => 0x80041001, :features => 0}
    end
    return ret
  end
  
  def rpm1000svcfindfwfilefirst(args)    
    #Update the directory list    
    @fw_file_list = Dir.entries(RPM_FIRMWARE_DIRECTORY)
    file = HcuFileUtils.get_next_file(RPM_FIRMWARE_DIRECTORY, @fw_file_list)
    if file != nil
      ret = {:ulReturn => 0, :filename => file, :usMajorRev => 0,
        :usMinorRev => 0, :ulTime => 0, :ulSize => 0}
    else
      ret = {:ulReturn => 0x8004100e, :filename => "", :usMajorRev => 0,
        :usMinorRev => 0, :ulTime => 0, :ulSize => 0}
    end
  end
  def rpm1000svcfindfwfilenext(args)
    #Update the directory list    
    file = HcuFileUtils.get_next_file(RPM_FIRMWARE_DIRECTORY, @fw_file_list)
    if file != nil
      ret = {:ulReturn => 0, :filename => file, :usMajorRev => 0,
        :usMinorRev => 0, :ulTime => 0, :ulSize => 0}
    else
      ret = {:ulReturn => 0x8004100e, :filename => "", :usMajorRev => 0,
        :usMinorRev => 0, :ulTime => 0, :ulSize => 0}
    end
  end
  def rpm1000svcinstallfw(args)
    ret = {}
    begin
      rpm = @rpm_delegator.get_rpm(args[:rpm])
      file_list = Dir.entries(RPM_FIRMWARE_DIRECTORY)  
      if file_list.include?(args[:filename])
        rpm.install_software(RPM_FIRMWARE_DIRECTORY + args[:filename])      
        ret = {:ulReturn => 0}
      else
        Log::GeneralLogger.error "rpm1000svcinstallfw firmware file not found for RPM#{args[:rpm]} failed"
        ret = {:ulReturn => 0x80041001}
      end
    rescue Exception
      Log::GeneralLogger.error "rpm1000svcinstallfw for RPM#{args[:rpm]} failed"
      #TODO Add IntfError constants to ruby
      ret = {:ulReturn => 0x80041001, :state => 0}
    end
    return ret
  end
  def rpm1000cfgputportlabel(args)
    ret = {}
    begin      
      port = @rpm_delegator.get_rpm(args[:rpm]).ports[args[:port]]
      port.label = args[:str].rstrip
      port.save
      ret = {:ulReturn => 0}
    rescue Exception
      Log::GeneralLogger.error "rpm1000cfgputportlabel for RPM#{args[:rpm]}, Port#{args[:port]} failed"
      #TODO Add IntfError constants to ruby
      ret = {:ulReturn => 0x80041001}
    end
    return ret
  end  
  def rpm1000cfggetportlabel(args)
    ret = {}
    begin
      port = @rpm_delegator.get_rpm(args[:rpm]).ports[args[:port]]
      label = port.label
      label = validate_string(label)
      ret = {:ulReturn => 0, :str => label}
    rescue Exception
      Log::GeneralLogger.error "rpm1000cfggetlabel for RPM#{args[:rpm]}, Port#{args[:port]} failed"
      #TODO Add IntfError constants to ruby
      ret = {:ulReturn => 0x80041001, :str => ""}
    end
    return ret
  end
  def rpm1000cfgputportuid(args)
    ret = {}
    begin
      port = @rpm_delegator.get_rpm(args[:rpm]).ports[args[:port]]
      port.uid = args[:uid]
      port.save
      ret = {:ulReturn => 0}
    rescue Exception
      Log::GeneralLogger.error "rpm1000cfgputportuid for RPM#{args[:rpm]}, Port#{args[:port]} failed"
      #TODO Add IntfError constants to ruby
      ret = {:ulReturn => 0x80041001}
    end
    return ret
  end

  def rpm1000cfggetportuid(args)    
    ret = {}
    begin
      port = @rpm_delegator.get_rpm(args[:rpm]).ports[args[:port]]
      ret = {:ulReturn => 0, :uid => port.uid}            
    rescue Exception
      Log::GeneralLogger.error "rpm1000cfggetportuid for RPM#{args[:rpm]}, Port#{args[:port]} failed"
      #TODO Add IntfError constants to ruby
      ret = {:ulReturn => 0x80041001, :state => 0}
    end
    return ret
  end
  
  def rpm1000monconnect(args)
    ret = {:ulReturn => 0}
  end

  def rpm1000mondisconnect(args)
    ret = {:ulReturn => 0}
  end
  
  def rpm1000monconnect(args)
    ret = {:ulReturn => 0}
  end
 
  def rpm1000monputmonstate(args)  
    ret = {:ulReturn => 0}
  end

  def rpm1000mongetmonstate(args)  
    ret = {:ulReturn => 0, :state => 0}
  end

  def rpm1000monputlimitstate(args)  
    ret = {:ulReturn => 0}
  end

  def rpm1000mongetlimitstate(args)  
    ret = {:ulReturn => 0, :state => 0}
  end

  def rpm1000monputlimittype(args)  
    ret = {:ulReturn => 0}
  end

  def rpm1000mongetlimittype(args)  
    ret = {:ulReturn => 0, :limit_id => 0}
  end

  def rpm1000moninitmeasconfig(args)  
    ret = {:ulReturn => 0}
  end

  def rpm1000monputmeasconfig(args)  
    ret = {:ulReturn => 0}
  end

  def rpm1000mongetmeasconfig(args)  
    ret = {:ulReturn => 0}
  end
  
  #Called if the RPC handler does not implement a method
  def method_missing(method, args=nil)
    #Log::GeneralLogger.fatal "Rpmgen RPC does not handle: #{method}"
    return nil
  end
end



  
