require 'Rpc/rpc_utils'
require 'Rpc/rpc_server'
require 'apputils/hcu_file_utils'
#require 'Rpc/SaRpc/RpmsaRpc'

class RpmsaRpcHandler
  include RpcUtils
  #Pass in an instance of a RPM delegate class that handles the RPC requests.
  def initialize(rpm_delegator)
    @rpm_delegator = rpm_delegator
    #@rpc_server = RpcServer.new(RpmsaRpc, self)
  end
  def rpm1000saconnect(args)
    ret = 0
    begin
      Log::GeneralLogger.info "rpm1000saconnect1"  
      rpm = @rpm_delegator.get_rpm(args[:rpm])  
      status = rpm.sa_connect(args[:server_ip], args[:server_port])
      Log::GeneralLogger.info "rpm1000saconnect2"  
      ret = {:ulReturn => status}
    rescue Exception
      Log::GeneralLogger.error "rpm1000saconnect for RPM#{args[:rpm]} failed"
      #TODO Add IntfError constants to ruby
      ret = {:ulReturn => 0x80041001}
    end
    return ret
  end
  def rpm1000sadisconnect(args)
    ret = 0  
    begin
      rpm = @rpm_delegator.get_rpm(args[:rpm])  
      status = rpm.sa_disconnect(args[:server_port])
      ret = {:ulReturn => status}
    rescue Exception
      Log::GeneralLogger.error "rpm1000sadisconnect for RPM#{args[:rpm]} failed"
      #TODO Add IntfError constants to ruby
      ret = {:ulReturn => 0x80041001}
    end
    return ret
  end
  def rpm1000saabortsngltrace(args)
    ret = 0  
    begin
      rpm = @rpm_delegator.get_rpm(args[:rpm])       
      ret = {:ulReturn => 0}
    rescue Exception
      Log::GeneralLogger.error "rpm1000saabortsngltrace for RPM#{args[:rpm]} failed"
      #TODO Add IntfError constants to ruby
      ret = {:ulReturn => 0x80041001}
    end
    return ret
  end
  def rpm1000sareqsngltrace(args)
    ret = 0
    begin
      rpm = @rpm_delegator.get_rpm(args[:rpm]) 
      status = rpm.spectrum_analyzers[args[:usSocketPort]].get_single_trace(args)
      ret = {:ulReturn => status}
    rescue Exception
      Log::GeneralLogger.error "rpm1000sareqsngltrace for RPM#{args[:rpm]} failed"
      #TODO Add IntfError constants to ruby
      ret = {:ulReturn => 0x80041001}
    end
    return ret
  end
  def rpm1000sareqmultitrace(args)
    ret = 0
    begin
      rpm = @rpm_delegator.get_rpm(args[:usRpmId])  
      status = rpm.spectrum_analyzers[args[:usSocketPort]].start_multi_trace(args)
      ret = {:ulReturn => status}
    rescue Exception
      Log::GeneralLogger.error "rpm1000sareqmultitrace for RPM#{args[:rpm]} failed"
      #TODO Add IntfError constants to ruby
      ret = {:ulReturn => 0x80041001}
    end
    return ret
  end
  def rpm1000sahaltmultitrace(args)
    ret = 0  
    begin
      rpm = @rpm_delegator.get_rpm(args[:usRpmId])  
      status = rpm.spectrum_analyzers[args[:usSocketPort]].halt_multi_trace
      ret = {:ulReturn => status}
    rescue Exception
      Log::GeneralLogger.error "rpm1000sahaltmultitrace for RPM#{args[:rpm]} failed"
      #TODO Add IntfError constants to ruby
      ret = {:ulReturn => 0x80041001}
    end
    return ret
  end
  
  
  #Called if the RPC handler does not implement a method
  def method_missing(method, args=nil)
    #Log::GeneralLogger.error "Rpmsa RPC does not handle: #{method}"
    return nil
  end
end
