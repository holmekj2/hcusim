#include <rpc/rpc.h>
#include "HCUBootrpc.h"
#include "ruby.h"
#include "../cruby_utils.h"
#include <stdio.h>

//Class singleton (i.e. a ruby class object)
VALUE hcuBootRpc;


static VALUE init(VALUE self)
{
  return self;
}

/*****************************************************************************/
/*                                                                           */
/*****************************************************************************/
//Send the HCU online RPC message. 
//Returns 0 if successful, -1 if the client could not connect to an RPC server, -2 if the RPC call failed, -3 if the RPC returned a failed code
static VALUE sendHcuOnline(VALUE self, VALUE server_ip, VALUE hcu_ip, VALUE hcu_uid, VALUE boot_status)
{
    // Attach to the RPC boot server and call Online method.
    //
    CLIENT *rpcClient;
    char* serverIp;
    char* hcuIp;    
    unsigned int hcuUid;
    unsigned int bootStatus;    
    int ret = 0;
    unsigned long *pConnected;    
    HcuBootIdInfo HCUInfo;            
    
    //Convert ruby types to c
    serverIp = STR2CSTR(server_ip);
    hcuIp = STR2CSTR(hcu_ip);   
    hcuUid = NUM2INT(hcu_uid);
    bootStatus = NUM2INT(boot_status);    
    
    if ((rpcClient =
         clnt_create (serverIp, HCUBOOTSVR, HCUBOOTSVRVERS,
                      "tcp")) == NULL)
    {
		printf("Cannot create boot rpc client\n");
        // Couldn't establish connection with server.
        ret = -1;
        //Convert the return code to Ruby and return
        return (INT2FIX(ret));        
    }   

    HCUInfo.ulHcuUID = hcuUid;
    HCUInfo.ulReturn = bootStatus;
    strcpy(HCUInfo.HcuIpAdder, hcuIp);
    

    // Make the RPC call
    pConnected = hcuonline_1 (&HCUInfo, rpcClient);
    if (pConnected != NULL)
    {
        if (*pConnected != 0)
        {
		    printf("Cannot make boot rpc call\n");
          //RPC returned a bad status
          ret = -3;
        }
    }
    else
    {
        printf("Cannot make boot rpc call\n");
        //RPC call failed
        ret = -2;
    }
    //Close the RPC client
    clnt_destroy (rpcClient);
    //Convert the return code to Ruby and return
    return (INT2FIX(ret));
}

void intHcuBootRpc() {
  //Create mapping from ruby to c. Ruby object will be named XdrCommon
  hcuBootRpc = rb_define_class("HcuBootRpc", rb_cObject);

  rb_define_method(hcuBootRpc, "initialize", init, 0);
  rb_define_method(hcuBootRpc, "send_hcu_online", sendHcuOnline, 4);  
}    

