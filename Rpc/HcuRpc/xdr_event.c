#include "ruby.h"
#include <rpc/rpc.h>
#include "hcurpc.h"
#include <stdio.h>
#include "hdr.h"

static VALUE xdr_event_init(VALUE self)
{
  return self;
}

//Decode an incoming event message. It is assumed that the header has been decoded
//at this point because it contains the length. 
//Returns an array with the event message
static VALUE xdr_evt_decode(VALUE self, VALUE r_input_stream, VALUE msg_length)
{
    char* inputStream;
    Hdr hdr; 
    VALUE arr, payload_arr;
    XDR xdrIn;
    EvtMsg EventMsg;
    unsigned int i;

    inputStream = STR2CSTR(r_input_stream);
    xdrmem_create(&xdrIn, inputStream, NUM2INT(msg_length), XDR_DECODE);    
    xdr_EvtMsg(&xdrIn, &EventMsg);

    arr = rb_ary_new2(5);
    rb_ary_push(arr, INT2FIX(EventMsg.ulSequenceNum));
    rb_ary_push(arr, INT2FIX(EventMsg.ulEventId));
    rb_ary_push(arr, INT2FIX(EventMsg.ulSourceUid));
    rb_ary_push(arr, INT2FIX(EventMsg.ulTime));
    payload_arr = rb_ary_new2(EventMsg.usPayloadLen);
    for (i = 0; i < EventMsg.usPayloadLen; ++i)
    {
       rb_ary_push(payload_arr, INT2FIX(EventMsg.ulPayload[i]));        
    }
    rb_ary_push(arr, payload_arr);
    return arr;
}

//Decode an ack message. It is assumed that the header has been decoded
//at this point because it contains the length. 
//Returns the sequence number of the ack
static VALUE xdr_evtack_decode(VALUE self, VALUE r_input_stream, VALUE msg_length)
{
    char* inputStream;
    XDR xdrIn;
    EvtAck ack;

    inputStream = STR2CSTR(r_input_stream);
    xdrmem_create(&xdrIn, inputStream, NUM2INT(msg_length), XDR_DECODE);    
    xdr_EvtAck(&xdrIn, &ack);
    return INT2FIX(ack);
}


//Encode an outgoing ack message
static VALUE xdr_evtack_encode(VALUE self, VALUE sequence_number)
{
    char outputStream[HDR_LEN + 10];
    Hdr hdr; 
    unsigned long seq = NUM2INT(sequence_number);

    //create xdr instance
    XDR xdrOut;
    //Output stream now contains xdr data
    xdrmem_create(&xdrOut, outputStream, HDR_LEN + 10, XDR_ENCODE);
    xdr_setpos( &xdrOut, HDR_LEN);
    xdr_EvtAck(&xdrOut, &seq);
    hdr.msgId = 0;
    hdr.msgLen = xdr_getpos(&xdrOut);
    xdr_setpos(&xdrOut, 0);
    xdr_Hdr(&xdrOut, &hdr);
    //return rb_ary_new4(HDR_LEN / sizeof(VALUE), (VALUE*)outputStream);
    //return rb_ary_new2(HDR_LEN);
    return rb_str_new(outputStream, hdr.msgLen);
}

//Encode an outgoing ack message
static VALUE xdr_evt_encode(VALUE self, VALUE sequence_number, VALUE event_id, VALUE source_uid, VALUE priority, VALUE time, VALUE payload)
{
    char outputStream[HDR_LEN + 50];
    EvtMsg msg; 
    Hdr hdr;
    unsigned int i;
    msg.ulSequenceNum = NUM2INT(sequence_number);
    msg.ulEventId = NUM2INT(event_id);
    msg.ulSourceUid = NUM2INT(source_uid);
    msg.usPriority = NUM2INT(priority);
    msg.ulTime =  NUM2ULONG(time);
    msg.usPayloadLen = RARRAY(payload)->len;
    for (i = 0; i < msg.usPayloadLen; ++i)
    {
        msg.ulPayload[i] = NUM2ULONG(rb_ary_shift(payload));
    } 

    //create xdr instance
    XDR xdrOut;
    //Output stream now contains xdr data
    xdrmem_create(&xdrOut, outputStream, HDR_LEN + 50, XDR_ENCODE);
    xdr_setpos( &xdrOut, HDR_LEN);
    xdr_EvtMsg(&xdrOut, &msg);
    hdr.msgId = 1000;
    hdr.msgLen = xdr_getpos(&xdrOut);
    xdr_setpos(&xdrOut, 0);
    xdr_Hdr(&xdrOut, &hdr);
    return rb_str_new(outputStream, hdr.msgLen);
}

VALUE xdrEvent;

//To create makefile open irb require 'mkmk' create_makefile "XdrCommon"
//Always need the Init_Name function
void initXdrEvent() {
  //Create mapping from ruby to c. Ruby object will be named XdrCommon
  xdrEvent = rb_define_class("XdrEvent", rb_cObject);

  //Ruby constructor
  //xdrEvent = XdrEvent.new
  rb_define_method(xdrEvent, "initialize", xdr_event_init, 0);
  rb_define_method(xdrEvent, "encode_evt_ack", xdr_evtack_encode, 1);
  rb_define_method(xdrEvent, "encode_evt", xdr_evt_encode, 6);  
  rb_define_method(xdrEvent, "decode_evt", xdr_evt_decode, 2);
  rb_define_method(xdrEvent, "decode_evt_ack", xdr_evtack_decode, 2);  
}    
    
 
