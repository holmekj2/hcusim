require 'log'
require 'Rpc/Intercepter/override_parser'

class RpcOverride
  def initialize(filename = nil)
    @overrides = Hash.new
    override_parser = OverrideParser.new
    filename = "Rpc/Intercepter/overrides.cfg" if filename.nil?
    file = File.open(filename, "r")
    file.each do |line|
      #puts "line #{line}"
      #Removing leading and trailing whitespace
      line.strip!
      #Parse the line if it isn't a comment
      if line[0].chr != '#' 
        inputs, returns = override_parser.parse_line(line)
        #Create the method name based on the input
        create_method_name(inputs)
        #Stick the inputs and returns hashes into a hash composed of all the overrides keyed by the method name
        @overrides[inputs[:method]] = {:inputs => inputs, :returns => returns}
        #p @overrides[inputs[:method]]
        #Dynamically create a method in this class named by the first column of each row in the overrides file.
        #The dynamically created method will call return_error with a key to the hash which contains         
        #@overrides[override[0]] = {:number_rpc_fails => override[1].to_i, :error_code => override[2]}
        RpcOverride.send(:define_method, inputs[:method], lambda{return_error(inputs[:method])})
        Log::GeneralLogger.info "Found override for #{inputs[:method]}"
      end
    end
    file.close
  end
  def return_error(method_name)
    Log::GeneralLogger.info "overriding rpc: #{method_name}"
    #Make sure that the number_rpc_overrides is greater than zero
    if @overrides[method_name][:inputs][:fails] > 0
      #ret = {:ulReturn => @overrides[method_name][:error_code].hex}
      #Set the return to parse args from the override file
      ret = @overrides[method_name][:returns]
      #Decrement the number of overrides
      @overrides[method_name][:inputs][:fails] -= 1
      #When we reach zero dynamically delete this method from this class so we no longer intercept
      if @overrides[method_name][:inputs][:fails] == 0
        RpcOverride.send :undef_method, method_name 
      end
      #If there is a timeout defined then wait that long before returning
      if @overrides[method_name][:inputs].has_key?(:timeout)
        timeout = @overrides[method_name][:inputs][:timeout]
        Log::GeneralLogger.info "RPC override pausing for #{timeout} seconds"
        sleep(timeout)
      end 
    end
    return ret
  end
  #For the HCU overrides the dynamic override methods will be called directly. However,
  #the RPM and port method dynamic names have the r and p appended to the end. Therefore the
  #RPC method call will not directly correspond to the override call. method_missing intercepts
  #all method calls that don't have a defined method. We'll override method_missing here to 
  #route the RPM RPC calls to the override. 
  #Returns nil if there is not an override method. Returns a hash of return parameters if the
  #method is overriden. 
  def method_missing(method, args=nil)
    ret = nil
    method_name = String.new(args[:method])
    if args.has_key?(:rpm)
      method_name << "_r" << args[:rpm].to_s    
      if args.has_key?(:port)
        method_name << "_p" << args[:port].to_s    
      end
    end
    #Call the base object responds_to? method
    handles = respond_to?(method_name)
    #If there is an overriden method call it here
    if handles 
      #Manipulate the input argument for the new method name. I was getting an occasional crash
      #when I directly changed the input args hash which I think is due to it being created in the c code. 
      args[:method] = method_name      
      #Call the method again this time with the corrected name. 
      ret = send(args[:method], args) 
    end
    return ret
  end

private
  #If the override call is destined for an RPM or port then we'll create a method name that indicates the RPM
  #and port.
  def create_method_name(override_inputs)
    if override_inputs.has_key?(:inrpm)
      #This call has an rpm parameter so we'll expand the method name to include the rpm number
      override_inputs[:method] << "_r" << override_inputs[:inrpm].to_s
      if override_inputs.has_key?(:inport)
        #This call has a port parameter so we'll expand the method name to include the port number
        #Ports in the config file are 1 - 8 but the incoming calls are 0 - 7. Therefore we'll offset it here. 
        override_inputs[:method] << "_p" << (override_inputs[:inport] - 1).to_s
      end
    end
    #puts "create_method_name #{override_inputs[:method]}"
  end
end



