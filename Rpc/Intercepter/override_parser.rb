class OverrideParser
  #Parse the override into a hash
  def parse_line(line)
    #First split the line by the colon which is the input/return separator.
    split_line = line.split(":")
    #Split each input argument
    inputs = split_line[0].split
    #Split each return arguement
    returns = split_line[1].split    
    #Parse the inputs into a hash
    in_hash = Hash.new      
    inputs.each do |inp|
      split_inp = inp.split("=>")
      inp_name = split_inp[0]
      inp_value = split_inp[1]
      #print inp_name, inp_value
      #puts
      in_hash[inp_name.to_sym] = arg_convert(inp_value)
    end
    if !in_hash.has_key?(:method) or !in_hash.has_key?(:fails) or in_hash[:fails] <= 0
      raise ArgumentError
    end
    #Parse the returns into a hash
    ret_hash = Hash.new          
    returns.each do |ret|
      split_ret = ret.split("=>")
      ret_name = split_ret[0]
      ret_value = split_ret[1]
      #print ret_name, ret_value
      #puts
      ret_hash[ret_name.to_sym] = arg_convert(ret_value)
    end
    return in_hash, ret_hash    
  end  
  
private
  def arg_convert(arg_value)
    ret = nil
    if arg_value[0].chr == "'" and arg_value[-1].chr == "'"
      #String value. Remove the leading and trailing quotes
      ret = arg_value[1...-1]
    elsif arg_value.include? "."
      #Float value
      ret = arg_value.to_f
    elsif arg_value =~ /^0x/
      #Hex value if it starts with 0x
      ret = arg_value.hex
    else
      ret = arg_value.to_i
    end
    #puts "arg_Value #{ret}"
    return ret
  end
end

#o1 = "method=>'rpm1000cfgputstate' fails=>2 inrpm=>2 inport=>3 : ulReturn=>0x80041001"
#o2 = "method=>'hcugetslotinfo' fails=>2 : ulReturn=>0x800410 ulRpm=>1"
#op = OverrideParser.new
#inp, ret = op.parse_line(o1)
#p inp
#p ret
#puts
#inp, ret = op.parse_line(o2)
#p inp
#p ret
