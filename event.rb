require "observer"
require 'thread'
require 'event_queue'
require 'socket'
require 'Rpc/HcuRpc/Rpc'
require 'constants/event_ids'
require 'log'
require 'constants/generic_constants'

class Event
  #Use extend instead of include since we want to use this at the class level
  extend Observable 
  attr_reader :event_type, :source_uid, :timestamp, :payload, :priority, :sequence_number
  #Incremented for each event sent
  @@next_sequence_number = 1
  @@mutex = Mutex.new
  @@xdr_event = XdrEvent.new    
  #Payload is array of max length of five
  #priority should be :low, :medium, or :high
  def initialize(source_uid, event_type, priority=EventIds::EVENT_PRIORITY_LOW, payload=nil)
    @source_uid = source_uid
    @event_type = event_type
    if payload != nil
      if payload.class == Array
      @payload = payload
      else
        Log::EventLogger.error "Error in Event creation for event type #{event_type}. Payload must be an array"
        raise
      end
    else
      @payload = []
    end
    @priority = priority
    #Event specific sequency number
    @sequence_number = 0
    #Time since epoch
    @time = Time.now.to_i
    #Publish this event
    Event.changed
    Event.notify_observers(self)
  end
  #Pack the message in xdr to send to server. Returns a xdr stream and the sequence number.
  def xdr_pack
    #Lock while getting the sequence number
    @@mutex.synchronize do
      @sequence_number = @@next_sequence_number
      @@next_sequence_number += 1
    end
    xdr_output = @@xdr_event.encode_evt(@sequence_number, @event_type, @source_uid, @priority, @time, @payload)
    return xdr_output
  end
end

#This class subscribes to events. It quest events and sends them when it is connected to the server.
class EventSender
  @@xdr_event = XdrEvent.new
  @@xdr_header = XdrHeader.new
  def initialize
    #Sign up for all events
    Event.add_observer(self)
    @connected = false
    @transfer_enabled = false
    @event_queue = EventMessageQueue.new
  end
  #Called by the RPC handler to connect to server event sink
  def connect(ip, port)
    if @connected
      Log::EventLogger.error "Received event sink connect call from #{ip}. Already connected to #{@sink_ip}"
      raise
    end
    @socket = TCPSocket.open(ip, port)    
    Log::EventLogger.debug "EventSender transfer connected"    
    @connected = true
    @sink_ip = ip
    @sink_port = port
  end
  #Called by the RPC handler to disconnect from server event sink  
  def disconnect
    Log::EventLogger.debug "EventSender transfer disconnected" 
    begin
      @socket.close
    rescue
    end
    @connected = false
    @sink_ip = nil
    @sink_port = nil
  end
  #Enables the HCU to send events to the server  
  def enable_transfer()        
    #Start the send_event thread which waits on events and sends them
    @transfer_enabled = true
    Thread.new{dispatch_events}    
    Log::EventLogger.debug "EventSender transfer enabled"
  end
  #Disables the HCU from sending events to server
  def disable_transfer()
    Log::EventLogger.debug "EventSender transfer disabled"
    @transfer_enabled = false
  end
  def connected?
    @connected
  end
  #This is the notify method when the publisher sends a new event. The event is sent over
  def update(event)
    #Send event to message queue_new_event
    Log::EventLogger.debug "Queuing event: #{event.event_type}"
    @event_queue.send_event(event)
  end
  #This method is run in a separate thread to send events
  def dispatch_events()
    while @transfer_enabled
      #Wait for an event. Blocks until an event is received. 
      #TODO need to figure out what to do here is the socket is closed or the transfer is disabled.
      #I think we can just store the event in instance variable and send it first after reconnect. 
      event = @event_queue.receive_event      
      Log::EventLogger.debug "Sending event: #{event.event_type}"
      #Pack the event and send it
      xdr_output = event.xdr_pack
      #TODO I'm not sure if I need to wrap this in a while loop
      @socket.write(xdr_output)
      receive_ack(event.sequence_number)
    end
  end  
  #Wait for the event ack. Return the ack sequence number 
  def receive_ack(sequence_number)
    seq = 0
    begin
        header = ""
        while header.length < GenericConstants::HEADER_LENGTH
          data = @socket.recv(GenericConstants::HEADER_LENGTH)        
          header << data
        end
        #Send the header off for decoding
        ack_header = @@xdr_header.hdr_decode(header)        
        payload_length = ack_header[0]
        payload = ""
        while payload.length < (payload_length - GenericConstants::HEADER_LENGTH)
          data = @socket.recv(payload_length - GenericConstants::HEADER_LENGTH)     
          payload << data
        end
        seq = @@xdr_event.decode_evt_ack(payload, payload_length - GenericConstants::HEADER_LENGTH)
    rescue Exception
        Log::EventLogger.error "Error in EventDriver::read_message"
    end
    if seq != sequence_number
      Log::EventLogger.error "Event ack received: sequence numbers do not match. Expected #{sequence_number}, Received #{seq}"
    else
      Log::EventLogger.debug "Received event ack"
    end
  end
end

